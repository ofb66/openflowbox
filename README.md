# OpenFlowBox

Control, automate and extend home gardening systems via ESP32 microcontroller.

_**Disclaimer**: The END-USER is COMPLETELY RESPONSIBLE for the use of OpenFlowBox. Developers and maintainers assume NO liability and are NOT responsible for any misuse or damage caused by this program_


![Alt text](pictures/application.png?raw=true "Application screenshot")


# Table of contents

1. [Introduction](#introduction)
    1. [License](#license)
    2. [Security vulnerabilities](#security_vul)
    3. [Planned in future releases](#future_releases)
2. [Installation](#installation)
    1. [Dependencies / Used libraries](#deps_and_libs)
    2. [Prerequisites](#prerequisites)
        1. [Download application](#download_app)
    3. [Setting up file system and flashing](#setup_flash)
3. [Documentation](#documentation)
    1. [What works](#what_works)
    2. [What works not](#what_not)
    3. [Modules](#modules)
    4. [Web](#web)
    5. [Hardware Control](#hw_control)
    6. [Persistence](#persistence)
    7. [Simple datastructures](#data_structs)
4. [User manual](#user_manual)
    1. [First steps](#first_steps)
    2. [Device configuration](#device_conf)
        1. [Infobar](#infobar)
        2. [Toolbar](#toolbar)
        3. [Basic settings](#basic_settings)
            1. [WiFi settings](#wifi_settings)
            2. [User authentication](#user_auth)
            3. [Backup and upload of existing files](#backup)
            4. [Create new configuration](#create_configs)
            5. [Time settings](#time_settings)
            6. [Basic module settings](#basic_module_settings)
        4. [Module settings](#module_settings)
            1. [Sensors](#sensor_section)
            2. [PWM controlled devices](#pwm_section)
            3. [Plug devices](#plug_section)
                1. [Relais plug devices](#plug_relais_section)
                2. [WiFi plug devices (Tasmota)](#plug_wifi_section)

<a name='introduction'></a>
# Introduction

<a name='license'></a>
## License
This program is licensed under GPL-3.0


<a name='security_vul'></a>
## Security vulnerabilities
- !!!There is no https/tls transport encryption available at the moment for server<->client communication!!!
**Don't use this application in an open network**!


<a name='future_releases'></a>
## Planned in future releases
- [ ] HTTPS/TLS transport encryption
- [x] WiFi scanning function
- [x] Determine all connected IPs in AP mode and show them in the UI
- [x] Add ability to control plugs by moisture sensor
- [ ] Integrated MQTT broker, if possible
- [ ] Switch from selfcreated file format (.ocp) for configuration file packages to a more standardized solution (e.g. zip) (Maybe possible in newer Arduino core versions)
- [ ] Add control structure for plugs (e.g. for lights) to check plug status against the actual status determined by sensor (e.g photoled)
- [ ] Let the user define a timebased configuration chain, to switch between different configurations automatically (e.g. 'Grow' 4Weeks => 'Flow' 63Days...)
- [x] Add undefined behaviour if the current value of sensor controlled plug switches is between the two setpoints


<a name='installation'></a>
# Installation

<a name='deps_and_libs'></a>
## Dependencies / Used libraries
- [Arduino core for ESP32](https://github.com/espressif/arduino-esp32)
- [Adapted **fontawesome** icons (minimal set) / Parts of Javascript icons and functionalities (v4.x)](https://fontawesome.com/)
- [AsyncWebServer v1.2.3](https://github.com/me-no-dev/ESPAsyncWebServer)
- [NTPClient v3.2.0](https://github.com/arduino-libraries/NTPClient)
- [SparkFunHTU21D](https://github.com/sparkfun/SparkFun_HTU21D_Breakout_Arduino_Library)
- [BME280](https://www.github.com/finitespace/BME280)

- File system (will maybe moved into 'Arduino core for ESP32' in future releases (from v2.0 on))
    - [FSplugin](https://github.com/lorol/arduino-esp32fs-plugin)
    - [mklittlefs](https://github.com/earlephilhower/mklittlefs)
    - [mkfatfs](https://github.com/labplus-cn/mkfatfs) (has to be compiled for linux)


<a name='prerequisites'></a>
## Prerequisites

- Installation of [Arduino IDE](https://www.arduino.cc/en/software)
- Install [Arduino core for ESP32](https://github.com/espressif/arduino-esp32) via device manager of the Arduino IDE
    - Go to "File" => "Preferences" and enter "https://dl.espressif.com/dl/package_esp32_index.json" into the "Additional Board Manager URLs” field. **Note**: You can enter more than one URL into this field by comma separating each.
    - Go to "Tools" => "Board" => "Boards manager..." => search for "esp32" and install "**esp32** by **Espressif Systems**"
- Install libraries:
    - Download [AsyncWebServer v1.2.3](https://github.com/me-no-dev/ESPAsyncWebServer), [NTPClient v3.2.0](https://github.com/arduino-libraries/NTPClient), [SparkFunHTU21D](https://github.com/sparkfun/SparkFun_HTU21D_Breakout_Arduino_Library) and [BME280](https://www.github.com/finitespace/BME280) libraries from github and put the unpacked folders into the "libraries" folder of the IDEs projects directory. (e.g. ~/Arduino/libraries)

**How to install Arduino libraries**: [https://www.arduino.cc/en/guide/libraries]


<a name='download_app'></a>
### Download application
- Download the openflowbox repository and put the content of its unpacked folder into the projects directory of the Arduino IDE

**Hint**: The project folder has to be named like the *.ino file (sketch in the Arduino environment). In this case the folder should named "flowbox" (=> "flowbox.ino")


<a name='setup_flash'></a>
## Setting up file system and flashing

Connect ESP32 board via USB

- Load data folder via esp32-fs-plugin in the arduino ide to the esp32 board
    - "Tools" => "ESP32 Sketch Data Upload" => select "LittleFS" and click "Ok"


- Now you can try to compile the application and if all goes fine, you are ready to flash the whole application onto your ESP32 via Arduino IDE

_Hint: Users experience sometimes a connection problem between computer and ESP32 while uploading data to filesystem or application => After the Arduino IDE shows in the progress bar `Connecting ....----...`, push the boot button on the ESP32 board until the upload process starts_


<a name='documentation'></a>
# Documentation

<a name='what_works'></a>
## What works

- Connect up to 8 sensors (HTU21D / BME280 / Chirp soil moisture) via I2C (plug and play)

- PWM Control
    - Control up to 4 different PWM signals for 4-pin-PWM-fans via temperature(PID), humidity(PID), soil moisture(PID), all of them combined or manually
    - Speed measurement
    - Different settings for day/night modi
    - Sensor matrix to choose from  
    
- Control up to 2 (in future releases maybe more, depends on the hardware setup) plugs directly connected via relais(currently 12V DC planned, but can also be used to connect 230V AC plugs), controlled by timer, interval timer, temperature and/or humidity and/or soil moisture (the control chain can be cascaded) or at least manually

- Control up to 3 Wireless plugs flashed with the [Tasmota](https://www.tasmota.info/) firmware. Behave in the same manner as the directly connected plugs

- Configuration files: Define different settings for different steps in the grow progress (e.g. growing with 18hrs light on, different humidity and temperature values and so on). These configurations can be saved persistently into files 

- Fileexchange: All files saved in the filesystem can be downloaded or exchanged through newer versions:
    - web files: js (warning: file must be error free (syntax...) to avoid flashing whole fs again (configuration backup recommended)), css, svgs ...
    - configuration files: splitted into two categories: basic configuration (Wifi settings, device names, ips, current active configuration) and module configuration (pwm, plug settings...). In the latter case there can exist multiple configuration files (named by the user, see below [Configuration files](#create_configs))

- User authentication (disabled by default)

- WiFi: Switch between AP and STA mode / Hide SSID in AP mode (disabled by default)


<a name='what_not'></a>
## What works not

- https/tls transport encryption
- no possibility to use the ESP32 as a MQTT broker, which would make the connection to other home automation devices (like Tasmota plugs) much easier (At the moment the connection between the OpenFlowBox and Tasmota devices is realized due simple requests to the Tasmota WebAPI). Also an external MQTT Broker (e.g. Raspberry Pi) is not supported, because intentionally this ESP32 board is designed as a standalone controller.


<a name='modules'></a>
## Modules
<a name='web'></a>
## Web
<a name='hw_control'></a>
## Hardware Control
<a name='persistence'></a>
## Persistence
<a name='data_structs'></a>
## Simple datastructures


<a name='user_manual'></a>
# User manual

<a name='first_steps'></a>
## First steps

After first boot the ESP32 will start an WiFi AccessPoint, its name looks like **'OFB32_XXXXX'**, where XXXXX stands for a 5-digit number extracted from the boards MAC-address. The WiFi AP is secured with the password **flowerpower**.
Now you can connect to the WebUI via a browser on your computer, mobile device, etc., under the address **flowbox_32.local** or **192.168.4.1** (Android for example doesn't support mDNS, so type the IP-address directly is the only way to reach the WebUI)
There you should change the WiFI AP password immediately or connect to another WiFi AP of your choice.[Wifi settings](#wifi_settings)


<a name='device_conf'></a>
## Device configuration

<a name='infobar'></a>
### Infobar

![Alt text](pictures/infobar.png?raw=true "Infobar")

Basic information about the device: time, runtime of the grow progress, average temperature and humidity, status of light sensor, current WiFi mode, currently consumed RAM and currently selected configuration


<a name='toolbar'></a>
### Toolbar

![Alt text](pictures/tool_btns.png?raw=true "Toolbar")

Implemented buttons:
- Save button (works for basic configuration and module configuration)
- Restore defaults
- Restart device
- Logout (of course only makes sense, if authentication is enabled and user name and password are set)

Everytime you change something in the current selected configuration, a red triangle will appear next to the save button to display unsaved changes. Also if you change something in some basic configurations (WiFi access point, SSID, user authentication,...) a red triangle will appear next to the restart button, to show that the made changes will affect only after a reboot.


<a name='basic_settings'></a>
### Basic settings

<a name='wifi_settings'></a>
#### WiFi settings

![Alt text](pictures/wifi_conf.png?raw=true "WiFi settings")

- Set an individual hostname to reach the webUi in browser (hostname.local)
- Set access point name (SSID) and its belonging password, additionaly you can enable to hide the access point, so only devices which know the exact access credentials find this access point
- Connect to an existing wifi network 

**Limitations**: either WiFi Access point or connection to an existing WiFi network can be active. Not both at the same time. 


<a name='user_auth'></a>
#### User authentication

![Alt text](pictures/user_auth.png?raw=true "User authentication")

To enable user authentication both the checkbox has to be selected and name and password have to be set.


<a name='backup'></a>
#### Backup and upload of existing files

![Alt text](pictures/conf_handle.png?raw=true "Backup/Upload section")

You can choose between 
- Basic: configuration file(WiFi configuration, user configuration, time settings, module basic config(names, symbols, ip addresses)
- Module: configuration files (module settings e.g. fan pid controller settings, plug settings, etc)
- Full: (both of the above)

Downloaded files are of .ocp format (ocp stands for **o**penflowbox **c**onfiguration **p**ackage).
For the reupload you are also able to upload and exchange the underlying web files (style(css), script(js), icons(svg))


<a name='create_configs'></a>
##### Create new configurations

![Alt text](pictures/conf_add.png?raw=true "Configuration handle")

To add a new configuration, type a name (max. 8 chars) into the "New config" field and enter the "+"-button.
It will automatically be the selected configuration. Now you can adapt the settings(e.g. time settings of the plugs, fan settings...) to fit your needs. After you have made the changes you want, you should save this configuration by pushing the "Save"-button located in the [Toolbar](#toolbar).
If you have multiple configurations saved, you can switch between them and the configuration changes right away.


<a name='time_settings'></a>
#### Time settings

![Alt text](pictures/time_conf.png?raw=true "Time settings")

- the measured progress runtime of the current grow: Possible formats are:
    - hh:mm:ss or
    - xxWxxDxxhxxmxxs (Weeks, Days, hours, minutes, seconds) (not necessary to mention them all e.g "2W3h")

- UTC timezone
- NTP server (only relevant if there is a connection to the internet, else the ESP uses the timestamp from the connected client)


<a name='basic_module_settings'></a>
#### Basic module settings

![Alt text](pictures/basic_module_setting.png?raw=true "Basic module settings")

All device names can be changed in the basic settings.
Also for the plug devices you can select individual symbols.
Additionaly for the wifi plugs, you have to adapt their IP-Adresses. (You can get them in WiFi STA mode by visit the website of your router)


<a name='module_settings'></a>
### Module settings

<a name='sensor_section'></a>
#### Sensors

![Alt text](pictures/sens_sect_wo_conn_sens.png?raw=true "Sensor section without connected sensor")

![Alt text](pictures/sens_sect_w_conn_sens.png?raw=true "Sensor section with connected sensors")

Add a sensor by plugging it in and push the scan button in the sensor section. If the ESP32 board recognize the new sensor, the page should reload after a few seconds automatically and show the new plugged in sensor.
To remove a sensor unplug it and start the scan process again.


![Alt text](pictures/sens_section_max.png?raw=true "Sensor section maximized")

All sensors save at least the minimum, the maximum and the average value. 
In addition all sensor values are shown averaged in the overview.


<a name='pwm_section'></a>
#### PWM controlled devices

![Alt text](pictures/fan_ov.png?raw=true "Fan section")
_Overview of the fan section_

![Alt text](pictures/fan_conf_min.png?raw=true "Fan section minimized")
_One fan with collapsed sensor section_

![Alt text](pictures/fan_conf_sens_min.png?raw=true "Fan section sensor minimized")
_Sensor section with collapsed temperature and humidity controller_

![Alt text](pictures/fan_conf_sens_max.png?raw=true "Fan section sensor maximized")
_Temperature sensor section with collapsed PID controller_

![Alt text](pictures/fan_conf_sens_pid.png?raw=true "Fan section sensor pid controller")
_Temperature sensor section with maximized PID controller_


<a name='plug_section'></a>
#### Plug devices

![Alt text](pictures/plug_conf_min.png?raw=true "Plug device minimized")

![Alt text](pictures/plug_conf_max.png?raw=true "Plug device maximized")

- Cascade the settings, for example:
    - Timer(enabled): from 08:00am to 08:00pm
    - Interval timer(enabled): 10minutes off then 20minutes on
    - Temperature(enabled): 20°C Off and 22°C On
    - Humidity(enabled): 60% Off and 70% On
Only if the timer is on and status of interval timer is on,
temperature and humidity are taken into account.
- This results in the following behaviour:
    - Time: 06:30pm (Timer on)
    - Interval timer: 5min30s lasts on
    - Temperature: 19.5°C
    - Humidity: 72%
    - => Plug is on

<a name='plug_relais_section'></a>
##### Relais plug devices


<a name='plug_wifi_section'></a>
##### WiFi plug devices (Tasmota)






