#ifndef PWMDEVICE_HPP
#define PWMDEVICE_HPP

#include <Arduino.h>

#include "idevice.hpp"
#include "devicetacho.hpp"
//#include "../simples/string.hpp"
#include "../persistence/serializable.hpp"
using namespace Persistence;

#define PWM_FREQUENCY   25000

class PWMDevice : public IDevice, public ISerializable {
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
    const uint8_t m_pin;
    std::string m_name;
    
    uint8_t m_currV;

#ifdef ARDUINO_ARCH_ESP32
    uint8_t m_channel;
    static uint8_t s_channel;
#endif
	DeviceTacho tacho;
	uint16_t m_rpm;


    //IWeb
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);

public:
    PWMDevice(uint8_t pin, const char* name, uint8_t tachopin, IWeb* const parent = nullptr);

    inline const std::string& name() const { return m_name; }

    //IDevice
    virtual void update();
    
	virtual void handleData(uint8_t varId, void* const &payload) override;
    
    //ISerializable
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
};

#endif
