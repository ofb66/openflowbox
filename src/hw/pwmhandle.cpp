#include "pwmhandle.hpp"

#include "pidcontrol.hpp"
#include "pins.h"

PWMHandle* PWMHandle::phandle = nullptr;

const char PWMHandle::s_header[] PROGMEM = R"=====(			<button type='button' class='collapsible'><i class='fas fa-fan fa-2x'></i></button>
			<div class='content' id='%u'>
)=====";

const char PWMHandle::s_footer[] PROGMEM = R"=====(			</div>
)=====";

PWMHandle::PWMHandle(IWeb* const parent)
: IWeb(parent)
{
#ifndef ARDUINO_ARCH_ESP32
    analogWriteRange(PWM_RANGE_MAX);
    analogWriteFreq(PWM_FREQUENCY);
#endif
}

size_t PWMHandle::len(IWeb::StrObject so)
{
	if(m_children.size() == 0) return 0;
	
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 3;
		case StrObject::FOOTER:
		return strlen_P(s_footer);
		default:
		return 0;
	}
}

size_t PWMHandle::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	if(m_children.size() == 0) return 0;
	
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header, webId());
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        break;
        default:
        return 0;
    }
}


PWMHandle* PWMHandle::singleton()
{
    if(phandle == nullptr){
        phandle = new PWMHandle();

        new PWMDevice(PIN_FAN1, "OUTPUT", PIN_TACHO, phandle);
        new PWMDevice(PIN_FAN2, "INPUT", PIN_TACHO, phandle);
        new PWMDevice(PIN_FAN3, "INSIDE", PIN_TACHO, phandle);
        new PWMDevice(PIN_FAN4, "LIGHT", PIN_TACHO, phandle);
    }
    return phandle;
}


void PWMHandle::update()
{
    std::vector<IWeb*>::iterator it;
    for(it = m_children.begin(); it != m_children.end(); it++)
    {
        auto fan = static_cast<PWMDevice*>(*it);
        fan->update();
    }
}
