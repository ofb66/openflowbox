#include "pwmsetting.hpp"

#include "sensorhandle.hpp"
#include "sensor.hpp"
#include "../persistence/serializablepod.hpp"

using namespace Persistence;


const char PWMSetting::s_header[] PROGMEM = R"=====(			<button type='button' class='collapsible relative'>
			<input type='radio' class='allb nccrb %s' id='%u' name='c%u'/><label for='%u'><span class='rbui'></span></label>
			</button>
			<div class='content' id='%u'>
				<p class='relative'><input type='checkbox' onclick='sendBool(this)' class='allb ccb standardcb' id='%u' %s><label for='%u'><span class='ui'></span></label></p>
				<br>
				<p>VMin <span id='%u'></span>%%</p>
				<p><input type='range' onchange='sendNumber(this); document.getElementById(parseInt(this.id)+2).setAttribute("min", this.value)' max='50' value='%u' id='%u'></p>
				<hr>
				<p>VMax <span id='%u'></span>%%</p>
				<p><input type='range' onchange='sendNumber(this);' min='%u' value='%u' id='%u'></p>
				<hr>
				<button type='button' class='collapsible'><i class='fas fa-microchip'></i></button>
				<subarticle class='subcontent'>
)=====";

const char PWMSetting::s_footer[] PROGMEM = R"=====(				</subarticle>
			</div>
)=====";
 
const char PWMSetting::s_json_dyn[] PROGMEM = R"=====({
"%u":%u
})=====";

const char PWMSetting::s_json_ui[] PROGMEM = R"=====({
"%u":%u,
"%u":%u,
"%u":%u,
"%u":%u,
"%u":%u
})=====";

size_t PWMSetting::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 120;
		case StrObject::FOOTER:
		return strlen_P(s_footer);
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 15;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 50;
        default: return 0;
	}
}

size_t PWMSetting::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	bool lightStatus = (SensorHandle::singleton()->isLightOn() == isDayDevice);
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							(isDayDevice) ? PSTR("sun") : PSTR("moon"),
							createId(8), parent()->webId(), createId(8),
							webId(), createId(0),
							(IWeb::isFlagSet(IDevice::ENABLED)) ? PSTR(" checked") : PSTR(""),
							createId(0), 
							createId(1), PWM_DISPLAY(m_vmin), createId(2),
							createId(3), PWM_DISPLAY(m_vmin), PWM_DISPLAY(m_vmax), createId(4)
						);
        break;
        case StrObject::FOOTER:
        IWeb::enableFlag(PWMSetting::ON_LOAD);
        load_timer.start();
        return snprintf_P((char*)buffer, len, s_footer);
        break;
        case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn,
							createId(8), (uint8_t)lightStatus
						);
        break;
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0), (uint8_t)IWeb::isFlagSet(IDevice::ENABLED),
							createId(1), PWM_DISPLAY(m_vmin),
							createId(2), PWM_DISPLAY(m_vmin),
							createId(3), PWM_DISPLAY(m_vmax),
							createId(4), PWM_DISPLAY(m_vmax)
						);
        break;
        default: return 0;
    }
}

PWMSetting::PWMSetting(uint8_t vmax, bool isday, IWeb* const parent) :
IDevice(parent),
ISerializable(ISerializable::Type::MODULE_CFG),
isDayDevice(isday),
m_vmax(vmax),
m_vmin(0),
m_mix(50)
{
    new PidControl(20., -3.5, -1, -0.5, SensorValue::Type::DEGREE, &m_vmin, this);
    new PidControl(45., -2.5, -0.75, -0.25, SensorValue::Type::HUMIDITY, &m_vmin, this);
	new PidControl(45., -1.5, -0.5, -0.15, SensorValue::Type::SOIL_MOISTURE, &m_vmin, this);
}

void PWMSetting::handleData(uint8_t varId, void* const &payload){
	switch(varId){
		case 0:
		IWeb::setFlag(IDevice::ENABLED, *(bool*)payload);
		break;
		case 2:
		m_vmin = PWM_DISPLAY_2_VAL(*(uint16_t*)payload);
		if(m_vmin > m_vmax) m_vmax = m_vmin;
		break;
		case 4:
		m_vmax = PWM_DISPLAY_2_VAL(*(uint16_t*)payload);
		if(m_vmin > m_vmax) m_vmax = m_vmin;
		break;
		case 7:
		m_mix = *(uint8_t*)payload;
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
}

//IDevice
void PWMSetting::update()
{
	if(IWeb::isFlagSet(PWMSetting::ON_LOAD) && load_timer.hasExpired(2000)){
		IWeb::disableFlag(PWMSetting::ON_LOAD);
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE | IWeb::HAS_CHANGE);
	}
	
	bool lo = SensorHandle::singleton()->isLightOn();
	if(lo != IWeb::isFlagSet(PWMSetting::LIGHT_ON)){
		IWeb::toggleFlag(PWMSetting::LIGHT_ON);
		IWeb::enableFlag(IWeb::HAS_CHANGE);
	}
	
	if(!IWeb::isFlagSet(IDevice::ENABLED))
	{
		m_vcurr = 0;
		return;
	}
	
	std::vector<IWeb*>::iterator it;
	
	float output = 0.;
	float weightSum = 0.;
	
	for(it = m_children.begin(); it != m_children.end(); it++){
		auto pidctrl = static_cast<PidControl*>(*it);
		pidctrl->update();
		output += pidctrl->getWeightedOutput();
		weightSum += pidctrl->getWeight();
	}
	uint8_t vcurr = m_vcurr;
	if(weightSum == 0.){
		m_vcurr = m_vmax;
	} else {
		m_vcurr = (uint8_t)(output / weightSum);
	}
	if(m_vcurr > m_vmax) m_vcurr = m_vmax;
	else if(m_vcurr < m_vmin) m_vcurr = 0;
	
	if(vcurr == 0 && m_vcurr > 0){
		m_vcurr = PWM_RANGE_MAX; // Start fan with 100% to be sure fan is starting
	}
}

//ISerializable
size_t PWMSetting::size(){
    return  SerializablePOD<uint16_t>::size(m_flags) +
			SerializablePOD<uint8_t>::size(m_vmin) +
			SerializablePOD<uint8_t>::size(m_vmax);
}
uint8_t* PWMSetting::serialize(uint8_t *eptr){
    eptr = SerializablePOD<uint16_t>::serialize(eptr, m_flags);
	eptr = SerializablePOD<uint8_t>::serialize(eptr, m_vmin);
	eptr = SerializablePOD<uint8_t>::serialize(eptr, m_vmax);
    return eptr;
}
const uint8_t* PWMSetting::deserialize(const uint8_t *eptr){
    eptr = SerializablePOD<uint16_t>::deserialize(eptr, m_flags);
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, m_vmin);
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, m_vmax);	
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
    return eptr;
}
