#ifndef PLUGDEVICE_HPP
#define PLUGDEVICE_HPP

#include <Arduino.h>

#include "idevice.hpp"
#include "../web/iweb.hpp"
//#include "../simples/string.hpp"

#ifdef ARDUINO_ARCH_ESP8266
#include <IPAddress.h>
#endif

#include "../persistence/serializable.hpp"
using namespace Persistence;

class AsyncClient;

class IPlugDevice : public IDevice, public ISerializable
{
protected:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	std::string m_name;
	uint8_t symbol_indx;
	
	Utils::Timer16 loadTimer;
	
    //IWeb
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
	bool hasToSwitch();
	
	IPlugDevice(const char* name, IWeb* const parent = nullptr);
public:
	enum {
		TIME_CTRL 		= DEVC_LAST,
		INTERVAL_CTRL 	= DEVC_LAST << 1,
		TEMP_CTRL		= DEVC_LAST << 2,
		HUMI_CTRL		= DEVC_LAST << 3
	};
	
	virtual ~IPlugDevice() {};
	
	virtual void switchOn() = 0;
	virtual void switchOff() = 0;
	
	virtual void update();
	void toggle();
	
	void handleData(uint8_t varId, void* const &payload);
	
	void setSymbol(uint8_t indx) { symbol_indx = indx; }
	uint8_t getSymbol() const { return symbol_indx; }
	
	const std::string& name() const { return m_name; }
	
    //ISerializable
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
};

class PlugDevice12V : public IPlugDevice
{
private:
	const uint8_t m_pin;
public:
	PlugDevice12V(uint8_t pin, const char* name, IWeb* const parent = nullptr);

	uint8_t pin() const { return m_pin; }
	
	virtual void switchOn();
	virtual void switchOff();
};

class PlugDeviceRemote : public IPlugDevice
{
private:
	static const char s_cmd_power_on[] PROGMEM;
	static const char s_cmd_power_off[] PROGMEM;
	static const char s_cmd_power_status[] PROGMEM;
	static const char s_respond_status_on[] PROGMEM;
	static const char s_respond_status_off[] PROGMEM;
	
	AsyncClient* client;
	IPAddress m_ip;
	
	void runAsyncClient(const char* cmd);
	void checkPlugStatus(char* data, size_t len);
	
public:
	PlugDeviceRemote(const char* name, uint32_t ip, IWeb* const parent = nullptr) : IPlugDevice(name, parent), client(nullptr), m_ip(ip) {}
	bool hasValidIP() const;
	uint32_t ipAddress() const { return m_ip; }
	
	virtual void handleData(uint8_t varId, void* const &payload) override;
	
	virtual void switchOn();
	virtual void switchOff();
	
    //ISerializable
    virtual size_t size() override;
    virtual uint8_t* serialize(uint8_t *eptr) override;
    virtual const uint8_t* deserialize(const uint8_t *eptr) override;
};

#endif
