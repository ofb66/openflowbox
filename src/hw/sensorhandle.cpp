#include "sensorhandle.hpp"

#include "Wire.h"
#include "pins.h"

SensorHandle* SensorHandle::shandle = nullptr;

const char SensorHandle::s_header[] PROGMEM = R"=====(			<button type='button' class='collapsible relative'>
			<i class='fas fa-microchip fa-2x'></i><br>
			</button>
			<div id='%u' class='content'>
			<article>
				<button type='button' class='collapsible'><h3><i class='fas fa-microchip'></i> Overview</h3>
					<h4><i class='fas fa-thermometer-half'></i> <span id='%u'></span>°C|<!--
				 --><i class='fas fa-tint'></i> <span id='%u'></span>%%|<!--
				 --><i class='fas fa-water'></i> <span id='%u'></span>%%</h4>
				</button>
				<div class='content'>
				<table>
					<tr>
					<th>Min</th><th>&#8960;</th><th>Max</th>
					</tr>
					<tr style='font-size:0.9em'>
					<td><span id='%u'>%5.2f</span>°C</td>
					<td><span id='%u'>%5.2f</span>°C</td>
					<td><span id='%u'>%5.2f</span>°C</td>
					</tr>
					<tr style='font-size:0.9em'>
					<td><span id='%u'>%5.2f</span>%%</td>
					<td><span id='%u'>%5.2f</span>%%</td>
					<td><span id='%u'>%5.2f</span>%%</td>
					</tr>
					<tr style='font-size:0.9em'>
					<td><span id='%u'>%5.2f</span>%%</td>
					<td><span id='%u'>%5.2f</span>%%</td>
					<td><span id='%u'>%5.2f</span>%%</td>
					</tr>
				</table>
				<button type='button' onclick='sendClick(this);' id='%u' class='btn widebtn'>
					<h4><i class='fas fa-sync-alt'></i></h4>
				</button>
				</div>
				<button type='button' onclick='sendClick(this);' id='%u' class='btn articlebtn'>
					<h4>
					<span class='fa-layers fa-fw'>
						<i class='fas fa-microchip fa-2x' color='#333666' data-fa-transform='rotate-90'></i>
						<span class='fa-layers-text' data-fa-transform='shrink-2 right-2' style='font-weight:600;'>I²</span>
						<i class='fas fa-redo-alt' data-fa-transform='shrink-5 right-12 rotate--18'></i>
					</span>
					</h4>
				</button>
			</article>
			<br>
			<div style='margin: auto;'>
)=====";

const char SensorHandle::s_footer[] PROGMEM = R"=====(
			</div>
			</div>
)=====";

const char SensorHandle::s_json_dyn[] PROGMEM = R"=====({
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f
})=====";

const char SensorHandle::s_json_ui[] PROGMEM = R"=====({
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f
})=====";


SensorHandle::SensorHandle(IWeb* const parent)
:
IWeb(parent),
m_availableSensors(0),
ov_sensor(0xFF)
{
    Wire.begin();
    IWeb::enableFlag(IWeb::INTERVAL);
    
    if(validateSensors()){
		IWeb::enableFlag(SensorHandle::ON_LOAD);
		interval_timer.start(); 
	}
}

int8_t SensorHandle::tcaselect(uint8_t i2c_bus){
    if(i2c_bus > MAX_SENSORS) return -99;
    Wire.beginTransmission(MUX_ADDRESS);
    Wire.write(1 << i2c_bus);
    return Wire.endTransmission();
}

size_t SensorHandle::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 155;
		case StrObject::FOOTER:
		return strlen_P(s_footer);
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 20;
		default:
		return 0;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 45;
	}
}

size_t SensorHandle::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{	
	const float* statsT = ov_sensor.getLogs(0);
	const float* statsH = ov_sensor.getLogs(1);
	const float* statsM = ov_sensor.getLogs(2);
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							webId(),
							createId(1), 
							createId(2), 
							createId(3),
							createId(4), statsT[LOG_MIN],
							createId(5), statsT[LOG_AVG],
							createId(6), statsT[LOG_MAX],
							createId(7), statsH[LOG_MIN],
							createId(8), statsH[LOG_AVG],
							createId(9), statsH[LOG_MAX],
							createId(10), statsM[LOG_MIN],
							createId(11), statsM[LOG_AVG],
							createId(12), statsM[LOG_MAX],
							createId(13), createId(14)
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        break;
        case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn,
							createId(1), ov_sensor.getMeasurement(SensorValue::Type::DEGREE),
							createId(2), ov_sensor.getMeasurement(SensorValue::Type::HUMIDITY),
							createId(3), ov_sensor.getMeasurement(SensorValue::Type::SOIL_MOISTURE)
						);
        break;
        default:
        return 0;
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(4), statsT[LOG_MIN],
							createId(5), statsT[LOG_AVG],
							createId(6), statsT[LOG_MAX],
							createId(7), statsH[LOG_MIN],
							createId(8), statsH[LOG_AVG],
							createId(9), statsH[LOG_MAX],
							createId(10), statsM[LOG_MIN],
							createId(11), statsM[LOG_AVG],
							createId(12), statsM[LOG_MAX]
						);
        break;
    }
}


SensorHandle* SensorHandle::singleton()
{
    if(shandle == nullptr){
        shandle = new SensorHandle();
    }
    return shandle;
}

void SensorHandle::handleData(uint8_t varId, void* const &payload)
{
    if(varId == 13){
		ov_sensor.resetLogs();
    } else if(varId == 14){
        if(validateSensors()){
			IWeb::requestPageRefresh();
		}
	}
}

uint8_t SensorHandle::getRegisteredSensors()
{
    uint8_t ret = 0;
    std::vector<IWeb*>::iterator it;
    for(it = m_children.begin(); it != m_children.end(); it++){
    	auto sens = static_cast<ISensor*>(*it);
    	if(sens->isAvailable())
			ret |= (1 << sens->address());
    }
    return ret;
}

uint8_t SensorHandle::getAvailableSensors(SensorValue::Type t)
{
    uint8_t ret = 0;
    std::vector<IWeb*>::iterator it;
    for(it = m_children.begin(); it != m_children.end(); it++){
    	auto sens = static_cast<ISensor*>(*it);
    	if(sens->canMeasure(t))
			ret |= (1 << sens->address());
    }
    return ret;
}

float SensorHandle::getAverage(SensorValue::Type sensor_t, uint8_t sensors)
{
	float ret = 0.;
	uint8_t divider = 0;
	std::vector<IWeb*>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++){
		auto sens = static_cast<ISensor*>(*it);
		if(((sensors >> sens->address()) & 1) && sens->canMeasure(sensor_t)){
			ret += sens->getMeasurement(sensor_t);
			++divider;
		}
	}
	return (divider>0) ? ( ret / (float)divider ) : 0.;
}

bool SensorHandle::validateSensors()
{
    log_d("Begin I2C scan...");
    uint8_t detectedSensors = getRegisteredSensors();
    bool reloadPage = false;
    for(uint8_t u = 0; u < MAX_SENSORS; u++)
    {
		//Determine sensor and sensor type at the given address
        log_v("TCA PORT #%u", u);
        int8_t i2cAddr = identifySensorByI2CAddress(u);
        
        bool invalid = (i2cAddr == TCA_SELECT_FAILED || i2cAddr == TCA_BUS_NOT_CONNECTED);
        bool sensorAlreadyRegistered = (detectedSensors >> u) & 1;
        
        bool foundNew = !sensorAlreadyRegistered && (!invalid);
        bool lostRegistered  =  sensorAlreadyRegistered && (invalid);
        
        if(foundNew){
			reloadPage = true;
			createSensor(i2cAddr, u, this);
		} else if(lostRegistered){
			reloadPage = true;
			
            std::vector<IWeb*>::iterator it = std::find_if(m_children.begin(), m_children.end(), 
												[u](IWeb* &s) -> IWeb* { return (u == static_cast<ISensor*>(s)->address()) ? s : nullptr; });
			if((*it)){
                delete (*it);
                *it = nullptr;
				m_children.erase(std::remove(m_children.begin(), m_children.end(), nullptr), m_children.end());
			}
		}
    }
    log_d("I2C Scan finished");
    return reloadPage;
}

int8_t SensorHandle::identifySensorByI2CAddress(uint8_t tca_bus)
{
	if(tcaselect(tca_bus) != 0){
		return TCA_SELECT_FAILED;
	}
	
	Wire.beginTransmission(I2C_ADDR_MOISTURE);
	if(! Wire.endTransmission()){
		log_v(" CHIRP ident");
		return I2C_ADDR_MOISTURE;
	}
	Wire.beginTransmission(I2C_ADDR_HTU);
	if(! Wire.endTransmission()){
		log_v(" HTU ident");
		return I2C_ADDR_HTU;
	}
	Wire.beginTransmission(I2C_ADDR_BME);
	if(! Wire.endTransmission()){
		log_v(" BME ident");
		return I2C_ADDR_BME;
	}
	log_v(" No sensor found");
	return TCA_BUS_NOT_CONNECTED;
}

ISensor* SensorHandle::createSensor(int8_t i2cAddr, uint8_t address, IWeb* const parent)
{
	ISensor* sensor = nullptr;
	switch(i2cAddr) {
		case I2C_ADDR_GENERIC:
		sensor = new GenericSensor(address, parent);
		break;
		case I2C_ADDR_MOISTURE:
		sensor = new Chirp_Sensor(address, parent);
		break;
		case I2C_ADDR_HTU:
		sensor = new HTU21D_Sensor(address, parent);
		break;
		case I2C_ADDR_BME:
		sensor = new BME280_Sensor(address, parent);
		break;
		default:
		sensor = nullptr;
		break;
	}
	
	if(sensor){
		log_d("New %s sensor created at %u", sensor->name(), address); 
	}
	
	return sensor;
}

void SensorHandle::update()
{
	if(!interval_timer.hasExpired(SENSOR_UPDATE_INTERVAL/m_children.size())) return;
	
	interval_timer.start();
	
    bool lightOn = !digitalRead(PLED_SENSOR);
    
    IWeb::setFlag(SensorHandle::LIGHT_ON, lightOn);
    
    static std::vector<IWeb*>::iterator it = m_children.begin();
    auto s = static_cast<ISensor*>(*it);
    tcaselect(s->address());
    s->update();
    
    ++it;
    if(it == m_children.end()) it = m_children.begin();
    
	static uint8_t refresh_count = 0;
	if(refresh_count == m_children.size()){
		refresh_count = 0;
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
		
		auto t = getAverage(SensorValue::DEGREE);
		auto h = getAverage(SensorValue::HUMIDITY);
		auto m = getAverage(SensorValue::SOIL_MOISTURE);
		ov_sensor.set(t, h, m);
		
		ov_sensor.update();
		
		if(IWeb::isFlagSet(SensorHandle::ON_LOAD))
		{
			ov_sensor.resetLogs();
	
			IWeb::disableFlag(SensorHandle::ON_LOAD);
			IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
		}
	}
	++refresh_count;
}
