#ifndef PLUGHANDLE_HPP
#define PLUGHANDLE_HPP

#include <Arduino.h>
#include "../web/iweb.hpp"

#include "plugdevice.hpp"

class IPlugHandle : public IWeb
{
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);

protected:
	virtual const char* symbol() const = 0;
	
    IPlugHandle(IWeb* const parent = nullptr);

public:
    bool isAnyActive();
    void update();
};

class PlugHandle12V : public IPlugHandle
{
private:
    static PlugHandle12V* phandle;
	static const char header_symbol[] PROGMEM;
	const char* symbol() const { return header_symbol; }

public:
    static PlugHandle12V* singleton();
};

class PlugHandleRemote : public IPlugHandle
{
private:
    static PlugHandleRemote* phandle;
	static const char header_symbol[] PROGMEM;
	const char* symbol() const { return header_symbol; }
	
public:
    static PlugHandleRemote* singleton();
};

#endif
