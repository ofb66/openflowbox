#ifndef PLUGSWITCH_HPP
#define PLUGSWITCH_HPP

#include <Arduino.h>

#include "sensorvalue.hpp"
#include "../web/iweb.hpp"
#include "../persistence/serializable.hpp"
using namespace Persistence;

class IPlugSwitch : public IWeb, public ISerializable {
protected:
	
	IPlugSwitch(IWeb* const parent = nullptr) : IWeb(parent) {}
public:	
	enum {
		ENABLED = IWeb::LAST,
		ACTIVE  = IWeb::LAST << 1
	};
	
	virtual ~IPlugSwitch(){}

	virtual int8_t isBeyondBoundaries() { return 0x7F; }
	bool isEnabled() const { return IWeb::isFlagSet(IPlugSwitch::ENABLED); }
	bool isActive() const { return IWeb::isFlagSet(IPlugSwitch::ACTIVE | IPlugSwitch::ENABLED, false); }
	
	virtual void update() = 0;
};

class IPlugSwitchTime : public IPlugSwitch
{
protected:
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	uint32_t m_offtime;
	uint32_t m_ontime;
	
	IPlugSwitchTime(IWeb* const parent = nullptr) : IPlugSwitch(parent), m_offtime(0), m_ontime(0) {}
public:
	virtual ~IPlugSwitchTime(){}
	
    //ISerializable
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
	
};

class PlugSwitchTimer : public IPlugSwitchTime
{
private:
	static const char s_header[] PROGMEM;
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
	void checkTimeOnChange();
public:
	PlugSwitchTimer(IWeb* const parent = nullptr) : IPlugSwitchTime(parent) {}
	
	virtual void handleData(uint8_t varId, void* const &payload) override;
	virtual void update();
};

class PlugSwitchInterval: public IPlugSwitchTime
{
private:
	static const char s_header[] PROGMEM;
	
	Utils::Timer16 m_intervalTimer;
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
public:
	PlugSwitchInterval(IWeb* const parent = nullptr) : IPlugSwitchTime(parent) {}
	
	virtual void handleData(uint8_t varId, void* const &payload) override;
	virtual void update();
};

class PlugSwitchSensor : public IPlugSwitch
{
protected:
	static const char s_header[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	float setpoint_off;
	float setpoint_on;
	
	SensorValue sv_current;
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
public:	
	PlugSwitchSensor(float setpOff, float setpOn, SensorValue::Type t, IWeb* const parent = nullptr);
	virtual ~PlugSwitchSensor() {}
	
	virtual void update();
	virtual int8_t isBeyondBoundaries() override;
	
	
	virtual void handleData(uint8_t varId, void* const &payload) override;
	
    //ISerializable
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
};

#endif
