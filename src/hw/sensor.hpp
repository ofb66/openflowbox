#ifndef SENSOR_HPP
#define SENSOR_HPP

#include <Arduino.h>
#include "../web/iweb.hpp"

#include <SparkFunHTU21D.h>
#include <BME280I2C.h>

#include "sensorlog.hpp"
#include "sensorvalue.hpp"

#define I2C_ADDR_GENERIC	0x00
#define I2C_ADDR_MOISTURE	0x20
#define I2C_ADDR_HTU    	0x40
#define I2C_ADDR_BME    	0x76

class ISensor : public IWeb
{
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	
	static char headerStr[250];
	static void createHeaderString(ISensor* sensor);
		
	static char jsonStr[60];
	static void createJsonString(ISensor* sensor);
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
	String getSensorValueUI();
public:
	/*
	enum Type{
		NONE,
		GENERIC,
		HTU21D_t,
		BME280_t,
		CHIRP_t
	} const stype;
	*/
	
	/*
    enum
    {
        TEMP,
        HUMI,
        PRESS,
        SOIL
    };
	*/
	
    virtual ~ISensor();

    inline uint8_t address() const { return m_address; }
    void resetLogs();
    void updateLogs();
    bool update();
    
    bool  canMeasure(SensorValue::Type t);
    float getMeasurement(SensorValue::Type t);
	
    inline float const * getLogs(uint8_t s_unit) const { return (s_unit < m_len) ? m_logs[s_unit]->stats() : nullptr; }
    inline bool isAvailable() const { return !m_errorCount; }
    inline uint8_t errors() const { return m_errorCount; }
    
	virtual void handleData(uint8_t varId, void* const &payload) override;
	
	virtual const char* color() = 0;
	virtual const char* name() = 0;

protected:		
    const uint8_t m_len;
    
    uint8_t m_address;
    SensorValue* m_measurement;
    uint8_t m_errorCount;

    SensorLog **m_logs;

    ISensor(uint8_t address, /*Type t,*/ uint8_t mLen, IWeb* const parent = nullptr);
    virtual bool read(SensorValue* measurement) = 0;

};

class GenericSensor : public ISensor
{
protected:
	virtual bool read(SensorValue* measurement) { return true; }
public:
	GenericSensor(uint8_t address, IWeb* const parent = nullptr);
	virtual ~GenericSensor() {}
	
    virtual const char* color() { return PSTR("#ffffff"); }
    virtual const char* name() { return PSTR("GEN"); }
    
	inline void set(float temp, float humi, float moisture){ 
		m_measurement[0].value = temp; 
		m_measurement[1].value = humi; 
		m_measurement[2].value = moisture;
	}
};

class HTU21D_Sensor : public ISensor
{
private:
	HTU21D m_sensor;
    
protected:
    virtual bool read(SensorValue* measurement);
public:
    HTU21D_Sensor(uint8_t address, IWeb* const parent  = nullptr);
    virtual ~HTU21D_Sensor(){}
    
    virtual const char* color() { return PSTR("#ff1a1a"); }
    virtual const char* name() { return PSTR("HTU"); }
};

class BME280_Sensor : public ISensor
{
private:
	BME280I2C m_sensor;
    
protected:
    virtual bool read(SensorValue* measurement);
public:
    BME280_Sensor(uint8_t address, IWeb* const parent  = nullptr);
    virtual ~BME280_Sensor(){}
    
    virtual const char* color() { return PSTR("#800040"); }
    virtual const char* name() { return PSTR("BME"); }
};

class Chirp_Sensor : public ISensor
{
private:
	enum State {
		START_LIGHT,
		GET_LIGHT,
		GET_TEMPERATURE,
		GET_CAPACITANCE
	} state;
	
	enum I2C_Register {
		CAPACITANCE = 0,
		LIGHT_REQ	= 3,
		LIGHT		= 4,
		TEMPERATURE = 5,
		RESET 		= 6,
	};
	long map(long val, long min, long max, long minOut, long maxOut);
	int writeI2CRegister8bit(int reg);
	uint16_t readI2CRegister16bit(int reg);
protected:
	virtual bool read(SensorValue* measurement);
public:
    Chirp_Sensor(uint8_t address, IWeb* const parent  = nullptr);
    virtual ~Chirp_Sensor(){}
    
    virtual const char* color() { return PSTR("#b37700"); }
    virtual const char* name() { return PSTR("CHIRP"); }
};


#endif
