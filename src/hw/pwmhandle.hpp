#ifndef PWMHANDLE_HPP
#define PWMHANDLE_HPP

#include <Arduino.h>
#include "../web/iweb.hpp"

#include "pwmdevice.hpp"

class PWMHandle : public IWeb
{
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	
    static PWMHandle* phandle;

    PWMHandle(IWeb* const parent = nullptr);
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);

public:
    static PWMHandle* singleton();

    void update();
};

#endif
