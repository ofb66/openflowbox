/*generated file sensorvalue.cpp*/
#include "sensorvalue.hpp"


const char* SensorValue::unit(SensorValue::Type type) {
	switch(type){
		case Type::DEGREE:
			return PSTR("°C");
		case Type::HUMIDITY:
		case Type::SOIL_MOISTURE:
			return PSTR("%");
		case Type::PRESSURE:
			return PSTR("bar");
		case Type::LIGHT:
			return PSTR("lm");
		default:
			return PSTR(" ");
	}
}

const char* SensorValue::symbol(SensorValue::Type type) {
	switch(type){
		case Type::DEGREE:
			return PSTR("thermometer-half");
		case Type::HUMIDITY:
			return PSTR("tint");
		case Type::SOIL_MOISTURE:
			return PSTR("water");
		case Type::PRESSURE:
			return PSTR("cloud-showers-heavy");
		case Type::LIGHT:
			return PSTR("lightbulb");
		default:
			return PSTR(" ");
	}
}


//EOF
