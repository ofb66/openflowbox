#include "sensor.hpp"

void ISensor::createHeaderString(ISensor* sensor)
{
	int indx = 0;
	for(uint8_t u = 0; u < sensor->m_len; u++){
		if(u > 0){
			indx += snprintf(&headerStr[indx], 4, PSTR("|"));
		}
		indx += snprintf(&headerStr[indx], 80, PSTR("<i class='fas fa-%s'></i> <span id='%u'>%5.2f</span>%s"), 
							SensorValue::symbol(sensor->m_measurement[u].type),
							sensor->createId(u+1), 
							sensor->m_measurement[u].value, 
							SensorValue::unit(sensor->m_measurement[u].type)
						);
	}
	headerStr[indx] = 0;
}

void ISensor::createJsonString(ISensor* sensor)
{
	jsonStr[0] = '{';
	int indx = 1;
	for(uint8_t u = 0; u < sensor->m_len; u++){
		if(u > 0){
			jsonStr[indx++] = ',';
		}
		indx += snprintf(&jsonStr[indx], 20, PSTR("\"%u\":%5.2f"), sensor->createId(u+1), sensor->m_measurement[u].value);
	}
	jsonStr[indx] = '}';
	jsonStr[indx+1] = 0;
}
char ISensor::headerStr[250] = {0};
char ISensor::jsonStr[60] = "{}";

const char ISensor::s_header[] PROGMEM = R"=====(				<article>
					<button type='button' class='collapsible'>				
					<h3>
					<span class='fa-layers fa-fw'>
						<i class='fas fa-microchip' style='color:%s'></i>
						<i class='fas fa-%s' data-fa-transform='shrink-5 up-6 left-10'></i>
						<span class='fa-layers-text' data-fa-transform='shrink-5 up-6 right-10' style='font-weight:800'>%u</span>
						<span class='fa-layers-text' data-fa-transform='shrink-7 down-5 right-8' style='font-weight:700'>%s</span>
					</span>
					</h3>
					<h4>
					<div style='padding-left:1px;padding-right:1px;'>
					%s
					</div>
					</h4>
					</button>
					<div class='content'>
					<table>
					<tr><th>Min</th><th>&#8960;</th><th>Max</th>
)=====";

const char ISensor::s_json_dyn[] PROGMEM = R"=====({
"%u":%5.2f
})=====";

const char ISensor::s_footer[] PROGMEM = R"=====(				</table>	
				<button type='button' onclick='sendClick(this);' id='%u' class='btn widebtn'>
					<i class='fas fa-sync-alt'></i>
				</button>		
				</div>
				</article>
)=====";

size_t ISensor::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 260 + strlen(color()) + strlen(name());
		case StrObject::FOOTER:
		return strlen_P(s_footer) + 3;
		case StrObject::JSON_DYN:
		return 60;
		default:
		return 0;
	}
}

size_t ISensor::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
    switch(so){
        case StrObject::HEADER:
        createHeaderString(this);
        return snprintf_P((char*)buffer, len, s_header,
							color(), 
							(canMeasure(SensorValue::Type::SOIL_MOISTURE)) ? PSTR("water") : PSTR("wind"),
							m_address,
							name(),  
							headerStr
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer,
							createId(2)
						);
        break;
        
        case StrObject::JSON_DYN:
        createJsonString(this);
        return snprintf((char*)buffer, len, jsonStr);
        break;
        default:
        return 0;
    }
}

ISensor::~ISensor()
{
    delete [] m_measurement;
    for(uint8_t u = 0; u < m_len; u++){
        delete m_logs[u];
    }
    delete [] m_logs;
}

void ISensor::resetLogs()
{
	for(uint8_t indx = 0; indx < m_len; indx++){
		m_logs[indx]->reset();
	}
}

void ISensor::updateLogs()
{
	for(uint8_t indx = 0; indx < m_len; indx++){
		m_logs[indx]->update();
	}
}

bool ISensor::update()
{	
	if(!read(m_measurement)){
		return false;
	}
	
	for(uint8_t indx = 0; indx < m_len; indx++){
		if(m_logs[indx]->hasBegun()){
			m_logs[indx]->update();	
		} else {
			m_logs[indx]->reset();
		}
	}
	return true;
}

bool ISensor::canMeasure(SensorValue::Type t)
{
	if(m_errorCount) return false;
	for(uint8_t u = 0; u < m_len; u++){
		if(m_measurement[u].type == t){
			return true;
		}
	}
	return false;	
}

float ISensor::getMeasurement(SensorValue::Type t){
	for(uint8_t u = 0; u < m_len; u++){
		if(m_measurement[u].type == t){
			return m_measurement[u].value;
		}
	}
	return NAN;
}

void ISensor::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 2){
		resetLogs();
	}	
}

ISensor::ISensor(uint8_t address, uint8_t mLen, IWeb* const parent)
:
IWeb(parent),
m_len(mLen),
m_address(address),
m_measurement(new SensorValue[mLen]),
m_errorCount(0),
m_logs(new SensorLog*[2])
{
    for(uint8_t u = 0; u < mLen; u++){
		m_logs[u] = new SensorLog(&m_measurement[u], this);
		m_measurement[u].value = 0.;
	}
}

GenericSensor::GenericSensor(uint8_t address, IWeb* const parent)
:
ISensor(address, 3, parent)
{
	m_measurement[0].type = SensorValue::Type::DEGREE;
	m_measurement[1].type = SensorValue::Type::HUMIDITY;
	m_measurement[2].type = SensorValue::Type::SOIL_MOISTURE;
}

HTU21D_Sensor::HTU21D_Sensor(uint8_t address, IWeb* const parent)
:
ISensor(address, 2, parent)
{
    m_sensor.begin();
	m_sensor.setResolution(USER_REGISTER_RESOLUTION_RH11_TEMP11);
	
	m_measurement[0].type = SensorValue::Type::DEGREE;
	m_measurement[1].type = SensorValue::Type::HUMIDITY;
}

bool HTU21D_Sensor::read(SensorValue* measurement)
{
    float f_tmp[2];

    f_tmp[0] = m_sensor.readTemperature();
    f_tmp[1] = m_sensor.readHumidity();
    
    if(f_tmp[0] > 900 || f_tmp[1] > 900)
    {
		++m_errorCount;
		return false;
    }
    
    m_errorCount = 0;
    if(measurement[0].value != f_tmp[0] || measurement[1].value != f_tmp[1]){
		measurement[0].value = f_tmp[0];
		measurement[1].value = f_tmp[1];
			
		IWeb::enableFlag(IWeb::HAS_CHANGE);
	}
    return true;
}


BME280_Sensor::BME280_Sensor(uint8_t address, IWeb* const parent)
:
ISensor(address, 3, parent)
{
    m_sensor.begin();
    
	m_measurement[0].type = SensorValue::Type::DEGREE;
	m_measurement[1].type = SensorValue::Type::HUMIDITY;
	m_measurement[2].type = SensorValue::Type::PRESSURE;
}

bool BME280_Sensor::read(SensorValue* measurement)
{
    float f_tmp[m_len];
    m_sensor.read(f_tmp[2], f_tmp[0], f_tmp[1], BME280::TempUnit_Celsius, BME280::PresUnit_bar );
    if(isnan(f_tmp[0]) || isnan(f_tmp[1])){
		++m_errorCount;
		return false;
	}
    m_errorCount = 0;
        
    if( measurement[0].value != f_tmp[0] || measurement[1].value != f_tmp[1]){
		measurement[0].value = f_tmp[0];
		measurement[1].value = f_tmp[1];
		measurement[2].value = f_tmp[2];
		
		IWeb::enableFlag(IWeb::HAS_CHANGE);
	}
    return true;
}

int Chirp_Sensor::writeI2CRegister8bit(int reg)
{
	Wire.beginTransmission(I2C_ADDR_MOISTURE);
	Wire.write(reg);
	return Wire.endTransmission();	
}

uint16_t Chirp_Sensor::readI2CRegister16bit(int reg)
{
	Wire.beginTransmission(I2C_ADDR_MOISTURE);
	Wire.write(reg);
	Wire.endTransmission();
	
	delay(10);	
	Wire.requestFrom(I2C_ADDR_MOISTURE, 0x02);
	uint16_t rec = Wire.read() << 8 | Wire.read();
	return rec;
}

long Chirp_Sensor::map(long val, long min, long max, long minOut, long maxOut)
{
	return (val - min) * (maxOut - minOut) / (max - min) + minOut;
}

bool Chirp_Sensor::read(SensorValue* measurement)
{	
	uint16_t reading = readI2CRegister16bit(I2C_Register::CAPACITANCE);
	if(reading == 0xFFFF){ 
		++m_errorCount;
		return false;
	}
	float measure = (float)map(reading, 290, 570, 0, 100);
	if(measure < 0){
		++m_errorCount;
		return false;
	}
	
	m_errorCount = 0;
	
	if(measure != measurement[0].value){
		IWeb::enableFlag(IWeb::HAS_CHANGE);
		measurement[0].value = measure;
	}
	return true;
}

Chirp_Sensor::Chirp_Sensor(uint8_t address, IWeb* const parent)
 : ISensor(address, /*Type::CHIRP_t,*/ 1, parent)
{	
	writeI2CRegister8bit(I2C_Register::RESET);
	
	m_measurement[0].type = SensorValue::Type::SOIL_MOISTURE;
}
