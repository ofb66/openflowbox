#ifndef DEVICETACHO_HPP
#define DEVICETACHO_HPP

#include <Arduino.h>
#include "../utils/timer.hpp"

#define SECONDS_PER_MINUTES			60
#define INTERRUPTS_PER_REVOLUTION	2
#define INTERVAL_MAGIC_NUMBER		SECONDS_PER_MINUTES / INTERRUPTS_PER_REVOLUTION

class DeviceTacho {
private:
	uint8_t m_pin;
	uint16_t m_rpm;
	
public:
	friend void updateTacho(DeviceTacho& tacho);

	DeviceTacho(uint8_t pin) : m_pin(pin), m_rpm(0) {}
	
	Utils::Timer16 updateTimer;
	
	uint8_t pin() const { return m_pin; }
	uint16_t rpm() const { return m_rpm; }
};


void initTacho(DeviceTacho &tacho);
void updateTacho(DeviceTacho& tacho);


#endif
