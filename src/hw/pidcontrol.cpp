#include "pidcontrol.hpp"

#include "sensorhandle.hpp"
#include "sensor.hpp"
#include "pidcontrolsensor.hpp"
#include "../persistence/serializablepod.hpp"

using namespace Persistence;

const char PidControl::s_header[] PROGMEM = R"=====(			<button type='button' class='collapsible'>
				<i class='fas fa-%s'></i>
			</button>
			<div class='content' id='%u'>
				<p class='relative'>
					<input type='checkbox' onclick='sendBool(this)' class='allb ccb standardcb' id='%u' %s><label for='%u'><span class='ui'></span></label>
					<span id='%u'>0</span>%%
				</p>
				<br>
				<hr>
				<div>
)=====";

const char PidControl::s_footer[] PROGMEM = R"=====(		</div>
				<hr>
				<p>
					<i class='fas fa-balance-scale'></i> <span id='%u'>%04.2f</span>
				</p>
				<p>
					<input type='range' onchange='sendDouble(this);' max='1' id='%u' value='%04.2f' step='0.01'>
				</p>
				<p>
					<span class='flash'><span id='%u' data-diff='%05.2f'>%05.2f</span>%s</span>
					<i class='fas fa-caret-left'></i> <i class='fas fa-caret-right'></i> 
					<span id='%u' class='comparator'>%05.2f</span>%s
				</p>
				<p>
					<input type='range' onchange='sendDouble(this);' min='%u' max='%u' value='%.2f' step='0.5' id='%u'>
				</p>
				<button type='button' class='collapsible'><i class='fas fa-sliders-h'></i></button>
				<subarticle class='subcontent'>
				<p>
				%s <span id='%u'>%.2f</span>
				<input min='-5' max='5' onchange='sendDouble(this);' type='range' step='0.01' id='%u' value='%.2f'>
				</p>
				<p>
				%s <span id='%u'>%.2f</span>
				<input min='-5' max='5' onchange='sendDouble(this);' type='range' step='0.01' id='%u' value='%.2f'>
				</p>
				<p>
				%s <span id='%u'>%.2f</span>
				<input min='-5' max='5' onchange='sendDouble(this);' type='range' step='0.01' id='%u' value='%.2f'>
				</p>
				</subarticle>
			</div>
)=====";

const char PidControl::s_json_dyn[] PROGMEM = R"=====({
"%u":%u,
"%u":%5.2f
})=====";

const char PidControl::s_json_ui[] PROGMEM = R"=====({
"%u":%u,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%4.2f,
"%u":%4.2f
})=====";

size_t PidControl::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 50;
		case StrObject::FOOTER:
		return strlen_P(s_footer) + 100;
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 20;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 60;
		default: 
		return 0;
	}
}

size_t PidControl::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							SensorValue::symbol(m_sensortype),
							webId(), createId(0), (IWeb::isFlagSet(IDevice::ENABLED)) ? PSTR("checked") : PSTR(""),
							createId(0), createId(1)
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer,
							createId(2), m_weight,
							createId(3), m_weight,
							createId(6), (m_sensortype == SensorValue::Type::DEGREE) ? 0.5 : 2.5, m_lastInput, 
							SensorValue::unit(m_sensortype),
							createId(7), m_target, SensorValue::unit(m_sensortype),
							(m_sensortype == SensorValue::Type::DEGREE) ? 10 : 30,
							(m_sensortype == SensorValue::Type::DEGREE) ? 45 : 80,
							m_target,
							createId(8),
							PSTR("Kp"), createId(9), m_params[0], createId(10), m_params[0],
							PSTR("Ki"), createId(11), m_params[1], createId(12), m_params[1],
							PSTR("Kd"), createId(13), m_params[2], createId(14), m_params[2]
						);
		break;
        case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn,
							createId(1), PWM_DISPLAY(m_output),
							createId(6), IWeb::isFlagSet(IDevice::ENABLED) ? m_lastInput : m_target
						);
        break;
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0), (uint8_t)IWeb::isFlagSet(IDevice::ENABLED),
							/*
							createId(2), m_sensors & 1,
							createId(3), m_sensors & 2,
							createId(4), m_sensors & 4,
							createId(5), m_sensors & 8,
							*/ 
							createId(7), m_target,
							createId(8), m_target,
							createId(9),  m_params[0], createId(10), m_params[0],
							createId(11), m_params[1], createId(12), m_params[1],
							createId(13), m_params[2], createId(14), m_params[2],
							createId(2), m_weight,
							createId(3), m_weight
						);
        break;
		default: 
		return 0;
    }
}

PidControl::PidControl(
    float target,
    float kp,
    float ki,
    float kd,
    const SensorValue::Type sensortype,
    uint8_t const * const vmin,
    IWeb* const parent)
    :
    IDevice(parent), ISerializable(),
    m_lastInput(0),
    m_target(target),
    m_params{kp, ki, kd},
    m_sensortype(sensortype),
    m_sensors(0),
    m_weight(1),
    m_vmin(vmin)
{
	sample_timer.start();
	m_sensors = SensorHandle::singleton()->getAvailableSensors(m_sensortype);
	
	for(uint8_t u = 0; u < 8; u++){
		if(m_sensors & (1 << u)){
			new PidControlSensor(u, this);
		}
	}
}

void PidControl::handleData(uint8_t varId, void* const &payload)
{
	switch(varId){
		case 0:
		IWeb::setFlag(IDevice::ENABLED, *(bool*)payload);
		if(m_sensors == 0) IWeb::disableFlag(IDevice::ENABLED);
		break;
		case 3:
		m_weight = *(float*)payload;
		break;
		case 8:
		m_target = *(float*)payload;
		break;
		case 10:
		m_params[0] = *(float*)payload;
		break;
		case 12:
		m_params[1] = *(float*)payload;
		break;
		case 14:
		m_params[2] = *(float*)payload;
		break;
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
}

//IDevice
void PidControl::update()
{	
	if(!IWeb::isFlagSet(IDevice::ENABLED)){
        return;
	}
    if(!sample_timer.hasExpired(SAMPLE_TIME)) { 
		return;
	}

    sample_timer.start();
    
    float input = SensorHandle::singleton()->getAverage(m_sensortype, m_sensors);

    float rangemin = (*m_vmin > 0) ? (*m_vmin) - 1 : 0;
    
    /*Compute all the working error variables*/
    float error = m_target - input;
    float dInput = (input - m_lastInput);
    m_outputSum += (m_params[INDX_KI] * error);

    if(m_outputSum > (PWM_RANGE_MAX)) m_outputSum = PWM_RANGE_MAX;
    else if(m_outputSum < rangemin) m_outputSum = rangemin;

    /*Add Proportional on Error, if P_ON_E is specified*/
    m_output = m_params[INDX_KP] * error;

    /*Compute Rest of PID Output*/
    m_output += m_outputSum - m_params[INDX_KD] * dInput;

    if(m_output > (PWM_RANGE_MAX)) m_output = PWM_RANGE_MAX;
    else if(m_output <= rangemin) m_output = 0;

    /*Remember some variables for next time*/
    m_lastInput = input;
	IWeb::enableFlag(IWeb::HAS_CHANGE);
}

//ISerializable
size_t PidControl::size()
{
    return  SerializablePOD<uint8_t>::size(m_sensors) +
			SerializablePOD<float>::size(m_params, 5) +
			SerializablePOD<uint16_t>::size(m_flags);
}
uint8_t* PidControl::serialize(uint8_t *eptr)
{
	eptr = SerializablePOD<uint8_t>::serialize(eptr, m_sensors);
	eptr = SerializablePOD<float>::serialize(eptr, m_target);
	eptr = SerializablePOD<float>::serialize(eptr, m_weight);
	eptr = SerializablePOD<float>::serializeArray(eptr, m_params, 3);
	eptr = SerializablePOD<uint16_t>::serialize(eptr, m_flags);
    return eptr;
}
const uint8_t* PidControl::deserialize(const uint8_t *eptr)
{	
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, m_sensors);
	eptr = SerializablePOD<float>::deserialize(eptr, m_target);
	eptr = SerializablePOD<float>::deserialize(eptr, m_weight);
	eptr = SerializablePOD<float>::deserializeArray(eptr, m_params, 3);
	eptr = SerializablePOD<uint16_t>::deserialize(eptr, m_flags);
	
    std::vector<IWeb*>::iterator it;
    for(it = m_children.begin(); it != m_children.end(); it++){
		PidControlSensor* pcs = static_cast<PidControlSensor*>(*it);
		pcs->setFlag(PidControlSensor::ENABLED, (m_sensors >> pcs->address()) & 1);
	}
	if(m_sensors == 0){
		IWeb::disableFlag(IDevice::ENABLED);
	}
	
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
    return eptr;
}

void PidControl::updateSensorStatus(uint8_t saddress, bool enable)
{
	if(enable){
		if(m_sensors == 0){
			IWeb::enableFlag(IDevice::ENABLED | IWeb::HAS_UI_CHANGE);
		}
		m_sensors |= (1 << saddress);
	} else {
		m_sensors &= ~(1 << saddress);
		if(m_sensors == 0){
			IWeb::disableFlag(IDevice::ENABLED);
			IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
			m_output = 0;
		}
	}
}
