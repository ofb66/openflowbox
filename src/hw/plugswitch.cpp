#include "plugswitch.hpp"

#include "../web/devicetime.hpp"
#include "sensorhandle.hpp"
#include "../persistence/serializablepod.hpp"

using namespace Persistence;

const char IPlugSwitchTime::s_json_dyn[] PROGMEM = R"=====({
"%u":%u
})=====";

const char IPlugSwitchTime::s_json_ui[] PROGMEM = R"=====({
"%u":%u,
"%u":%u,
"%u":%u,
"AT":[{"id":%u,"att":"data-ison","val":%u}]
})=====";

//ISerializable
size_t IPlugSwitchTime::size() { 
	return 	SerializablePOD<uint16_t>::size(m_flags) + 
			SerializablePOD<uint32_t>::size(m_ontime) 	+ 
			SerializablePOD<uint32_t>::size(m_offtime); 
}
uint8_t* IPlugSwitchTime::serialize(uint8_t *eptr) {
	eptr = SerializablePOD<uint16_t>::serialize(eptr, m_flags);
	eptr = SerializablePOD<uint32_t>::serialize(eptr, m_ontime);
	eptr = SerializablePOD<uint32_t>::serialize(eptr, m_offtime);
	return eptr; 
}
const uint8_t* IPlugSwitchTime::deserialize(const uint8_t *eptr) {
	eptr = SerializablePOD<uint16_t>::deserialize(eptr, m_flags);
	eptr = SerializablePOD<uint32_t>::deserialize(eptr, m_ontime);
	eptr = SerializablePOD<uint32_t>::deserialize(eptr, m_offtime);
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE); 
	return eptr; 
}


const char PlugSwitchTimer::s_header[] PROGMEM = R"=====(<hr>
				<div id='%u' class='bar timer'>
					<p class='relative'><input type='checkbox' id='%u' onclick='sendBool(this)' class='allb ccb stopwatchbtn'><label for='%u'><span class='ui'></span></label></p>
					<table>
						<tr><th>OFF</th><th>ON</th></tr>
						<tr>
							<td>
								<input type='time' id='%u' class='time nOff' onchange='updateTimeBar(document.getElementById("%u"), -1); sendTime(this);'>
							</td>
							<td>
								<input type='time' id='%u' class='time nOn' onchange='updateTimeBar(document.getElementById("%u"), -1); sendTime(this);'>
							</td>
						</tr>
					</table>
					<div class='progressbar'>
						<div class='progress' style='width: 0px'>
							<div class='progresslabel ptLabel' data-ison='0' id='%u' style='width: fit-content'></div>
						</div>
					</div>
				</div>		
)=====";

size_t PlugSwitchTimer::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
			return strlen_P(s_header) + 20;
		case StrObject::JSON_DYN:
			return strlen_P(s_json_dyn) + 15;
		case StrObject::JSON_UI:
			return strlen_P(s_json_ui) + 40;
		default:
			return 0;
	}
}

size_t PlugSwitchTimer::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{	
	uint32_t currDisT = (isActive()) ? m_offtime : m_ontime;
	uint32_t currEpTDay = DeviceTime::singleton()->getEpochTime()%86400;
	
	if(currEpTDay > currDisT){
		currDisT += 86400;
	}
	currDisT -= currEpTDay;
	
	switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							webId(), createId(0), createId(0),
							createId(1), webId(),
							createId(2), webId(),
							createId(3)
						);
        break;
        case StrObject::JSON_DYN:
		return snprintf_P((char*)buffer, len, s_json_dyn, createId(3), currDisT);
        break;
		case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0), (uint8_t)IWeb::isFlagSet(IPlugSwitch::ENABLED),
							createId(1), m_offtime,
							createId(2), m_ontime,
							createId(3), (uint8_t)isActive()
						);
		break;
        default:
        return 0;
    }
}

void PlugSwitchTimer::checkTimeOnChange()
{
	auto time = DeviceTime::singleton()->getEpochTime() % 86400;

	bool enable = (m_offtime < m_ontime && time > m_ontime) ||
				  (m_offtime > time && m_offtime < m_ontime) ||
				  (m_offtime > time && time > m_ontime);		
	IWeb::setFlag(IPlugSwitch::ACTIVE, enable);
}

void PlugSwitchTimer::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 0){
		bool enable = *(bool*)payload;
		IWeb::setFlag((IPlugSwitch::ENABLED|IWeb::INTERVAL), enable);
	} else if(varId == 1) {
		m_offtime = *(uint32_t*)payload;
		checkTimeOnChange();
	} else if(varId == 2) {
		m_ontime = *(uint32_t*)payload;
		checkTimeOnChange();
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
}

void PlugSwitchTimer::update()
{
	if(!isEnabled()) return;
	
	auto currTime = DeviceTime::singleton()->getEpochTime() % 86400;
	
	if(currTime == m_offtime) IWeb::disableFlag(IPlugSwitch::ACTIVE);
	else if(currTime == m_ontime) IWeb::enableFlag(IPlugSwitch::ACTIVE);
}


const char PlugSwitchInterval::s_header[] PROGMEM = R"=====(<hr>
				<div id='%u' class='bar'>
					<p class='relative'><input type='checkbox' id='%u' onclick='sendBool(this)' class='allb ccb hourglassbtn'><label for='%u'><span class='ui'></span></label></p>
					<table>
						<tr><th>OFF</th><th>ON</th></tr>
						<tr>
							<td>
								<input type='text' placeholder='WDhhmmss' style='width: 6em; text-align:center;' id='%u' class='dtime2 nOff' onchange='updateIntervalBar(document.getElementById("%u"), -1); sendTime(this);'>
							</td>
							<td>
								<input type='text' placeholder='WDhhmmss' style='width: 6em; text-align:center;' id='%u' class='dtime2 nOn' onchange='updateIntervalBar(document.getElementById("%u"), -1); sendTime(this);'>
							</td>
						</tr>
					</table>
					<div class='progressbar'>
						<div class='progress' style='width: 0px'>
							<div class='progresslabel piLabel' data-ison='0' id='%u' style='width: fit-content'></div>
						</div>
					</div>
				</div>		
)=====";

size_t PlugSwitchInterval::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
			return strlen_P(s_header) + 20;
		case StrObject::JSON_DYN:
			return strlen_P(s_json_dyn) + 15;
		case StrObject::JSON_UI:
			return strlen_P(s_json_ui) + 40;
		default:
			return 0;
	}
}

size_t PlugSwitchInterval::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	
	switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							webId(), createId(0), createId(0),
							createId(1), webId(),
							createId(2), webId(),
							createId(3)
						);
        break;
        case StrObject::JSON_DYN:
		return snprintf_P((char*)buffer, len, s_json_dyn, 
								createId(3), m_intervalTimer.countdown((isActive()) ? m_ontime*1000 : m_offtime*1000)/1000);
        break;
		case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0), (uint8_t)IWeb::isFlagSet(IPlugSwitch::ENABLED),
							createId(1), m_offtime,
							createId(2), m_ontime,
							createId(3), (uint8_t)isActive()
						);
		break;
        default:
        return 0;
    }
}

void PlugSwitchInterval::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 0){
		bool enable = *(bool*)payload;
		IWeb::setFlag((IPlugSwitch::ENABLED|IWeb::INTERVAL), enable);
		if(enable && m_ontime > 0 && m_offtime > 0){
			m_intervalTimer.start();
		}
	} else if(varId == 1) {
		m_offtime = *(uint32_t*)payload;
	} else if(varId == 2) {
		m_ontime = *(uint32_t*)payload;
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
}

void PlugSwitchInterval::update()
{
	if(!isEnabled()) return;
	
	if(!m_intervalTimer.isRunning()) m_intervalTimer.start();
	
	uint32_t time = (isActive()) ? m_ontime : m_offtime;
	
	if(m_intervalTimer.hasExpired(time*1000)){
		m_intervalTimer.start();
		IWeb::setFlag(IPlugSwitch::ACTIVE, !isActive());
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	}
}


const char PlugSwitchSensor::s_header[] PROGMEM = R"=====( <hr>
				<div id='%u' class='bar'>
					<p class='relative'><input type='checkbox' id='%u' onclick='sendBool(this)' class='allb ccb %sbtn'><label for='%u'><span class='ui'></span></label></p>
					<table>
						<tr><th>OFF</th><th>ON</th></tr>
						<tr>
							<td>
							<button type='button' name='numCntDown' class='progressin progressbtn'><i class='fas fa-minus'></i></button><!--
						 --><input type='number' class='progressin progressinput nOff' value='30' id='%u' step='0.5' min='%u' max='%u' onchange='updateSensorBar(document.getElementById("%u"), -1);sendDouble(this);'><!--
						 --><button type='button' name='numCntUp' class='progressin progressbtn'><i class='fas fa-plus'></i></button>
							</td>
							<td>
							<button type='button' name='numCntDown' class='progressin progressbtn'><i class='fas fa-minus'></i></button><!--
						 --><input type='number' class='progressin progressinput nOn' value='35' id='%u' step='0.5' min='%u' max='%u' onchange='updateSensorBar(document.getElementById("%u"), -1);sendDouble(this);'><!--
						 --><button type='button' name='numCntUp' class='progressin progressbtn'><i class='fas fa-plus'></i></button>
							</td>
						</tr>
					</table>
					<div class='progressbar'>
						<div class='progress backtransition' style='width: 0px'>
							<label class='progresslabel'><span id='%u' class='pLabel'>35</span>%s</label>
						</div>
					</div>
				</div>
)=====";

const char PlugSwitchSensor::s_json_dyn[] PROGMEM = R"=====({
"%u":%5.2f
})=====";

const char PlugSwitchSensor::s_json_ui[] PROGMEM = R"=====({
"%u":%u,
"%u":%5.2f,
"%u":%5.2f
})=====";

size_t PlugSwitchSensor::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
			return strlen_P(s_header) + 40 + strlen(SensorValue::symbol(sv_current.type));
		case StrObject::JSON_DYN:
			return strlen_P(s_json_dyn) + 10;
		case StrObject::JSON_UI:
			return strlen_P(s_json_ui) + 20;
		default:
			return 0;
	}
}

size_t PlugSwitchSensor::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{	
	const size_t symstrlen = strlen(SensorValue::symbol(sv_current.type));
	char sym[symstrlen+1];
	uint8_t lower_limit = (sv_current.type == SensorValue::Type::DEGREE) ? 10 : 25;
	uint8_t upper_limit = (sv_current.type == SensorValue::Type::DEGREE) ? 45 : 90;
	switch(so){
        case StrObject::HEADER:
        
			strcpy(sym, SensorValue::symbol(sv_current.type));
			
			return snprintf_P((char*)buffer, len, s_header,
								webId(), createId(0), sym, createId(0),
								createId(1), lower_limit, upper_limit, webId(),
								createId(2), lower_limit, upper_limit, webId(),
								createId(3), SensorValue::unit(sv_current.type)
							);
        break;
        case StrObject::JSON_DYN:
			return snprintf_P((char*)buffer, len, s_json_dyn, 
								createId(3), sv_current.value
							);
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0), (uint8_t)IWeb::isFlagSet(IPlugSwitch::ENABLED),
							createId(1), setpoint_off,
							createId(2), setpoint_on
						);
        default:
        return 0;
    }
}

PlugSwitchSensor::PlugSwitchSensor(float setpOff, float setpOn, SensorValue::Type t, IWeb* const parent)
 : IPlugSwitch(parent), setpoint_off(setpOff), setpoint_on(setpOn), sv_current(t) 
{
}

void PlugSwitchSensor::update()
{
	sv_current.value = SensorHandle::singleton()->getAverage(sv_current.type);
	if(!isEnabled()) return;
		
	int8_t ibb = isBeyondBoundaries();
	
	if(ibb == 0) return;
	
	bool bound = false;
		
	if(ibb == 1) bound = true;
		
	bool inverse = (setpoint_off > setpoint_on);
	
	if(isActive() != (inverse != bound)){
		IWeb::toggleFlag(IPlugSwitch::ACTIVE);
	}	
}

int8_t PlugSwitchSensor::isBeyondBoundaries()
{
	if(!isEnabled()) return -2;
	
	bool inversed = setpoint_off > setpoint_on;
	
	float upper = (inversed) ? setpoint_off : setpoint_on;
	float lower = (inversed) ? setpoint_on : setpoint_off;
	float value = sv_current.value;
	
	if(value <= lower) return -1;
	if(value >= upper) return 1;
	
	return 0;
}

void PlugSwitchSensor::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 0){
		bool enable = *(bool*)payload;
		if(setpoint_off > 0 && setpoint_on > 0)
			IWeb::setFlag((IPlugSwitch::ENABLED|IWeb::INTERVAL), enable);
	} else if(varId == 1) {
		setpoint_off = *(float*)payload;
	} else if(varId == 2) {
		setpoint_on = *(float*)payload;
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
}

//ISerializable
size_t PlugSwitchSensor::size() { 
	return 	SerializablePOD<uint16_t>::size(m_flags) + 
			SerializablePOD<float>::size(setpoint_on) 	+ 
			SerializablePOD<float>::size(setpoint_off); 
}
uint8_t* PlugSwitchSensor::serialize(uint8_t *eptr) {
	eptr = SerializablePOD<uint16_t>::serialize(eptr, m_flags);
	eptr = SerializablePOD<float>::serialize(eptr, setpoint_on);
	eptr = SerializablePOD<float>::serialize(eptr, setpoint_off);
	return eptr; 
}
const uint8_t* PlugSwitchSensor::deserialize(const uint8_t *eptr) {
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE); 
	eptr = SerializablePOD<uint16_t>::deserialize(eptr, m_flags);
	eptr = SerializablePOD<float>::deserialize(eptr, setpoint_on);
	eptr = SerializablePOD<float>::deserialize(eptr, setpoint_off);
	return eptr; 
	
}
