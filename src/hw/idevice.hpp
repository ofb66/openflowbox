#ifndef IDEVICE_HPP
#define IDEVICE_HPP

#include "Arduino.h"
#include "../web/iweb.hpp"

class IDevice : public IWeb {
public:
    IDevice(IWeb *const parent = nullptr) : IWeb(parent) {};
    virtual ~IDevice(){};
    virtual void update() = 0;
    
    inline bool isOn() const { return IWeb::isFlagSet(IDevice::ON); }
    
    enum {
        ENABLED 	=  IWeb::LAST,
        ACTIVE  	= (IWeb::LAST << 1),
        ON      	= (IWeb::LAST << 2),
        LOCKED  	= (IWeb::LAST << 3),
        DEVC_LAST   = (IWeb::LAST << 4)
    };
};

#endif
