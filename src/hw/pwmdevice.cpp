#include "pwmdevice.hpp"

#include "sensorhandle.hpp"
#include "pwmsetting.hpp"
#include "../persistence/serializablepod.hpp"

using namespace Persistence;


#ifdef ARDUINO_ARCH_ESP32
uint8_t PWMDevice::s_channel = 0;
#endif

const char PWMDevice::s_header[] PROGMEM = R"=====(				<article>
				<h3 style='padding-top:10px;'> 
					<i class='fas fa-fan'></i> <span id='%u'>%s</span> <span id='%u' class='fanctrlin'>%u</span>%%
				</h3>
				<h4 style='padding-bottom:10px;'><i class='fas fa-tachometer-alt'></i> <span id='%u'>%u</span>rpm</h4>
)=====";

const char PWMDevice::s_footer[] PROGMEM = R"=====(
				</article>
)=====";

const char PWMDevice::s_json_dyn[] PROGMEM = R"=====({
"%u":%u,
"%u":%u
})=====";

const char PWMDevice::s_json_ui[] PROGMEM = R"=====({
"%u":"%s",
"%u":"%s"
})=====";

size_t PWMDevice::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 30;
		case StrObject::FOOTER:
		return strlen_P(s_footer);
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 20;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 30;
		default:
		return 0;
	}
}

size_t PWMDevice::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header, 
							createId(0), m_name.c_str(),
							createId(1), m_currV,
							createId(2), m_rpm
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        break;
        case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn,
							createId(1), m_currV,
							createId(2), m_rpm
						);
        break;
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0),m_name.c_str(),
							createId(50), m_name.c_str()
						);
        default:
        return 0;
    }
}


PWMDevice::PWMDevice(uint8_t pin, const char* name, uint8_t tachopin, IWeb* const parent)
    : IDevice(parent), ISerializable(ISerializable::BASIC_CFG), m_pin(pin), 
		m_name(name), m_currV(50), tacho(tachopin), m_rpm(0)
{
#ifdef ARDUINO_ARCH_ESP32
    m_channel = s_channel++;
    ledcSetup(m_channel, PWM_FREQUENCY, PWM_RESOLUTION);
    ledcAttachPin(m_pin, m_channel);
#else
	pinMode(m_pin, OUTPUT);
#endif
	initTacho(tacho);
    //NIGHT
    new PWMSetting(PWM_DISPLAY_2_VAL(50), false, this);
    //DAY
    new PWMSetting(PWM_DISPLAY_2_VAL(50), PWMSetting::IS_DAY, this);
}

void PWMDevice::update(){
	
	int8_t vcurrent[m_children.size()];
	uint8_t i = 0;
	std::vector<IWeb*>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++){
		auto setting = static_cast<PWMSetting*>(*it);
		setting->update();
		vcurrent[i++] = setting->getVCurr();
	}
    /*
    auto currV = setting->getVCurr();

    setting = static_cast<PWMSetting*>(m_children[1]);
    setting->update();
	
	
    if(SensorHandle::singleton()->isLightOn()){
        currV = setting->getVCurr();
    }
    */
    
    auto currV = vcurrent[SensorHandle::singleton()->isLightOn()];
	
    if(currV != m_currV){
		IWeb::enableFlag(IWeb::HAS_CHANGE);
		m_currV = currV;
	}
	
#ifdef ARDUINO_ARCH_ESP32
    ledcWrite(m_channel, m_currV);
#else
    analogWrite(m_pin, m_currV);
#endif


	updateTacho(tacho);
    auto rpm = tacho.rpm();
    
    if(m_rpm != rpm){
		IWeb::enableFlag(IWeb::HAS_CHANGE);
		m_rpm = rpm;
	}
}

void PWMDevice::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 50){
		m_name = (char*)payload;
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
		ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
	}
}
//ISerializable
size_t PWMDevice::size() { 
	return m_name.size(); 
}
uint8_t* PWMDevice::serialize(uint8_t *eptr) { 
	return SerializablePOD<char>::serializeString(eptr, m_name);
}
const uint8_t* PWMDevice::deserialize(const uint8_t *eptr) { 
	return SerializablePOD<char>::deserializeString(eptr, m_name);
}
