/*generated file pidcontrolsensor.hpp*/
#ifndef PIDCONTROLSENSOR_HPP
#define PIDCONTROLSENSOR_HPP

#include "../web/iweb.hpp"

class PidControlSensor : public IWeb
{
private:
	static const char s_header[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	const uint8_t m_address;
    //inherited functions
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
protected:
public:
	enum {
		ENABLED = IWeb::LAST
	};
    //ctor
    PidControlSensor(uint8_t addr, IWeb* const parent = nullptr);
    //dtor
    virtual ~PidControlSensor();
        
	virtual void handleData(uint8_t varId, void* const &payload) override;
	
	inline uint8_t address() const  { return m_address; }
};
#endif //PIDCONTROLSENSOR_HPP
