#ifndef PWMSETTING_HPP
#define PWMSETTING_HPP

#include <Arduino.h>

#include "idevice.hpp"
#include "../web/iweb.hpp"

#include "pidcontrol.hpp"
#include "../utils/timer.hpp"
#include "../persistence/serializable.hpp"
using namespace Persistence;

#define INDX_TEMP	0
#define INDX_HUMI	1

class PWMSetting : public IDevice, public ISerializable  {
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	const bool isDayDevice;
	
	uint8_t m_vmax;
	uint8_t m_vmin;
	uint8_t m_vcurr;
	uint8_t m_mix;
	
	Utils::Timer16 load_timer;

	//IWeb
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
public:
    PWMSetting(uint8_t vmax, bool isday, IWeb* const parent = nullptr);

	inline uint8_t getVCurr() const { return PWM_DISPLAY(m_vcurr); }

	enum {
		IS_DAY = IDevice::DEVC_LAST,
		LIGHT_ON = (IDevice::DEVC_LAST << 1),
		ON_LOAD  = (IDevice::DEVC_LAST << 2)
	};
    
    virtual void handleData(uint8_t varId, void* const &payload) override;
    
    //IDevice
    virtual void update();
    
    //ISerializable
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
};

#endif
