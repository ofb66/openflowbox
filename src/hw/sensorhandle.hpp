#ifndef SENSORHANDLE_HPP
#define SENSORHANDLE_HPP

#include <Arduino.h>
#include "sensor.hpp"
#include "../utils/timer.hpp"

#define MAX_SENSORS     		8
#define SENSOR_UPDATE_INTERVAL	5000
#define MUX_ADDRESS     		0x70

#define TCA_SELECT_FAILED		-2
#define TCA_BUS_NOT_CONNECTED	-1

class SensorHandle : public IWeb {
private:
    static SensorHandle* shandle;
    
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;

	Utils::Timer16 interval_timer;

    uint8_t m_availableSensors;
    GenericSensor ov_sensor;

    SensorHandle(IWeb* const parent = nullptr);

    int8_t tcaselect(uint8_t i2c_bus);
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
public:
	enum {
		LIGHT_ON = IWeb::LAST,
		ON_LOAD  = (IWeb::LAST << 1)
	};
    static SensorHandle *  singleton();
    
    int8_t identifySensorByI2CAddress(uint8_t tca_bus);
    ISensor* createSensor(int8_t i2cAddr, uint8_t address, IWeb* const parent = nullptr);

    virtual void handleData(uint8_t varId, void* const &payload) override;

	uint8_t getRegisteredSensors();
    uint8_t getAvailableSensors(SensorValue::Type t);
    float getAverage(SensorValue::Type sensor_t, uint8_t sensors = 0xFF);

    bool validateSensors();
    void update();
    inline bool isLightOn() const { return IWeb::isFlagSet(LIGHT_ON); }
};

#endif
