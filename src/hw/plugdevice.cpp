#include "plugdevice.hpp"

#ifdef ARDUINO_ARCH_ESP32
#include <AsyncTCP.h>
#else
#include <ESPAsyncTCP.h>
#endif

#include "plugswitch.hpp"
#include "../persistence/serializablepod.hpp"

using namespace Persistence;

const char IPlugDevice::s_header[] PROGMEM = R"=====(				<article>
				<button class='collapsible'>
				<h3>
				<span class='fa-layers fa-fw'>
					<i class='fas fa-plug fa-lg sym-el' id='%u'></i>
					<input type='checkbox' class='allb nccrb onflash' id='%u'/><label for='%u'><span class='rbui'></span></label>
				</span>
				<span id='%u'> %s</span>
				</h3>
				</button>
				<div class='content' id='%u'>
					<span class='relative'>
						<input type='checkbox' onclick='sendBool(this)' class='allb ccb switbtn' id='%u'>
						<label for='%u'><span class='ui'></span></label>
					</span>
					<button type='button' style='margin-top: 1.8em;' class='collapsible'><i class='fas fa-cog'></i></button>
					<div class='content'>
)=====";

const char IPlugDevice::s_footer[] PROGMEM = R"=====(				</div></div>
				</article>
)=====";

const char IPlugDevice::s_json_ui[] PROGMEM = R"=====({
"%u":%u,
"%u":"%s",
"%u":%u,
"%u":%u,
"%u":"%s",
"%u":%u
})=====";

//IWeb
size_t IPlugDevice::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 40;
		case StrObject::FOOTER:
		loadTimer.start();
		return strlen_P(s_footer);
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 40;
		default:
		return 0;
	}
}

size_t IPlugDevice::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{ 
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							createId(3),
							createId(0), createId(0),
							createId(1), m_name.c_str(),
							webId(),
							createId(2), createId(2)
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        break;
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(0), isOn(),
							createId(1), m_name.c_str(),
							createId(2), isOn(),
							createId(3), symbol_indx,
							createId(50), m_name.c_str(),
							createId(51), symbol_indx
						);
        break;
        default:
        return 0;
    }
} 

IPlugDevice::IPlugDevice(const char* name, IWeb* const parent)
	: IDevice(parent), ISerializable(ISerializable::Type::BASIC_CFG), m_name(name)
{	
	new PlugSwitchTimer(this);
	new PlugSwitchInterval(this);
	
	new PlugSwitchSensor(20., 25., SensorValue::Type::DEGREE, this);
	new PlugSwitchSensor(50., 60., SensorValue::Type::HUMIDITY, this);
	new PlugSwitchSensor(80., 45., SensorValue::Type::SOIL_MOISTURE, this);
}

void IPlugDevice::update()
{
	std::vector<IWeb*>::iterator it;
	if(loadTimer.isRunning() && loadTimer.hasExpired(2000)){
		loadTimer.stop();
		
		for(it = m_children.begin(); it != m_children.end(); it++){
			(*it)->IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
		}
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	}
	 
	for(it = m_children.begin(); it != m_children.end(); it++){
		auto child = static_cast<IPlugSwitch*>(*it);
		child->update();
	}
	if(hasToSwitch()){
		toggle();
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	}
}

bool IPlugDevice::hasToSwitch()
{
	bool hasToSwitch = false;
	//Timer
	auto timerSwitch = static_cast<IPlugSwitch*>(m_children[0]);
	if(timerSwitch->isEnabled()){
		hasToSwitch = timerSwitch->isActive() != isOn();
		if(!timerSwitch->isActive()) return hasToSwitch;
	}
	//Interval
	auto intervalSwitch = static_cast<IPlugSwitch*>(m_children[1]);
	if(intervalSwitch->isEnabled()){
		hasToSwitch = intervalSwitch->isActive() != isOn();
		if(!intervalSwitch->isActive()) return hasToSwitch;
	}
	
	auto tempSwitch = static_cast<IPlugSwitch*>(m_children[2]);
	auto humiSwitch = static_cast<IPlugSwitch*>(m_children[3]);
	auto moisSwitch = static_cast<IPlugSwitch*>(m_children[4]);
	if(tempSwitch->isEnabled() || humiSwitch->isEnabled() || moisSwitch->isEnabled()){
		bool isAnyActive = tempSwitch->isActive() || humiSwitch->isActive() || moisSwitch->isActive();
		hasToSwitch = (isOn() != isAnyActive);
	}

	return hasToSwitch;
}

void IPlugDevice::toggle()
{
	if(isOn()) switchOff();
	else switchOn();
}

void IPlugDevice::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 2){
		for(uint8_t u = 0; u < m_children.size(); u++){
			auto child = static_cast<IPlugSwitch*>(m_children[u]);
			if(child->isBeyondBoundaries() == 0){
				child->setFlag(IPlugSwitch::ACTIVE, *(bool*)payload);
			}
		}
		
		if(*(bool*)payload)
			switchOn();
		else switchOff();
	} else if(varId == 50){
		m_name = (char*)payload;
	} else if(varId == 51){
		symbol_indx = *(uint8_t*)payload;
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
}

//ISerializable
size_t IPlugDevice::size() { 
	return SerializablePOD<uint8_t>::size(symbol_indx) + m_name.size(); 
}

uint8_t* IPlugDevice::serialize(uint8_t *eptr) {
	eptr = SerializablePOD<uint8_t>::serialize(eptr, symbol_indx);
	eptr = SerializablePOD<uint8_t>::serializeString(eptr, m_name);
	return eptr; 
}
const uint8_t* IPlugDevice::deserialize(const uint8_t *eptr) { 
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, symbol_indx);
    eptr = SerializablePOD<uint8_t>::deserializeString(eptr, m_name);
	return eptr;
}

PlugDevice12V::PlugDevice12V(uint8_t pin, const char* name, IWeb* const parent) 
: IPlugDevice(name, parent), m_pin(pin) 
{
	pinMode(m_pin, OUTPUT);
}

void PlugDevice12V::switchOn()
{
	IWeb::enableFlag(IDevice::ON);
	digitalWrite(m_pin, HIGH);
}
void PlugDevice12V::switchOff()
{
	IWeb::disableFlag(IDevice::ON);
	digitalWrite(m_pin, LOW);
}

const char PlugDeviceRemote::s_cmd_power_on[] PROGMEM = "GET /cm?cmnd=power%20on HTTP/1.0\r\n";
const char PlugDeviceRemote::s_cmd_power_off[] PROGMEM = "GET /cm?cmnd=power%20off HTTP/1.0\r\n";
const char PlugDeviceRemote::s_cmd_power_status[] PROGMEM = "GET /cm?cmnd=power%20status HTTP/1.0\r\n";

const char PlugDeviceRemote::s_respond_status_on[] PROGMEM = R"=====({"POWER":"ON"})=====";
const char PlugDeviceRemote::s_respond_status_off[] PROGMEM = R"=====({"POWER":"OFF"})=====";

void PlugDeviceRemote::runAsyncClient(const char* cmd)
{
	if(!hasValidIP()) return;
	if(client) return;
	
	client = new AsyncClient();
	
	if(!client) {
		log_w("Failed to allocate aclient");
		return;
	}
	
	client->onConnect([this, cmd](void * arg, AsyncClient * c){
		log_d("%s conn", m_ip.toString());
		client->onError(NULL, NULL);
		
		c->onDisconnect([this](void * arg, AsyncClient * ac){
			log_d("%s disconn", m_ip.toString());
			client = NULL;
			delete ac;
		}, NULL);
	
		c->onData([this](void * arg, AsyncClient * ac, void * data, size_t len){
			log_v("data: %u", len);
			uint8_t * d = (uint8_t*)data;
			d[len] = 0;
			checkPlugStatus((char*)d, len);
		}, NULL);
		
		const size_t len = strlen_P(cmd); 
		char cmdStr[len+1];
		
		strncpy_P(cmdStr, cmd, len);
		cmdStr[len] = 0; 
		log_v("cmd: %s", cmdStr);
		c->write(cmdStr);
	}, NULL);
	
	client->onError([this](void * arg, AsyncClient * c, int error){
		log_w("RPlug Connect Error");
		client = NULL;
		delete c;
	}, NULL);
	
	if(!client->connect(m_ip, 80)){
		log_w("Failed to connect");
		return;
	}
}

bool PlugDeviceRemote::hasValidIP() const 
{
	log_v("R_IP %s", m_ip.toString());
#ifdef ARDUINO_ARCH_ESP32
    return m_ip != INADDR_NONE;
#else
    return m_ip.isSet();
#endif
}

void PlugDeviceRemote::handleData(uint8_t varId, void * const &payload)
{
	if(varId == 52){
		m_ip = *(uint32_t*)payload;
		//changeIP(n_ip);
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	} else {
		IPlugDevice::handleData(varId, payload);
	}
}

void PlugDeviceRemote::checkPlugStatus(char* data, size_t len)
{
	if(strcmp_P(data, s_respond_status_on) == 0){
		log_d("%s on", m_ip.toString());
		IWeb::enableFlag(IDevice::ON | IWeb::HAS_UI_CHANGE);
	} else {
		if(strcmp_P(data, s_respond_status_off) == 0){
			log_d("%s off", m_ip.toString());
			IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
			IWeb::disableFlag(IDevice::ON);
		}
	}
	
}

void PlugDeviceRemote::switchOn()
{
	IWeb::enableFlag(IDevice::ON);
	runAsyncClient(s_cmd_power_on);
	/* If hasValidIP() switch remote plug on via webapi */
}
void PlugDeviceRemote::switchOff()
{
	IWeb::disableFlag(IDevice::ON);
	runAsyncClient(s_cmd_power_off);
	/* If hasValidIP() switch remote plug off via webapi */
}

//ISerializable
size_t PlugDeviceRemote::size() { 
	return IPlugDevice::size() + SerializablePOD<uint32_t>::size(m_ip); 
}

uint8_t* PlugDeviceRemote::serialize(uint8_t *eptr) { 
	eptr = IPlugDevice::serialize(eptr);
	eptr = SerializablePOD<uint32_t>::serialize(eptr, m_ip);
	return eptr; 
}
const uint8_t* PlugDeviceRemote::deserialize(const uint8_t *eptr) {
	eptr = IPlugDevice::deserialize(eptr);
	uint32_t ip = 0;
	eptr = SerializablePOD<uint32_t>::deserialize(eptr, ip);
	m_ip = ip;
	return eptr; 	
}
