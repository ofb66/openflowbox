#include "sensorlog.hpp"


const char SensorLog::s_header[] PROGMEM = R"=====(					<tr style='font-size:0.8em;'>
					<td><span id='%u'>%5.2f</span>%s</td>
					<td><span id='%u'>%5.2f</span>%s</td>
					<td><span id='%u'>%5.2f</span>%s</td>
					</tr>
)=====";

const char SensorLog::s_json_dyn[] PROGMEM = R"=====({
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f
})=====";

SensorLog::SensorLog(SensorValue const * const input, IWeb* const parent)
:
IWeb(parent),
m_stats { 0., 0., 0. },
m_input(input),
m_nSamples(0)
{
}

void SensorLog::reset()
{
    m_stats[LOG_MIN] = m_input->value;
    m_stats[LOG_AVG] = m_input->value;
    m_stats[LOG_MAX] = m_input->value;

    IWeb::enableFlag(HAS_BEGUN | IWeb::HAS_CHANGE);
}

void SensorLog::update(){
    if(m_input->value < m_stats[LOG_MIN]) {
        m_stats[LOG_MIN] = m_input->value;
        IWeb::enableFlag(IWeb::HAS_CHANGE);
    } else if(m_input->value > m_stats[LOG_MAX]) {
        m_stats[LOG_MAX] = m_input->value;
        IWeb::enableFlag(IWeb::HAS_CHANGE);
    }
    calcAvrg();
}

void SensorLog::calcAvrg()
{
    float a_tmp = m_stats[LOG_AVG];
	if(m_nSamples < 1000){
		++m_nSamples;
	}
	double alpha = 1./m_nSamples;
	a_tmp = (alpha * m_input->value) + (1.0 - alpha) * a_tmp;

    if(a_tmp != m_stats[LOG_AVG]){
        m_stats[LOG_AVG] = a_tmp;
        IWeb::enableFlag(IWeb::HAS_CHANGE);
    }
}

size_t SensorLog::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 20;
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 10;
		default:
		return 0;
	}
}

size_t SensorLog::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{	
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							createId(0), m_stats[LOG_MIN], SensorValue::unit(m_input->type),
							createId(1), m_stats[LOG_AVG], SensorValue::unit(m_input->type),
							createId(2), m_stats[LOG_MAX], SensorValue::unit(m_input->type)
						);
        break;
        case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn,
							createId(0), m_stats[LOG_MIN],
							createId(1), m_stats[LOG_AVG],
							createId(2), m_stats[LOG_MAX]
						);
        break;
        default:
        return 0;
    }
}

