#include "devicetacho.hpp"
static volatile int interrupt_count = 0;
void IRAM_ATTR rpm_fan()
{
	++interrupt_count;
} 

void initTacho(DeviceTacho &tacho)
{
	pinMode(tacho.pin(), INPUT);
	digitalWrite(tacho.pin(), HIGH);
	
	attachInterrupt(digitalPinToInterrupt(tacho.pin()), rpm_fan, FALLING);
	
	tacho.updateTimer.start();
}

void updateTacho(DeviceTacho& tacho)
{
	if(tacho.updateTimer.hasExpired(1000)){
		tacho.updateTimer.start();
		detachInterrupt(digitalPinToInterrupt(tacho.pin()));
		
		tacho.m_rpm = interrupt_count * INTERVAL_MAGIC_NUMBER;
		
		interrupt_count = 0;
		
		attachInterrupt(digitalPinToInterrupt(tacho.pin()), rpm_fan, FALLING);
	}
}
