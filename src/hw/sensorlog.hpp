#ifndef SENSORLOG_HPP
#define SENSORLOG_HPP

#include <Arduino.h>
#include "sensorvalue.hpp"
#include "../web/iweb.hpp"

#define LOG_MIN 0
#define LOG_AVG 1
#define LOG_MAX 2

class SensorLog : public IWeb {

public:
	enum {
		HAS_BEGUN = IWeb::LAST
	};
	
    SensorLog(SensorValue const * const input, IWeb* const parent = nullptr);
    virtual ~SensorLog(){ log_d("DTOR called %u", webId()); }
    void reset();
    bool hasBegun() const { return IWeb::isFlagSet(HAS_BEGUN); }
    void update();

    const float* stats() const { return m_stats; }
private:
	static const char s_header[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	
    float m_stats[3];
    SensorValue const * const m_input;

    bool m_hasBegun;

    uint16_t m_nSamples;

    void calcAvrg();

    //IWeb
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
};


#endif
