#include "plughandle.hpp"

#include "pins.h"

const char IPlugHandle::s_header[] PROGMEM = R"=====(			<button type='button' class='collapsible'>
				<span class='fa-layers fa-fw'>
					<i class='fas fa-plug fa-2x'></i>
					%s
				</span>
				</button>
				<div style='margin: auto;' class='content' id='%u'>
)=====";

const char IPlugHandle::s_footer[] PROGMEM = R"=====(				</div>
)=====";

IPlugHandle::IPlugHandle(IWeb* const parent)
: IWeb(parent)
{
}

size_t IPlugHandle::len(IWeb::StrObject so)
{
	if(m_children.size() == 0) return 0;
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + strlen_P(symbol()) + 20;
		case StrObject::FOOTER:
		return strlen_P(s_footer);
		default:
		return 0;
	}
}

size_t IPlugHandle::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	if(m_children.size() == 0) return 0;
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							symbol(),
							webId()
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        break;
        default:
        return 0;
    }
}

bool IPlugHandle::isAnyActive(){
    std::vector<IWeb*>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++){
		auto child = static_cast<IPlugDevice*>(*it);
		if(child->isOn()) {
			return true;
		}
	}
	return false;
}

void IPlugHandle::update()
{	
	std::vector<IWeb*>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++){
		auto child = static_cast<IPlugDevice*>(*it);
		child->update();
	}
}

PlugHandle12V* PlugHandle12V::phandle = nullptr;
const char PlugHandle12V::header_symbol[] PROGMEM = " ";

PlugHandle12V* PlugHandle12V::singleton()
{
    if(phandle == nullptr){
        phandle = new PlugHandle12V();

        auto dvc = new PlugDevice12V(PIN_REMOTE12V_1, "Light", phandle);
        dvc->setSymbol(2);
        dvc = new PlugDevice12V(PIN_REMOTE12V_2, "Heat", phandle);
        dvc->setSymbol(3);
        
    }
    return phandle;
}

PlugHandleRemote* PlugHandleRemote::phandle = nullptr;
const char PlugHandleRemote::header_symbol[] PROGMEM = "<i class='fas fa-wifi' data-fa-transform='shrink-5 down-10 right-15'></i>";

PlugHandleRemote* PlugHandleRemote::singleton()
{
    if(phandle == nullptr){
        phandle = new PlugHandleRemote();
		
		uint8_t ip[4] = { 192, 168, 0, 53 };
		
        auto dvc = new PlugDeviceRemote("Light", *(uint32_t*)ip, phandle);
        dvc->setSymbol(3);
        
        ip[3] = 92;
        dvc = new PlugDeviceRemote("Heat", *(uint32_t*)ip, phandle);
        dvc->setSymbol(2);
        
        ip[3] = 93;
        dvc = new PlugDeviceRemote("Pump", *(uint32_t*)ip, phandle);
        dvc->setSymbol(4);
        
    }
    return phandle;
}

