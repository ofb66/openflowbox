/*generated file pidsensor.cpp*/
#include "pidcontrolsensor.hpp"
#include "pidcontrol.hpp"

const char PidControlSensor::s_header[] PROGMEM = R"=====(					<span id='%u' class='fa-layers fa-fw'>
						<input type='checkbox' id='%u' onclick='sendBool(this)' class='allb nccrb sensorcb' %s/><label for='%u'><span class='rbui'></span></label>
						<span class='fa-layers-text nowhite'>%u</span>
					</span>
					<span>&ensp;</span>
)=====";

const char PidControlSensor::s_json_ui[] PROGMEM = R"=====({
"%u":%u
})=====";

//inherited functions
size_t PidControlSensor::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 20;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 5;
		default:
		return 0;
	}
}

size_t PidControlSensor::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	switch(so){
		case StrObject::HEADER:
		return snprintf_P((char*)buffer, len, s_header, webId(), 
							createId(0), (IWeb::isFlagSet(PidControlSensor::ENABLED)) ? PSTR("checked") : PSTR(" "),
							createId(0), m_address
						);
		case StrObject::JSON_UI:
		return snprintf_P((char*)buffer, len, s_json_ui, createId(0), IWeb::isFlagSet(PidControlSensor::ENABLED));
		default:
		return 0;
	}
}

//ctor
PidControlSensor::PidControlSensor(uint8_t addr, IWeb* const parent)
	: IWeb(parent), m_address(addr)
{
	IWeb::enableFlag(PidControlSensor::ENABLED);
}

//dtor
PidControlSensor::~PidControlSensor()
{
}

//inherited functions
void PidControlSensor::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 0){
		IWeb::setFlag(PidControlSensor::ENABLED, *(bool*)payload);
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
		
		static_cast<PidControl* const>(parent())->updateSensorStatus(m_address, *(bool*)payload);
	}
}

//EOF
