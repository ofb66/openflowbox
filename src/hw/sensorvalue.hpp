/*generated file sensorvalue.hpp*/
#ifndef SENSORVALUE_HPP
#define SENSORVALUE_HPP

#include <Arduino.h>

struct SensorValue {
	enum Type {
		DEGREE,
		HUMIDITY,
		SOIL_MOISTURE,
		PRESSURE,
		LIGHT
	} type;
	
	float value;
	
	SensorValue(Type t = Type::DEGREE, float v = 0.) : type(t), value(v) {}
	~SensorValue() {}
	
	static const char* unit(SensorValue::Type type);
	static const char* symbol(SensorValue::Type type);
};
#endif //SENSORVALUE_HPP
