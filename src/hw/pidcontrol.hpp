#ifndef PIDCONTROL_HPP
#define PIDCONTROL_HPP

#include <Arduino.h>
#include "idevice.hpp"
#include "sensorvalue.hpp"
#include "../utils/timer.hpp"
#include "../web/iweb.hpp"
#include "../persistence/serializable.hpp"

using namespace Persistence;

#define ESP_PWM_MAX	100
#define PWM_RANGE_MIN   0

#ifdef ARDUINO_ARCH_ESP32
#define PWM_RESOLUTION  7
#define PWM_RANGE_MAX   (1 << PWM_RESOLUTION)
#define PWM_DISPLAY(x) (((uint16_t)x * ESP_PWM_MAX) >> PWM_RESOLUTION)
#define PWM_DISPLAY_2_VAL(x) (((uint16_t)x << PWM_RESOLUTION) / ESP_PWM_MAX)
#else
#define PWM_RANGE_MAX   ESP_PWM_MAX
#define PWM_DISPLAY(x) (x)
#define PWM_DISPLAY_2_VAL(x) (x)
#endif


#define SAMPLE_TIME 1000
#define INDX_KP	0
#define INDX_KI	1
#define INDX_KD	2

class PidControl : public IDevice, public ISerializable
{
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	Utils::Timer16 sample_timer;
	float m_lastInput, m_outputSum;

	float m_target;
	float m_params[3];
	int16_t m_output;
	
    const SensorValue::Type m_sensortype;
    uint8_t m_sensors;
    float m_weight;
    uint8_t const * const m_vmin;
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);

	const char* determineColor();
public:
    PidControl(
        float target,
        float kp,
        float ki,
        float kd,
        const SensorValue::Type sensortype,
        uint8_t const * const vmin,
        IWeb* const parent = nullptr
    );
	
	virtual void handleData(uint8_t varId, void* const &payload) override;
    
    //IDevice
    virtual void update();
    
    //ISerializable
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);

	inline int8_t getOutput() const { return m_output; }
	inline float getWeightedOutput() const { return (isFlagSet(IDevice::ENABLED)) ? m_output * m_weight : 0.; }
	inline float getWeight() const { return (isFlagSet(IDevice::ENABLED)) ? m_weight : 0.; }
	
	void updateSensorStatus(uint8_t saddress, bool enable);
};

#endif
