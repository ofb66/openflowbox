#ifndef PINS_H
#define PINS_H

#include <Arduino.h>

#ifdef ARDUINO_ARCH_ESP32
#define PIN_FAN1    	26
#define PIN_FAN2    	27
#define PIN_FAN3    	14
#define PIN_FAN4    	12
#define PIN_TACHO   	17
#define PIN_REMOTE12V_1	15
#define PIN_REMOTE12V_2	16
#define PLED_SENSOR     2
#else
#define PIN_FAN1    	0
#define PIN_FAN2    	14
#define PIN_FAN3    	12
#define PIN_FAN4    	13
#define PIN_TACHO   	17
#define PIN_REMOTE12V_1	15
#define PIN_REMOTE12V_2	16
#define PLED_SENSOR     16
#endif

#endif
