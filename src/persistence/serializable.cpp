/*generated file serializable.cpp*/
#include "serializable.hpp"
#include "serializablepod.hpp"

#include "../version.h"

namespace Persistence {

uint8_t ISerializable::s_statusFlags[2] = {0};

Utils::LinkedList<ISerializable> ISerializable::list_ov;
Utils::LinkedList<ISerializable> ISerializable::list_dvc;
uint16_t ISerializable::s_extFileVersion = 0;

//ctor
ISerializable::ISerializable(Type t)
	: m_type(t)
{
	if(m_type == Type::BASIC_CFG)
		list_ov.add(this);
	else
		list_dvc.add(this);
}

//dtor
ISerializable::~ISerializable()
{
}

void ISerializable::enableFlag(uint8_t flag)  { s_statusFlags[m_type] |= flag; }
void ISerializable::disableFlag(uint8_t flag) { s_statusFlags[m_type] &= ~flag; }

size_t ISerializable::s_totalSize(Type t){
	size_t s = 0;
	
	Utils::Node<ISerializable>* last = (t == Type::BASIC_CFG)? list_ov.head() : list_dvc.head();
	while(last){
		s += last->data()->size();
		last = last->next();
	}
	return s+2; // 2Bytes (uint16_t) reserved for file version tag
}

void ISerializable::s_serialize(uint8_t *eptr, Type t){
	Utils::Node<ISerializable>* last = (t == Type::BASIC_CFG)? list_ov.head() : list_dvc.head();
	eptr = SerializablePOD<uint16_t>::serialize(eptr, FILE_VERSION);
	s_extFileVersion = FILE_VERSION;
	while(last){
		eptr = last->data()->serialize(eptr);
		last = last->next();
    }
}

void ISerializable::s_deserialize(const uint8_t* eptr, Type t){
	Utils::Node<ISerializable>* last = (t == Type::BASIC_CFG)? list_ov.head() : list_dvc.head();
	
	eptr = SerializablePOD<uint16_t>::deserialize(eptr, s_extFileVersion);
	log_d("EFV %u", s_extFileVersion);
	while(last){
		eptr = last->data()->deserialize(eptr);
		last = last->next();
    }
}

bool ISerializable::s_flagEnabled(uint8_t flag, bool orConstraint, Type t) { return (orConstraint) ? s_statusFlags[t] & flag : (s_statusFlags[t] & flag) == flag; }

};

//EOF
