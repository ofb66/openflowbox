/*generated file persistence.cpp*/
#include "persistence.hpp"

#include <LittleFS.h>
#include <FS.h>
//#include <EEPROM.h>

#include <string>

#include "serializablepod.hpp"

namespace Persistence {

void printU8Array(uint8_t* ptr, size_t len)
{
	log_v("Content: %s", (const char*)ptr);
	/*
	for(int i = 0; i < len; i++){
		Serial.print((char)ptr[i]);
	}
	Serial.println();
	*/ 
	
}

uint16_t crc16(uint8_t const *data, size_t size) {     
	uint16_t crc = 0;     
	while (size--) {  
		crc ^= *data++;
		for (unsigned k = 0; k < 8; k++){
			crc = crc & 1 ? (crc >> 1) ^ 0xA001 : crc >> 1;
		}
	}
	return crc; 
}
    
uint8_t IPersistence::read()
{
	log_i("Begin reading %s CFG file %s", (p_type == ISerializable::Type::MODULE_CFG)?"MODULE":"BASIC", configFile().c_str());
	auto config_file = configFile();
	if(config_file.empty()){
		log_w("No file selected");
		return ErrorStatus::NO_FILE_SEL;
	}
	
	if(!LittleFS.exists(config_file.c_str())){
		log_w("File not exist %s", config_file.c_str());
		return ErrorStatus::ON_NOT_EXIST;
	}
	
	File current_config = LittleFS.open(config_file.c_str(), FILE_READ);
	
	if(!current_config) {
		log_w("Could not open %s", current_config.name());
		return ErrorStatus::ON_OPEN;
	}
	
	const size_t len = current_config.size();
	if(len == 0){
		log_w("%s empty", current_config.name());
		return ErrorStatus::EMPTY;
	}
	
	uint8_t content[len];
	
	current_config.read(content, len);
	
	current_config.close();
	
	m_crc = crc16(content, len);
	
	ISerializable::s_deserialize(content, p_type);
	
	log_i("Finished reading CFG file");
	return ErrorStatus::OK;	
}

uint8_t IPersistence::write()
{
	log_i("Begin writing %s CFG file %s", (p_type == ISerializable::Type::MODULE_CFG)?"MODULE":"BASIC", configFile().c_str());
	auto config_file = configFile();
	if(config_file.empty()){
		log_i("No file selected");
		return ErrorStatus::NO_FILE_SEL;
	}
		
	size_t len = ISerializable::s_totalSize(p_type);
	uint8_t content[len];
	
	ISerializable::s_serialize(content, p_type);
	
	auto _crc = crc16(content, len);
	if(m_crc == _crc){
		log_d("No changes, nothing to do...");
		return ErrorStatus::NO_CHANGES;
	}
	m_crc = _crc;
	
	if(LittleFS.exists(config_file.c_str())){
		log_v("Remove file...");
		LittleFS.remove(config_file.c_str());
	}
	
	File current_config = LittleFS.open(config_file.c_str(), FILE_WRITE);
	
	if(!current_config) {
		log_w("Could not open");
		return ErrorStatus::ON_OPEN;
	}

	//printU8Array(content, len);
	if(current_config.write(content, len) != len)
	{
		log_w("FW %s failed", config_file.c_str());
		return ErrorStatus::ON_WRITE;
	}
	current_config.close();
	log_i("Finished writing CFG file");
	return ErrorStatus::OK;
}

IPersistence::IPersistence(ISerializable::Type t) : 
	p_type(t),
	m_crc(0)
{
}

//dtor
IPersistence::~IPersistence()
{
}

void IPersistence::begin()
{
    if(read() != ErrorStatus::OK){
		write();
    }
}

void IPersistence::update(unsigned long expireInterval)
{
	if(ISerializable::s_flagEnabled(ISerializable::SER_NOTIFY_ABOUT_CHANGES, true, p_type)){
		ISerializable::s_disableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES, p_type);
		ISerializable::s_enableFlag(ISerializable::SER_READY_TO_COMMIT, p_type);
		updateTimer.start();
	}
	if(ISerializable::s_flagEnabled(ISerializable::SER_NEW_VALUE_IN_VERSION, true, p_type) || updateTimer.hasExpired(expireInterval))
	{
		write();
		ISerializable::s_disableFlag(ISerializable::SER_NEW_VALUE_IN_VERSION | ISerializable::SER_READY_TO_COMMIT, p_type);
		updateTimer.start();
	}
}

bool IPersistence::setBackToLastRestorePoint()
{
	return read();
}

void IModuleConfigHandle::setConfigFile(const char* new_config)
{
	config_file = new_config;
}

const std::string& IModuleConfigHandle::configFile() const
{
	return config_file;
}

size_t IModuleConfigHandle::size()
{
	return config_file.size();
}
uint8_t* IModuleConfigHandle::serialize(uint8_t *eptr)
{
	return SerializablePOD<char>::serializeString(eptr, config_file);
}
const uint8_t* IModuleConfigHandle::deserialize(const uint8_t *eptr)
{
	return SerializablePOD<char>::deserializeString(eptr, config_file);
}

const std::string& BasicConfigHandle::configFile() const
{
	return config_file;
}

BasicConfigHandle* BasicConfigHandle::singleton()
{
	static BasicConfigHandle* _instance = nullptr;
	if(_instance == nullptr){
		_instance = new BasicConfigHandle();
	}
	return _instance;
}

};

//EOF
