/*generated file fileexchangehandler.hpp*/
#ifndef FILEEXCHANGEHANDLER_HPP
#define FILEEXCHANGEHANDLER_HPP

#include <Arduino.h>
#include <FS.h>
#include "persistence.hpp"

namespace Persistence {

struct Header {
	const size_t len;
	uint16_t crc;
	uint8_t name_len;
	uint16_t data_len;
	Header() : len(sizeof(crc) +  sizeof(name_len) + sizeof(data_len)) {}
};

class FileExchangeHandler 
{
private:
	uint8_t config_pkg;

	//const char* enc_pkg_name;
	const char* dec_pkg_name;
	//File enc_pkg_file;
	File dec_pkg_file;

	ErrorStatus writeToExportTempFile(File f);
	ErrorStatus prepareExportTempFile();
	
	uint16_t separateFileFromPackageImport(uint16_t pos);
	
    //ctor
    FileExchangeHandler();
protected:
public:
	enum ConfigPackage {
		NONE = 0,
		BASIC = 1,
		MODULE = 2,
		FULL = 3
	};
	static FileExchangeHandler* singleton();
    //dtor
    ~FileExchangeHandler();
        
    //void downloadConfiguration();
    //void uploadConfiguration();
    
	ErrorStatus prepareExportPackage(const char* pkgname);	
	ErrorStatus processPackageImport(const char* pkgname);
	
	uint8_t configPackage() const { return config_pkg; }
	void setConfigPackage(uint8_t pkg) { config_pkg = pkg; }
};

};
#endif //FILEEXCHANGEHANDLER_HPP
