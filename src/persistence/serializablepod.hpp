/*generated file serializablepod.hpp*/
#ifndef SERIALIZABLEPOD_HPP
#define SERIALIZABLEPOD_HPP

#include <Arduino.h>

namespace Persistence {
template <typename POD>
class SerializablePOD
{
public:
	static size_t size(POD p);
	static uint8_t* serialize(uint8_t* target, const POD& value);
	static const uint8_t* deserialize(const uint8_t* source, POD& target);

	static size_t size(POD *p, size_t len);
	static uint8_t* serializeArray(uint8_t* target, const POD* array, size_t len);
	static const uint8_t* deserializeArray(const uint8_t* source, POD* target, size_t len);

    static uint8_t* serializeString(uint8_t* target, const std::string& source);
    static const uint8_t* deserializeString(const uint8_t* source, std::string& target);
};
};
#endif //SERIALIZABLEPOD_HPP
