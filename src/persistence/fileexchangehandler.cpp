/*generated file fileexchangehandler.cpp*/
#include "fileexchangehandler.hpp"

#include <LittleFS.h>

#include "serializablepod.hpp"

extern "C" {
#include "libb64/cdecode.h"
#include "libb64/cencode.h"
}


namespace Persistence {

uint16_t FileExchangeHandler::separateFileFromPackageImport(uint16_t pos)
{
	Header head;
	uint8_t headbuf[head.len];
	dec_pkg_file.seek(pos);
	dec_pkg_file.read(headbuf, head.len);
	pos += head.len;
	const uint8_t* headptr = headbuf;
	
	headptr = SerializablePOD<uint16_t>::deserialize(headptr, head.crc);
	headptr = SerializablePOD<uint16_t>::deserialize(headptr, head.data_len);
	headptr = SerializablePOD<uint8_t>::deserialize(headptr, head.name_len);
	
	uint8_t buf[head.name_len+head.data_len];
	const uint8_t* ptr = buf;
	
	dec_pkg_file.seek(pos);
	dec_pkg_file.read(buf, head.name_len+head.data_len);
	
	char name[head.name_len+1];
	uint8_t data[head.data_len];
	ptr = SerializablePOD<char>::deserializeArray(ptr, name, head.name_len);
	ptr = SerializablePOD<uint8_t>::deserializeArray(ptr, data, head.data_len);
	name[head.name_len] = 0;
	
	if(LittleFS.exists(name)){
		LittleFS.remove(name);
	}
	
	File f = LittleFS.open(name, FILE_WRITE);
	f.write(data, head.data_len);
	f.close();
	
	log_v("N %s NL %u| SCRC %u <=> %u CCRC|PL %u", name, head.name_len, head.crc, crc16(data, head.data_len), head.data_len);
	
	return pos + head.name_len + head.data_len;
}

ErrorStatus FileExchangeHandler::processPackageImport(const char* pkgname)
{
	log_i("Begin parsing pkg file");
	if(!LittleFS.exists(pkgname)){
		log_w("ENC FILE NOT EXIST %s", pkgname);
		return ErrorStatus::ON_NOT_EXIST;
	}
	
	File enc_pkg_file = LittleFS.open(pkgname, FILE_READ);
	
	if(!enc_pkg_file){
		log_w("ENC FILE OPEN ERR");
		return ErrorStatus::ON_OPEN;
	}
	
	if(LittleFS.exists(dec_pkg_name)){
		log_v("REMOVE OLD DEC FILE");
		LittleFS.remove(dec_pkg_name);
	}
	
	dec_pkg_file = LittleFS.open(dec_pkg_name, FILE_APPEND);
	
	if(!dec_pkg_file){
		log_w("DEC FILE OPEN ERR");
		return ErrorStatus::ON_OPEN;
	}
	
	const size_t enc_len = 400;
	uint8_t pkg[enc_len];
	
	size_t pkg_size = enc_pkg_file.size();
	
	size_t pos = 0;
	
	while(pos < pkg_size){
		
		size_t bytes_read = (pkg_size-pos < enc_len) ? pkg_size - pos : enc_len;
		log_v("POS %u | LEN %u", pos, bytes_read);
		enc_pkg_file.seek(pos);
		enc_pkg_file.read(pkg, bytes_read);
		pos += bytes_read;
	
		size_t dec_len = base64_decode_expected_len(bytes_read);
	
		uint8_t buf[dec_len];
		base64_decodestate state;
		base64_init_decodestate(&state);
	
		int len = base64_decode_block((const char*)pkg, bytes_read, (char*)buf, &state);
		dec_pkg_file.write(buf, len);
	}
	
	dec_pkg_file.close();
	enc_pkg_file.close();
	LittleFS.remove(pkgname);
	
	dec_pkg_file = LittleFS.open(dec_pkg_name, FILE_READ);
	
	if(!dec_pkg_file) {
		log_w("DEC FILE OPEN ERR");
		return ErrorStatus::ON_OPEN;
	}
	
	pkg_size = dec_pkg_file.size();
	pos = 0;
	
	while(pos < pkg_size){
		pos = separateFileFromPackageImport(pos);
	}
	
	dec_pkg_file.close();
	LittleFS.remove(dec_pkg_name);
	log_i("Finished parsing, success");
	return ErrorStatus::OK;
}

//ctor
FileExchangeHandler::FileExchangeHandler() :
	config_pkg(ConfigPackage::FULL),
	dec_pkg_name("/cfg/dec_cfg")
{
}

FileExchangeHandler* FileExchangeHandler::singleton()
{
	static FileExchangeHandler* instance = nullptr;
	if(instance == nullptr){
		instance = new FileExchangeHandler();
	}
	return instance;
}

//dtor
FileExchangeHandler::~FileExchangeHandler()
{
}

ErrorStatus FileExchangeHandler::writeToExportTempFile(File file)
{
	
	log_i("Begin writing %s to tmp file", file.name());
	Header head;
	head.name_len = strlen(file.name());
	
	head.data_len = file.size();
	log_d("CREATE BUF W %u", head.data_len);
	uint8_t payload[head.data_len];
	
	size_t pkg_size = head.len + head.name_len + head.data_len;
	uint8_t pkg[pkg_size];
	uint8_t* pkg_ptr = pkg;
	
	log_d("READ %u", file.read(payload, head.data_len));
	
	
	head.crc = crc16(payload, head.data_len);
	
	log_v("CRC W %u", head.crc);
	
	pkg_ptr = SerializablePOD<uint16_t>::serialize(pkg_ptr, head.crc);
	pkg_ptr = SerializablePOD<uint16_t>::serialize(pkg_ptr, head.data_len);
	pkg_ptr = SerializablePOD<uint8_t>::serialize(pkg_ptr, head.name_len);
	pkg_ptr = SerializablePOD<char>::serializeArray(pkg_ptr, file.name(), head.name_len);
	pkg_ptr = SerializablePOD<uint8_t>::serializeArray(pkg_ptr, payload, head.data_len);
	
	dec_pkg_file.write(pkg, pkg_size);
	
	log_i("Finished writing %s to tmp file", file.name());
	
	return ErrorStatus::OK;
}

ErrorStatus FileExchangeHandler::prepareExportTempFile()
{
	
	log_i("Preparing temp file for download");
	if(LittleFS.exists(dec_pkg_name)){
		log_v("Remove tmp file");
		LittleFS.remove(dec_pkg_name);
	}
	
	dec_pkg_file = LittleFS.open(dec_pkg_name, FILE_APPEND);
	
	File file;
	if(config_pkg & ConfigPackage::BASIC){
		file = LittleFS.open(BasicConfigHandle::singleton()->configFile().c_str(), FILE_READ);
		
		if(!file){
			log_w("ERR could not open OV cfg");
			return ErrorStatus::ON_OPEN;
		}
			
		writeToExportTempFile(file);
		
		file.close();
	}
	
	if(config_pkg & ConfigPackage::MODULE){
		File cfgs = LittleFS.open("/cfg/mod");
		
		if(cfgs){
			log_v("Take cfgs from /cfg/mod");
		
			file = cfgs.openNextFile();
			
			while(file){
				writeToExportTempFile(file);
				file.close();
				file = cfgs.openNextFile();
			}
		}
	}
	dec_pkg_file.close();
	
	log_i("Finished preparation of temp file");
	return ErrorStatus::OK;
}

ErrorStatus FileExchangeHandler::prepareExportPackage(const char* pkgname)
{
	if(config_pkg == ConfigPackage::NONE){
		log_i("Nothing selected");
		return ErrorStatus::NO_FILE_SEL;
	}
	
	log_i("Preparing %s package for download", pkgname);
	if(strcmp(pkgname, "Basic.ocp") == 0){
		config_pkg = ConfigPackage::BASIC;
	} else if(strcmp(pkgname, "Module.ocp") == 0){
		config_pkg = ConfigPackage::MODULE;
	} else {
		config_pkg = ConfigPackage::FULL;
	}
	
	prepareExportTempFile();
	
	if(!LittleFS.exists(dec_pkg_name)){
		log_w("ERR Could not find dec file");
		return ErrorStatus::ON_NOT_EXIST;
	}
	
	
	if(LittleFS.exists(pkgname))
	{
		log_v("Remove old package");
		LittleFS.remove(pkgname);
	}	
	
	
	log_d("Begin B64 ENC");
	File enc_pkg_file = LittleFS.open(pkgname, FILE_APPEND);
	dec_pkg_file = LittleFS.open(dec_pkg_name, FILE_READ);
	
	const uint16_t size = dec_pkg_file.size();
	const size_t len = 300;
	uint8_t data[len];
		
	uint16_t pos = 0;
	while(pos < size){
		size_t bytes_to_read = (size-pos < len) ? size-pos : len;
		log_v("POS %u | LEN %u", pos, bytes_to_read);
		dec_pkg_file.seek(pos);
		dec_pkg_file.read(data, bytes_to_read);
		
		pos += bytes_to_read;
		
		const size_t enclen = base64_encode_expected_len(bytes_to_read)+1;
		uint8_t buf[enclen];
		
		base64_encodestate state;
		base64_init_encodestate(&state);
		int enc_len = base64_encode_block((const char*)data, bytes_to_read, (char*)buf, &state);
		enc_len += base64_encode_blockend(((char*)buf + enc_len), &state);
		
		enc_pkg_file.write(buf, enc_len);
	}
	
	dec_pkg_file.close();
	enc_pkg_file.close();
	
	LittleFS.remove(dec_pkg_name);
	
	log_i("Finished preparation of cfg package, ready for download");
	return ErrorStatus::OK;
}

};
//EOF
