/*generated file persistence.hpp*/
#ifndef PERSISTENCE_HPP
#define PERSISTENCE_HPP

#include <Arduino.h>

//#include "../simples/string.hpp"
#include "../utils/timer.hpp"
#include "serializable.hpp"

namespace Persistence {
void printU8Array(uint8_t* ptr, size_t len);
uint16_t crc16(uint8_t const *data, size_t size);

enum ErrorStatus 
{
	OK 				= 0,
	NO_CHANGES		= 1,
	NO_FILE_SEL 	= 2,
	ON_OPEN			= 3,
	ON_NOT_EXIST	= 4,
	ON_WRITE		= 5,
	EMPTY			= 6,
};

class IPersistence 
{
private:
	Utils::Timer32 updateTimer;
	const ISerializable::Type p_type;
protected:
	uint16_t m_crc;

	uint8_t read();
	uint8_t write();
	
	virtual const std::string& configFile() const = 0;
    //ctor
    IPersistence(ISerializable::Type t);
public:
    //dtor
    virtual ~IPersistence();
    
    void begin();    
    void update(unsigned long expireInterval = 0);
    bool setBackToLastRestorePoint();
};

class IModuleConfigHandle 
: 	public IPersistence,
	public ISerializable
{
private:
protected:
	std::string config_file;
	
	IModuleConfigHandle() : 
		IPersistence(ISerializable::Type::MODULE_CFG),
		ISerializable(ISerializable::Type::BASIC_CFG),
		config_file() {}
	
public:
	virtual ~IModuleConfigHandle() = default;

	void setConfigFile(const char* new_config);
	void writeToOtherConfigFile(const char* config);
	
    //inherited functions
	virtual const std::string& configFile() const;
	
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
};

class BasicConfigHandle : public IPersistence
{
private:
	const std::string config_file;
	BasicConfigHandle() : 
		IPersistence(ISerializable::Type::BASIC_CFG),
		config_file("/cfg/basic_cfg") {}
protected:
public:
	static BasicConfigHandle* singleton();
	
	virtual ~BasicConfigHandle() { }
	
	virtual const std::string& configFile() const;
};

};
#endif //PERSISTENCE_HPP
