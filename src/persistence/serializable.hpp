/*generated file serializable.hpp*/
#ifndef SERIALIZABLE_HPP
#define SERIALIZABLE_HPP

#include "../utils/linkedlist.hpp"

#define FILE_VERSION 2

namespace Persistence {

class ISerializable 
{
private:
	static Utils::LinkedList<ISerializable> list_ov;
	static Utils::LinkedList<ISerializable> list_dvc;
	
	static uint16_t s_extFileVersion;
	static constexpr uint16_t fileVersion() { return FILE_VERSION; }
protected:  
	static uint8_t s_statusFlags[2];
    
public:
	enum Type {
		BASIC_CFG,
		MODULE_CFG
	};
	const ISerializable::Type m_type;
	enum {
		RESTART_REQUEST = 1,
		SER_READY_TO_COMMIT = 2,
		SER_NOTIFY_ABOUT_CHANGES = 4,
		SER_NEW_VALUE_IN_VERSION = 8,
		SER_LAST = 16
	};
	    
    virtual ~ISerializable();
    
	virtual size_t size() = 0;
	virtual uint8_t* serialize(uint8_t *eptr) = 0;
	virtual const uint8_t* deserialize(const uint8_t *eptr) = 0;
	
	virtual void enableFlag(uint8_t flag);
	virtual void disableFlag(uint8_t flag = 0xFF);
	
	static size_t s_totalSize(Type t);
    static void s_serialize(uint8_t *eptr, Type t);
    static void s_deserialize(const uint8_t *eptr, Type t);
    
    static bool s_matchFileVersion() { return s_extFileVersion >= fileVersion(); }
	static void s_enableFlag(uint8_t flag, Type t = Type::MODULE_CFG) { s_statusFlags[t] |= flag; }
	static void s_disableFlag(uint8_t flag, Type t = Type::MODULE_CFG) { s_statusFlags[t] &= ~flag; }
	static bool s_flagEnabled(uint8_t flag, bool orConstrained = true, Type t = Type::MODULE_CFG);
protected:
    ISerializable(Type t = Type::MODULE_CFG);
};

};
#endif //SERIALIZABLE_HPP
