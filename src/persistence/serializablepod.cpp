/*generated file serializablepod.cpp*/
#include "serializablepod.hpp"

namespace Persistence {

template <typename POD>
size_t SerializablePOD<POD>::size(POD p)
{
		return sizeof(POD);
}

template <typename POD>
size_t SerializablePOD<POD>::size(POD *p, size_t len)
{
		return sizeof(POD)*len;
}

template <typename POD>
uint8_t* SerializablePOD<POD>::serialize(uint8_t* target, const POD& value)
{
		return (uint8_t*)(memcpy(target, &value, size(value))+size(value));
}

template <typename POD>
const uint8_t* SerializablePOD<POD>::deserialize(const uint8_t* source, POD& target){
		memcpy( &target, source, size(target));
		return source + size(target);
}

template <typename POD>
uint8_t* SerializablePOD<POD>::serializeArray(uint8_t* target, const POD* array, size_t len)
{
		return (uint8_t*)(memcpy(target, array, size(*array)*len) + size(*array)*len);
}

template <typename POD>
const uint8_t* SerializablePOD<POD>::deserializeArray(const uint8_t* source, POD* target, size_t len)
{
		memcpy( target, source, size(*target)*len );
		return source + size(*target) * len;
}

template class SerializablePOD<float>;
template class SerializablePOD<uint8_t>;
template class SerializablePOD<int8_t>;
template class SerializablePOD<char>;
template class SerializablePOD<int16_t>;
template class SerializablePOD<uint16_t>;
template class SerializablePOD<int32_t>;
template class SerializablePOD<uint32_t>;

template <typename POD>
uint8_t* SerializablePOD<POD>::serializeString(uint8_t* target, const std::string& source){
    return (uint8_t*)(memcpy(target, source.c_str(), source.size()) + source.size());

}
template <typename POD>
const uint8_t* SerializablePOD<POD>::deserializeString(const uint8_t* source, std::string& target){
    uint8_t l = 0;
	source = SerializablePOD<uint8_t>::deserialize(source, l);
    if(l > 0){
        char str[l + 1];
        source = SerializablePOD<char>::deserializeArray(source, str, l);
        str[l] = 0;
        target = str;
    }
    return source + target.size();
}

};
//EOF
