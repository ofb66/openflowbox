#include "iweb.hpp"

uint8_t IWeb::s_id = 1;
uint16_t IWeb::s_settings = 0;
Utils::Timer16 IWeb::intervalTimer;

IWeb* IWeb::s_root_ptr = nullptr;
IWeb* IWeb::s_load_ptr = nullptr;
//bool IWeb::final = false;
//size_t IWeb::s_buf_index = 0;
//size_t IWeb::s_buf_len = 0;

std::queue<IWeb*> IWeb::jsonDynQueue;
std::queue<IWeb*> IWeb::jsonUiQueue;
std::queue<IWeb*> IWeb::jsonQueue;

IWeb::IWeb(IWeb* const parent) :
	m_parent(parent),
	m_id(s_id),
	m_flags(0),
	m_children(std::vector<IWeb*>()),
	m_it(m_children.end())
{
	++s_id;
	if(m_parent != nullptr){
		m_parent->addChildElement(this);
	}
}

uint16_t IWeb::createId(uint8_t varId)
{
	return ((uint16_t)m_id) << 8 | varId;
}

IWeb::~IWeb(){
	m_children.clear();
}

uint8_t IWeb::webId() const {
	return m_id;
}

bool IWeb::getObjById(uint8_t id, IWeb*& foundObj)
{
	foundObj = nullptr;
	if(s_root_ptr){
		s_root_ptr->getById(id, foundObj);
	}
	if(!foundObj) return false;
	
	return true;
}

void IWeb::getById(uint8_t id, IWeb*& foundObj){
	if(id == m_id){
		foundObj = this;
	} else if(foundObj == nullptr){
		std::vector<IWeb*>::iterator it;
		for(it = m_children.begin(); it != m_children.end(); it++){
			(*it)->getById(id, foundObj);
		}
	}
}

void IWeb::setFlag(uint16_t flag, bool enable) {
	if(enable){
		this->enableFlag(flag);
	} else {
		this->disableFlag(flag);
	}
}

void IWeb::enableFlag(uint16_t flag) {
	m_flags |= flag;
	//s_enable(setting);
}

void IWeb::disableFlag(uint16_t flag){
	m_flags &= ~(flag);
}

void IWeb::toggleFlag(uint16_t flag){
    m_flags ^= flag;
}

void IWeb::setParent(IWeb* const parent){
	if(m_parent == nullptr){
		m_parent = parent;
		m_parent->addChildElement(this);
	}
}

IWeb* IWeb::nextSibling()
{
	IWeb* next = nullptr;
	if(m_parent && this != m_parent->m_children.back()){
		auto it = std::find(m_parent->m_children.begin(), m_parent->m_children.end(), this);
		++it;
		next = *it;
	}
	return next;
}

void IWeb::addChildElement(IWeb* const child){
	if(std::find(m_children.begin(), m_children.end(), child) == m_children.end()){
		m_children.push_back(child);
		child->setParent(this);
	}
}

bool IWeb::removeChildElement(IWeb*& child){
	if(removeChildElement(child->webId())){
		child = nullptr;
		return true;
	}
	return false;
}

bool IWeb::removeChildElement(uint16_t id){
	IWeb *child = nullptr;
	getById(id, child);
	if(child != nullptr){
		delete child;
		child = nullptr;
		return true;
	}
	return false;
}

const std::vector<IWeb*>& IWeb::getChildren(){
	return m_children;
}

size_t IWeb::load_page_elements(uint8_t* buffer, size_t maxlen, IWeb*& cptr)
{
	static size_t index = 0;
	
	if(!cptr) return 0;
	//static IWeb* cptr = s_root_ptr;
	
	//if(!cptr) {
	//	cptr = s_root_ptr;
	//}
	
	//Serial.printf("1)CPTR ID %u\n", cptr->webId());
	/*
	if(index == 0){
		if(!s_load_ptr) s_load_ptr = s_root_ptr;
		else {
			if(s_load_ptr->isFlagSet(HEAD_DELIVERED)){
				s_load_ptr->disableFlag(HEAD_DELIVERED);
				s_load_ptr = s_load_ptr->nextSibling();
				if(s_load_ptr){
					s_load_ptr->disableFlag(HEAD_DELIVERED);
				} else {
					s_load_ptr = s_load_ptr->parent();
					if(s_load_ptr)
						s_load_ptr->enableFlag(HEAD_DELIVERED);
				}
			} else {
				s_load_ptr->enableFlag(HEAD_DELIVERED);
				if(s_load_ptr->m_children.size() > 0){
					s_load_ptr = s_load_ptr->m_children.front();
					s_load_ptr->disableFlag(HEAD_DELIVERED);
				} else if(s_load_ptr->len(StrObject::FOOTER) == 0) {
					s_load_ptr->disableFlag(HEAD_DELIVERED);
					s_load_ptr = s_load_ptr->nextSibling();
					if(!s_load_ptr) s_load_ptr = s_load_ptr->parent();
				}
			}
		}
	}
	
	if(!s_load_ptr) return 0;
	*/
	
	StrObject cSo = cptr->isFlagSet(HEAD_DELIVERED) ? StrObject::FOOTER : StrObject::HEADER;
	
	size_t len = cptr->len(cSo);
	size_t cpy_buf_len_real = 0, cpy_buf_len = 0;
	uint8_t temp_buffer[len];
	
	len = cptr->copy(cSo, temp_buffer, len);
	
	cpy_buf_len_real = len - index;
	cpy_buf_len = cpy_buf_len_real;
	
	if(cpy_buf_len > maxlen) cpy_buf_len = maxlen;
	
	memcpy(buffer, &temp_buffer[index], cpy_buf_len);
	index += cpy_buf_len;
	//all copied => next element
	if(cpy_buf_len_real == cpy_buf_len) {
		//Serial.printf("2)ID %u\n", cptr->webId());
		//Serial.printf("ID %u L %u CL %u SO %s\n", cptr->webId(), len, cpy_buf_len, (cSo==StrObject::HEADER) ? "HEAD" : "FOOT");
		index = 0;
		if(cptr->isFlagSet(HEAD_DELIVERED) || (cptr->m_children.size() == 0 && cptr->len(StrObject::FOOTER) == 0))
		{
			//Serial.println("GET NEXT SIB OR PAR");
			cptr->disableFlag(HEAD_DELIVERED);
			auto sib = cptr->nextSibling();
			if(!sib) cptr = cptr->parent();
			else { 
				cptr = sib;
				cptr->disableFlag(HEAD_DELIVERED);
			}
		} else {
			cptr->enableFlag(HEAD_DELIVERED);
			if(cptr->m_children.size() > 0){
				//Serial.println("GET NEXT CHILD");
				cptr = cptr->m_children.front();
				cptr->disableFlag(HEAD_DELIVERED);
			}
		}
		//if(cptr) Serial.printf("3)ID %u\n", cptr->webId());
	}
	return cpy_buf_len;
	//Copied header =>
	//1)(Get children || footer) == 0 then 2)
	//Copied footer =>
	//2)Get sibling  || parent
	/*
	StrObject cSo = s_load_ptr->isFlagSet(HEAD_DELIVERED) ? StrObject::FOOTER : StrObject::HEADER;
	size_t len = s_load_ptr->len(cSo);
		
	size_t buf_len = 0;
	
	
	uint8_t temp_buffer[len];
	
	buf_len = s_load_ptr->copy(cSo, temp_buffer, len);
	
	size_t cpy_buf_len_real = buf_len - index;
	size_t cpy_buf_len = cpy_buf_len_real;
	if(cpy_buf_len > maxlen){
		cpy_buf_len = maxlen;
	}
	
	memcpy(buffer, &temp_buffer[index], cpy_buf_len);
	index += cpy_buf_len;
	
	if(cpy_buf_len_real <= maxlen){
		index = 0;
		if(s_load_ptr->isFlagSet(HEAD_DELIVERED)){
			s_load_ptr->disableFlag(HEAD_DELIVERED);
			if(s_load_ptr->m_parent && s_load_ptr != s_load_ptr->m_parent->m_children.back()){
				auto it = std::find(s_load_ptr->m_parent->m_children.begin(), s_load_ptr->m_parent->m_children.end(), s_load_ptr);
				auto next_index = std::distance(s_load_ptr->m_parent->m_children.begin(), it) + 1;
				s_load_ptr = s_load_ptr->m_parent->m_children[next_index];
				s_load_ptr->disableFlag(HEAD_DELIVERED);
			} else {
				s_load_ptr = s_load_ptr->m_parent;
			}
		} else {
			s_load_ptr->enableFlag(HEAD_DELIVERED);
			if(s_load_ptr && s_load_ptr->m_children.size() > 0){
				bool idFound = false;
				s_load_ptr = *s_load_ptr->m_children.begin();
				s_load_ptr->disableFlag(HEAD_DELIVERED);
			} else if(s_load_ptr->len(StrObject::FOOTER) == 0){
				s_load_ptr->disableFlag(HEAD_DELIVERED);
				getNextElement(s_load_ptr);
			}
		}
	}
	return cpy_buf_len;
	*/ 
}

void IWeb::getNextElement(IWeb*& currPtr){
	if(currPtr->m_children.size() > 0){
		currPtr = *currPtr->m_children.begin();
	} else if(currPtr->m_parent && currPtr->m_parent->m_children.back() != currPtr){
		//getNextSibling
		auto it = std::find(s_load_ptr->m_parent->m_children.begin(), s_load_ptr->m_parent->m_children.end(), s_load_ptr);
		auto next_index = std::distance(s_load_ptr->m_parent->m_children.begin(), it) +  1;
		currPtr = currPtr->m_parent->m_children[next_index];
	} else {
		currPtr = currPtr->m_parent;
	}
}

void IWeb::resetBeforeLoad() { 
	s_load_ptr = nullptr; 
	//final = false;
	//s_buf_len = 0;
	intervalTimer.start();
}

size_t IWeb::handleJson(uint8_t *buffer, size_t len, StrObject so)
{
	if(jsonQueue.empty()) return 0;
	
	IWeb* ptr = nullptr;
	size_t indx = 0, nextlen = 0;
	
	buffer[indx++] = '{';
	buffer[indx++] = '"';
	buffer[indx++] = 'J';
	buffer[indx++] = '"';
	buffer[indx++] = ':';
	buffer[indx++] = '[';	
	
	ptr = jsonQueue.front();
	nextlen = ptr->len(StrObject::JSON_UI) + ptr->len(StrObject::JSON_DYN); 
	
	do {
			
		if(ptr->isFlagSet(HAS_UI_CHANGE)){
			indx += ptr->copy(StrObject::JSON_UI, &buffer[indx], ptr->len(StrObject::JSON_UI));
			buffer[indx++] = ',';
		}
		if(ptr->isFlagSet(HAS_CHANGE)){
			indx += ptr->copy(StrObject::JSON_DYN, &buffer[indx], ptr->len(StrObject::JSON_DYN));
			buffer[indx++] = ',';
		}
		
		ptr->disableFlag(HAS_UI_CHANGE | HAS_CHANGE | ALREADY_QUEUED);
		jsonQueue.pop();
		
		if(!jsonQueue.empty()){
			ptr = jsonQueue.front();
			nextlen = ptr->len(StrObject::JSON_UI) + ptr->len(StrObject::JSON_DYN); 
		}
	} while(len>(indx+nextlen+20) && !jsonQueue.empty());
	
	if(buffer[indx-1] == '\0' || buffer[indx-1] == ',') --indx;
	
	buffer[indx++] = ']';
	buffer[indx++] = '}';
	buffer[indx] = '\0';
	
	return indx;
}

bool IWeb::jsonAvailable(StrObject so){
	
	if(intervalTimer.hasExpired(1000)) { 
		intervalTimer.start();
		s_settings |= INTERVAL;
	} else {
		s_settings &= ~INTERVAL;
	}
	return !jsonQueue.empty();
}

size_t IWeb::getJsonLen(StrObject so){
	
	size_t len = 0;
	
	if(jsonQueue.empty()) return len;
	
	IWeb* ptr = jsonQueue.front();
	if(ptr->isFlagSet(HAS_UI_CHANGE)){
		len += ptr->len(StrObject::JSON_UI);
	} 
	if(ptr->isFlagSet(HAS_CHANGE | INTERVAL)){
		len += ptr->len(StrObject::JSON_DYN);
	}
	return len;
}

void IWeb::pushToJsonQueue()
{	
	if(isFlagSet(INTERVAL) && s_settings & INTERVAL){
		enableFlag(HAS_CHANGE);
	}
	
	if(isFlagSet(HAS_UI_CHANGE | HAS_CHANGE) && !isFlagSet(ALREADY_QUEUED)){
		enableFlag(IWeb::ALREADY_QUEUED);
		jsonQueue.push(this);
	}
	
	std::vector<IWeb*>::iterator it;
	for(it = m_children.begin(); it != m_children.end(); it++){
		(*it)->pushToJsonQueue();
	}
}

void IWeb::handleData(uint8_t varId, void* const &payload){
	//Nothing to do here
}
/*
void IWeb::createWebsite(File &file)
{
	IWeb* curr = s_root_ptr;
	
	StrObject currObj = StrObject::HEADER;
	
	while(curr){
		const size_t len = curr->len(currObj);
		if(len!=0){
			uint8_t buf[len];
			size_t clen = curr->copy(currObj, buf, len);
			if(buf[clen-1] == 0){
				--clen;
			}
			file.write(buf, clen);
		}
		if(currObj == StrObject::HEADER){
			if(curr->m_children.size() > 0){
				curr = curr->m_children[0];
			} else {
				currObj = StrObject::FOOTER;
			}
		} else {
			if(curr->m_parent && curr != curr->m_parent->m_children.back()){
                auto it = std::find(curr->m_parent->m_children.begin(), curr->m_parent->m_children.end(), curr);
                auto next_index = std::distance(curr->m_parent->m_children.begin(), it) +  1;
                curr = curr->m_parent->m_children[next_index];
                currObj = StrObject::HEADER;
			} else {
				curr = curr->m_parent;
			}
		}
	}
	resetOnLoad();
}
*/
