/*generated file credentials.cpp*/
#include "credentials.hpp"

#include "../persistence/serializablepod.hpp"
using namespace Persistence;

//ctor
Credentials::Credentials()
	: ISerializable(ISerializable::Type::BASIC_CFG), name(), password()
{
}

Credentials::Credentials(const char* _name, const char* _password)
	: ISerializable(ISerializable::Type::BASIC_CFG), name(_name), password(_password)
{
	
}
//dtor

Credentials::~Credentials()
{
	name.clear();
	password.clear();
}

size_t Credentials::size(){
	return name.size() + password.size();
}
uint8_t* Credentials::serialize(uint8_t *eptr){
    eptr = SerializablePOD<char>::serializeString(eptr, name);
    eptr = SerializablePOD<char>::serializeString(eptr, password);
	//eptr = name.serialize(eptr);
	//eptr = password.serialize(eptr);
	return eptr;
}
const uint8_t* Credentials::deserialize(const uint8_t *eptr){
	
    eptr = SerializablePOD<char>::deserializeString(eptr, name);
    eptr = SerializablePOD<char>::deserializeString(eptr, password);
	//eptr = name.deserialize(eptr);
	//eptr = password.deserialize(eptr);
	
	return eptr;
}

//inherited functions


//EOF
