#ifndef DEVICESETTINGS_HPP
#define DEVICESETTINGS_HPP

#include <Arduino.h>
#include "iweb.hpp"

class DeviceSettings : public IWeb
{
protected:
	static const char s_footer[] PROGMEM;
	static const char s_symbol_select[] PROGMEM;
public:
	virtual ~DeviceSettings() {}
};

class PlugHandle12VSettings : public DeviceSettings
{
private:
	static const char s_header[] PROGMEM;
public:
	static PlugHandle12VSettings* singleton();

	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
};

class PlugHandleRemoteSettings : public DeviceSettings
{
private:
	static const char s_header[] PROGMEM;
	
public:
	static PlugHandleRemoteSettings* singleton();
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
};

class PWMHandleSettings : public DeviceSettings
{
private:
	static const char s_header[] PROGMEM;
	
public:
	static PWMHandleSettings* singleton();
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
};

#endif 
