#include "rootdoc.hpp"

#include "../hw/sensorhandle.hpp"
#include "../hw/plughandle.hpp"
#include "../hw/pwmhandle.hpp"
#include "../utils/timer.hpp"
#include "websettings.hpp"
#include "devicetime.hpp"
#include "configmanager.hpp"

//#include "../eeprom/eeprommanager.hpp"

#include "../version.h"

#include "../persistence/serializable.hpp"
using namespace Persistence;

RootDoc* RootDoc::m_root = nullptr;

const char RootDoc::s_header[] PROGMEM = R"=====(<!DOCTYPE html>
<html lang='en'>
	<head>
		<meta charset='utf-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1' />
		<title>OpenFlowbox</title>
		<link rel='icon' href='/svg/leaf.svg' type='image/x-icon'>
		<link rel='stylesheet' type='text/css' href='/css/style.min.css'/>
		<script defer src='/js/solid_custom.min.js' type='text/javascript'></script>
		<script defer src='/js/fontawesome.min.js' type='text/javascript'></script>
		<script defer src='/js/page_script.min.js' type='text/javascript'></script>
	</head>
	<body>
		<header>
			<nav>
				<div>
					<i class='fas fa-info'></i> |
					<i class='fas fa-clock'></i> <span id='%u' class='dtime1' data-timeformat=':'>00:00:00</span> | 
					<i class='fas fa-calendar-alt'></i> <span id='%u' class='dtime1' data-timeformat='wdh'>000days</span> | 
					<i class='fas fa-thermometer-half'></i> <span id='%u'>00.0</span>°C | 
					<i class='fas fa-tint'></i> <span id='%u'>00.0</span>%% | 
					<i class='fas fa-water'></i> <span id='%u'>00.0</span>%% |
					<input type='checkbox' class='allb nccrb bulb' id='%u'/><label for='%u'><span class='rbui'></span></label> |
					<i class='fas fa-wifi'></i> <span id='%u'></span> | 
					<i class='fas fa-memory'></i> <span id='%u'>000</span>/%uKB |
					<i class='fas fa-cogs'></i> <span id='%u'>%s</span>
				</div>
			</nav>
			<div style='height: 2.5em;'></div>
			<img src='svg/leaf.png' style='width: 20em;'>
		</header>
		<main id='%u'>
)=====";

const char RootDoc::s_footer[] PROGMEM = R"=====(		</main>
		<div class='icon-bar'>
			<button type='button' id='%u' onclick='confirmClickThroughUser(this, "Save changes?");' class='icon-bar-btn'>
				<span class='fa-layers fa-fw'>
				<i class='fas fa-save'></i>
				<input type='checkbox' class='allb nccrb warntriangle' id='%u'/><label for='%u'><span class='rbui'></span></label>
				</span>
			</button><button type='button' id='%u' onclick='confirmClickThroughUser(this, "Factory reset?\nAll made changes and configurations will be lost after next restart!");' class='icon-bar-btn'>
				<i class='fas fa-history'></i>
			</button><button type='button' id='%u' onclick='if(confirmClickThroughUser(this, "Restart device? (All unsaved changes will be lost!)")) reload(5);' class='icon-bar-btn'>
				<span class='fa-layers fa-fw'>
				<i class='fas fa-redo-alt' data-fa-transform='rotate-255'></i>
				<i class='fas fa-power-off'></i>
				<input type='checkbox' class='allb nccrb warntriangle' id='%u' />
				<label for='%u'><span class='rbui'></span></label>
				</span>
			</button><button type='button' id='%u' onclick='if(confirmClickThroughUser(this, "Logout?")) sendReq("logout");'  class='icon-bar-btn'>
				<i class='fas fa-sign-out-alt'></i>
			</button>
		</div>		
		<footer>
			<p>Uptime 
			<span id='%u' class='dtime1' data-timeformat='wdhms'></span>
			| Version %s 
			| IP <span class='ip' data-ip='%u'></span> 
			| %s %s</p>
		</footer>
		<script>
			setTimeout(function(){
				init();
			}, 500);
		</script>
	</body>
</html>
)=====";

const char RootDoc::s_json_dyn[] PROGMEM = R"=====({ 
"%u":%lu,
"%u":%u,
"%u":%5.2f,
"%u":%5.2f,
"%u":%5.2f,
"%u":%u,
"%u":"%s",
"%u":%u,
"%u":%u,
"%u":%u,
"%u":%llu,
"%u":"%s"
})=====";


RootDoc::RootDoc(IWeb* const parent)
: IWeb(parent)
{
    IWeb::enableFlag(IWeb::INTERVAL);
    
    //auto ih = InfoHandle::singleton();
    //ih->setParent(this);
	
    auto sh = SensorHandle::singleton();
    sh->setParent(this);


    auto ph = PWMHandle::singleton();
    ph->setParent(this);

    auto ph12V = PlugHandle12V::singleton();
    ph12V->setParent(this);
    
    auto phr = PlugHandleRemote::singleton();
    phr->setParent(this);

    auto ws = WebSettings::singleton();
    ws->setParent(this);
}
//IWeb

size_t RootDoc::len(IWeb::StrObject so)
{
	//size_t len = strlen(ConfigManager::singleton()->getCurrentConfig());
	size_t len = ModuleConfigManager::singleton()->configFile().size();
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 50 + len;
		case StrObject::FOOTER:
		return strlen_P(s_footer) + 100;
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 100 + len;
		default:
		return 0;
	}
}

size_t RootDoc::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	bool anyCommits = ISerializable::s_flagEnabled(ISerializable::SER_READY_TO_COMMIT, true, ISerializable::Type::BASIC_CFG) || 
						ISerializable::s_flagEnabled(ISerializable::SER_READY_TO_COMMIT, true, ISerializable::Type::MODULE_CFG);
	auto cfgm = ModuleConfigManager::singleton();
	switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							createId(0), createId(1), createId(2),
							createId(3), createId(4),
							createId(5), createId(5), 
							createId(6), createId(7), DISP_HEAP,
							createId(15), (cfgm->configFile().empty()) ? " " : &cfgm->configFile().c_str()[9],
							webId()
						);
        break;
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer,
							createId(8), createId(9), createId(9),
							createId(10), 
							createId(11), createId(12), createId(12),
							createId(13),
							createId(14),
							VERSION, (uint32_t)WiFi.localIP(), 
							__DATE__, __TIME__
						);
        break;
        case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn,
							createId(0), DeviceTime::singleton()->getEpochTime()%86400,
							createId(1), DeviceTime::singleton()->getTotalRuntimeSec(),
							createId(2), SensorHandle::singleton()->getAverage(SensorValue::Type::DEGREE),
							createId(3), SensorHandle::singleton()->getAverage(SensorValue::Type::HUMIDITY),
							createId(4), SensorHandle::singleton()->getAverage(SensorValue::Type::SOIL_MOISTURE),
							createId(5), SensorHandle::singleton()->isLightOn(),
							createId(6), (WebSettings::singleton()->wifiMode()== WIFI_AP) ? PSTR("AP") : PSTR("STA"),
							createId(7), DISP_HEAP - (ESP.getFreeHeap()>>10),
							createId(9), anyCommits,
							createId(12), ISerializable::s_flagEnabled(ISerializable::RESTART_REQUEST, true, ISerializable::Type::BASIC_CFG),
							createId(14), DeviceTime::singleton()->getCurrentRuntimeSec(),
							createId(15), (cfgm->configFile().empty()) ? " " : &cfgm->configFile().c_str()[9]
						);
        break;
        default:
        return 0;
    }
}

RootDoc* RootDoc::singleton()
{
    if(m_root == nullptr){
        m_root = new RootDoc();
        m_root->setRoot();        
    }
    return m_root;
}

void RootDoc::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 8){
		log_d("Save changes");
		BasicConfigHandle::singleton()->update();
		ModuleConfigManager::singleton()->update();
	} else if (varId == 10) {
		log_d("Restore defaults");
		//BasicConfigHandle::singleton()->setBackToLastRestorePoint();
		ModuleConfigManager::singleton()->setBackToLastRestorePoint();
	} else if (varId == 11) {
		log_d("Restart device");
		IWeb::requestESPRestart();
	}
}
