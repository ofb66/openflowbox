#ifndef ROOTDOC_HPP
#define ROOTDOC_HPP

#include <Arduino.h>

#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#else
#include <ESP8266WiFi.h>
#endif

#include "iweb.hpp"

#ifdef ARDUINO_ARCH_ESP32
#define DISP_HEAP	320
#else
#define DISP_HEAP	80
#endif

class RootDoc : public IWeb/*, public ISerializable*/
{
private:
    static RootDoc* m_root;
    
	static const char PROGMEM s_header[];
	static const char PROGMEM s_footer[];
	static const char PROGMEM s_json_dyn[];
	
    RootDoc(IWeb* const parent = nullptr);
    
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
public:
    static RootDoc* singleton();
	virtual void handleData(uint8_t varId, void* const &payload) override;
};

#endif
