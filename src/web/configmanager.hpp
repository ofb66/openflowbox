/*generated file configmanager.hpp*/
#ifndef CONFIGMANAGER_HPP
#define CONFIGMANAGER_HPP

#include <Arduino.h>
#include "iweb.hpp"
#include "../persistence/persistence.hpp"
#include "../persistence/serializable.hpp"

#define OPT_BUF_MAX_LEN	100

class ModuleConfigManager : 
	public IWeb, 
	public Persistence::IModuleConfigHandle
{
private:
	static const char s_header[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	char opt_buf[OPT_BUF_MAX_LEN];
	
	const char* mod_cfg_dir;
	
	uint8_t m_sindx;

    ModuleConfigManager(IWeb *parent = nullptr);
    
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
	size_t getOptLen();
	void createOptions();
protected:
public:
	static ModuleConfigManager* singleton();
    //dtor
    virtual ~ModuleConfigManager();
    
	virtual void handleData(uint8_t varId, void* const &payload) override;
};
#endif //CONFIGMANAGER_HPP
