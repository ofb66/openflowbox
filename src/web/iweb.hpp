#ifndef IWEB_HPP
#define IWEB_HPP

#include <Arduino.h>
#include <FS.h>
#include <vector>
#include <algorithm>
#include <queue>

#include "../utils/timer.hpp"

class IWeb {

private:
	IWeb* m_parent;
	uint8_t m_id;
	
	static IWeb* s_root_ptr;
	static IWeb* s_load_ptr;
	//static size_t s_buf_index;
	//static size_t s_buf_len; 
	
	static std::queue<IWeb*> jsonDynQueue;
	static std::queue<IWeb*> jsonUiQueue;
	static std::queue<IWeb*> jsonQueue;
	
protected:
	uint16_t m_flags;
	
	std::vector<IWeb*> m_children;
	std::vector<IWeb*>::iterator m_it;

	IWeb(IWeb* const parent = nullptr);
	
	inline void setRoot() { s_root_ptr = this; s_load_ptr = this; }
public:
	enum StrObject {
		HEADER,
		FOOTER,
		JSON_DYN,
		JSON_UI
	};
	enum {
		RESTART_ESP		= 0x0001,
		REFRESH_PAGE	= 0x0002,
		HAS_CHANGE		= 0x0004,
		HAS_UI_CHANGE	= 0x0008,
		INTERVAL		= 0x0010,
		HEAD_DELIVERED	= 0x0020,
		BUF_OVERFLOW	= 0x0040,
		ALREADY_QUEUED	= 0x0080,
		LAST			= 0x0100,
        ALL             = 0xFFFF
	};

	virtual ~IWeb();

	uint16_t createId(uint8_t varId);
	uint8_t webId() const;
	void getById(uint8_t id, IWeb*& foundObj);

	void setFlag(uint16_t flag, bool enable);
	void enableFlag(uint16_t flag = IWeb::ALL);
	void disableFlag(uint16_t flag = IWeb::ALL);
    void toggleFlag(uint16_t flag);
    inline uint16_t flags() const { return m_flags; }
	inline bool isFlagSet(uint16_t flag = IWeb::ALL, bool orConstraint = true) const { return (orConstraint) ? m_flags & flag : (m_flags & flag) == flag; }
	
	void setParent(IWeb* const parent);
	IWeb * parent() { return m_parent; }
	
	IWeb* nextSibling();

	void addChildElement(IWeb* const child);
	bool removeChildElement(IWeb*& child);
	bool removeChildElement(uint16_t id);
	const std::vector<IWeb*>& getChildren();
	
	//Override only, if you want to update values in a derived class
	virtual void handleData(uint8_t varId, void* const &payload);

	void pushToJsonQueue();

private:
	static Utils::Timer16 intervalTimer;
	static uint8_t s_id;
	static uint16_t s_settings;
	//static bool final;
protected:
	virtual size_t len(StrObject so) = 0;
	virtual size_t copy(StrObject so, uint8_t* buffer, size_t len) = 0;
	
public:
	static IWeb* root() { return s_root_ptr; }
	static bool getObjById(uint8_t id, IWeb *& foundObj);
	
	static void requestESPRestart() { s_settings |= (RESTART_ESP | REFRESH_PAGE); }
	static bool restartESPRequested() { bool restart = s_settings & RESTART_ESP; s_settings &= ~RESTART_ESP; return restart; }
	
	static void requestPageRefresh() { s_settings |= REFRESH_PAGE; }
	static bool pageRefreshRequested() { bool refresh = s_settings & REFRESH_PAGE; s_settings &= ~REFRESH_PAGE; return refresh; }
	
	static size_t load_page_elements(uint8_t* buffer, size_t maxlen, IWeb*& cptr);
	static void getNextElement(IWeb*& currPtr);
	static IWeb* load_ptr() { return s_load_ptr; }
	//static bool isFinal() { return final; }
	static void resetBeforeLoad();
	
	static size_t handleJson(uint8_t *buffer, size_t len, StrObject so);
	static bool jsonAvailable(StrObject so);
	static size_t getJsonLen(StrObject so);
	
	//static void createWebsite(File &file);
};


#endif
