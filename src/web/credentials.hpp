/*generated file credentials.hpp*/
#ifndef CREDENTIALS_HPP
#define CREDENTIALS_HPP

#include <Arduino.h>

//#include "../simples/string.hpp"
#include <string>
#include "../persistence/serializable.hpp"

class Credentials : public Persistence::ISerializable
{
private:
protected:
public:
	std::string name;
	std::string password;
    //ctor
    Credentials();
    Credentials(const char* _name, const char* _password = "");
    //dtor
    ~Credentials();
    
    bool areEmpty() const { return name.empty() && password.empty(); }
        
    //inherited functions
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);

};
#endif //CREDENTIALS_HPP
