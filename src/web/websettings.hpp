#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <Arduino.h>

#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#include "esp_wifi.h"
#else 
#include <ESP8266WiFi.h>
#endif

#include <WiFiUdp.h>
#include <NTPClient.h>

#include "iweb.hpp"
#include "credentials.hpp"
//#include "../simples/string.hpp"

#include "../persistence/serializable.hpp"
using namespace Persistence;

#define STA_NAME_MAX_LEN 32
#define STA_LIST_MAX_LEN 5

uint16_t getChipId();

class WebSettings : public IWeb, public ISerializable {
private:
	static const char s_header[] PROGMEM;
	static const char s_footer[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	WiFiMode_t m_wifi_mode;
	bool m_ap_hidden;
	bool authentication_enabled;
	

	std::string m_hostname;
	
	Credentials ap_credentials;
	Credentials sta_credentials;
	Credentials user_credentials;
	
	wifi_sta_list_t wifi_sta_list;
	tcpip_adapter_sta_list_t adapter_sta_list;
	
	std::string m_jsonSTAStr;
	size_t m_staJsonStrLen;
	
	WebSettings(IWeb* const parent = nullptr);
	
public:
	static WebSettings* singleton();

	inline const std::string& dnsHostname() const { return m_hostname; }
	inline bool authEnabledAndValid() { return authentication_enabled && !user_credentials.areEmpty(); }
	inline Credentials& userCredentials() { return user_credentials; }
	inline Credentials& staCredentials() { return sta_credentials; }
	inline Credentials& apCredentials() { return ap_credentials; }
	
	void scanWifi();
	uint16_t getIPTableLen();
	void getIPTable(char* buf);
	
	inline WiFiMode_t wifiMode() const { return m_wifi_mode; }
	inline void setWifiMode(WiFiMode_t wt){ m_wifi_mode = wt; }
	
	size_t getSTAJsonStrLen();
	
	inline bool apHidden() const { return m_ap_hidden; }
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
    virtual void handleData(uint8_t varId, void* const &payload) override;
    
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
};

#endif
