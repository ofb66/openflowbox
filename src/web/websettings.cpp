#include "websettings.hpp"

#include "devicesettings.hpp"
#include "configmanager.hpp"
#include "devicetime.hpp"
#include "../persistence/serializablepod.hpp"
using namespace Persistence;


#ifdef ARDUINO_ARCH_ESP32
#define HOSTNAME "flowbox_32"
uint16_t getChipId(){
	uint32_t id = (ESP.getEfuseMac() >> 24) & 0xFFFF00;
	return ((id >> 16) & 0xFF) | (id & 0x1F00); //Use only 13bits to fit a 4 digit number (max 8192)
}
#else
#define HOSTNAME "flowbox_8266"
uint16_t getChipId(){
	return ESP.getChipId();
}
#endif

const char WebSettings::s_header[] PROGMEM = R"=====(				<button type='button' class='collapsible'><i class='fas fa-cogs fa-2x'></i></button>
				<div class='content'>
					<article style='text-align: left;'>
						<button class='collapsible' type='button'><i class='fas fa-wifi'></i></button>
						<div class='content'>
							<div>
								<p><label for='%u'>Hostname(mDNS):</label></p>
								<p><input type='text' onchange='sendText(this)' id='%u' value='%s'><label for='%u'>.local</label></p>
							</div>
							<hr>
							<p>
							<input type='radio' onclick='sendNumber(this);' %s name='wifi_mode' id='%u' value='%u' style='cursor: pointer;'>
							<label for='%u' style='cursor: pointer;'>WiFi AccessPoint</label>
							</p>
							<p class='credits'>
							<label for='%u'><i class='fas fa-broadcast-tower'></i></label><input type='text' id='%u' value='%s' placeholder='Name AP (SSID)' onchange='sendText(this);'>
							</p>
							<p class='credits'>
							<label for='%u'><i class='fas fa-key'></i></label><input type='password' id='%u' placeholder='Enter password (optional)' onchange='sendText(this);'>
							<span name='passwd'><i class='fas fa-eye'></i></span>
							</p>
							<p>
							<input type='checkbox' id='%u' onclick='sendBool(this)' %s>
							<label for='%u'>Hide AP</label>
							</p>
							<hr>
							<p>
							<input type='radio' onclick='sendNumber(this);' %s name='wifi_mode' id='%u' value='%u' style='cursor: pointer;'>
							<label for='%u' style='cursor: pointer;'>Connect to WiFi network</label>
							</p>
							<p>
							<select name='WSR' id='%u' style='width:75%%;' onchange='parentElement.nextElementSibling.children[1].value = this.value'>
							</select><!--
						 --><button type='button' onclick='sendClick(this);' class='progressbtn' style='width:15%%;' id='%u'><i class='fas fa-sync-alt'></i></button>
							</p>
							<p class='credits'>
							<label for='%u'><i class='fas fa-wifi'></i></label><input type='text' id='%u' value='%s' placeholder='Network (SSID)' onchange='sendText(this);'>
							</p>
							<p class='credits'>
								<label for='%u'><i class='fas fa-key'></i></label><!--
							 --><input type='password' id='%u' placeholder='Enter password' onchange='sendCredentials(parentElement.previousElementSibling.children[1],this);'>
							<span name='passwd'><i class='fas fa-eye'></i></span>
							</p>
						</div>
					</article>
					<article style='text-align: left;'>
						<button class='collapsible' type='button'><i class='fas fa-user-lock'></i></button>
						<div class='content'>
							<p>
							<input type='checkbox' onclick='sendBool(this);' id='%u' style='cursor: pointer;' %s>
							<label for='%u' style='cursor: pointer;'>Enable authentication</label>
							</p>
							<p class='credits'>
							<label for='%u'><i class='fas fa-user'></i></label><input type='text' id='%u' value='%s' placeholder='Username' onchange='sendText(this);'>
							</p>
							<p class='credits'>
							<label for='%u'><i class='fas fa-key'></i></label><input type='password' id='%u' placeholder='Enter password' onchange='sendText(this);'>
							<span name='passwd'><i class='fas fa-eye'></i></span>
							</p>
						</div>
					</article>
)=====";


const char WebSettings::s_footer[] PROGMEM = R"=====(				</div>
)=====";

const char WebSettings::s_json_ui[] PROGMEM = R"=====({
"%u":%u,
"%u":"%s",
"%u":%u,
"%u":"%s",
"%u":%u,
"%u":"%s",
"%u":"%s"
})=====";

WebSettings::WebSettings(IWeb* const parent)
: IWeb(parent), ISerializable(ISerializable::Type::BASIC_CFG),
	m_wifi_mode(WIFI_AP),
	m_ap_hidden(false),
	authentication_enabled(false),
	m_hostname(HOSTNAME)
{	
	ModuleConfigManager::singleton()->setParent(this);
	DeviceTime::singleton()->setParent(this);
	PlugHandle12VSettings::singleton()->setParent(this);
	PlugHandleRemoteSettings::singleton()->setParent(this);
	PWMHandleSettings::singleton()->setParent(this);
	
#ifdef ARDUINO_ARCH_ESP32
	ap_credentials.name = "OFB32_";
#else	
	ap_credentials.name = "OFB8266_";
#endif
	ap_credentials.name += getChipId();
	ap_credentials.password = "flowerpower";
	
	memset(&wifi_sta_list, 0, sizeof(wifi_sta_list));
	memset(&adapter_sta_list, 0, sizeof(adapter_sta_list));
	
	m_jsonSTAStr.reserve(STA_NAME_MAX_LEN * STA_LIST_MAX_LEN + STA_LIST_MAX_LEN*15);
}

size_t WebSettings::getSTAJsonStrLen()
{
	return  + STA_LIST_MAX_LEN + 20;
}

void WebSettings::scanWifi(){
	
	int n = WiFi.scanComplete();
	if(n == -2){
		WiFi.scanNetworks(true);
	} else if(n > 0){
		IWeb::enableFlag(IWeb::HAS_CHANGE);
		if(n > STA_LIST_MAX_LEN){
			n = STA_LIST_MAX_LEN;
		}
		
		m_staJsonStrLen = 0;
		m_jsonSTAStr = "{\"WSR\":[";
		for(int i = 0; i < n; i++){
			if(i>0){
				m_jsonSTAStr += ",";
			}
			m_jsonSTAStr += "\"";
			m_jsonSTAStr += WiFi.SSID(i).c_str();
			m_jsonSTAStr += "\"";
		}
		m_jsonSTAStr += "]}";
		log_v("%u, %s",
						m_jsonSTAStr.size(), 
						m_jsonSTAStr.c_str());
						
		WiFi.scanDelete();
		if(WiFi.scanComplete() == -2){
			WiFi.scanNetworks(true);
		}
	}
}

uint16_t WebSettings::getIPTableLen()
{
	if(m_wifi_mode == WIFI_STA){
		return 30;
	}
	
	esp_wifi_ap_get_sta_list(&wifi_sta_list);
	tcpip_adapter_get_sta_list(&wifi_sta_list, &adapter_sta_list);
		
	return adapter_sta_list.num * 90 + 250;
}

void WebSettings::getIPTable(char* buf)
{
	if(m_wifi_mode == WIFI_STA){
		strcpy(buf, PSTR("Only available in AP mode"));
		return;
	}
	int indx = 0;
	indx = sprintf(buf, PSTR("<!DOCTYPE html><html><head><style>td, th {border:1px solid #dddddd;text-align:left;padding:8px;} tr:nth-child(even) {background-color:#dddddd;}</style></head><body><table style='border:'><tr><th>No</th><th>IP</th><th>MAC</th></tr>"));
	
	for(uint8_t u = 0; u < adapter_sta_list.num; u++){
		tcpip_adapter_sta_info_t station = adapter_sta_list.sta[u];
		char mac[30];
		sprintf(mac, PSTR("%02X:%02X:%02X:%02X:%02X:%02X"),
							station.mac[0],station.mac[1],
							station.mac[2],station.mac[3],
							station.mac[4],station.mac[5]
						);
		indx += sprintf(&buf[indx], PSTR("<tr><td>%u</td><td>%s</td><td>%s</td></tr>"),
						u+1, IP2STR(&(station.ip)), mac
						);
	}
	
	indx += sprintf(&buf[indx], PSTR("</table></body></html>"));
	buf[indx] = 0;
}

size_t WebSettings::len(IWeb::StrObject so)
{
	uint16_t namelen = 0;
	namelen += m_hostname.size();
	namelen += ap_credentials.name.size();
	namelen += sta_credentials.name.size();
	namelen += user_credentials.name.size();
	
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 26*3 + 60 + namelen;
		case StrObject::FOOTER:
		return strlen_P(s_footer);
		case StrObject::JSON_DYN:
		return m_jsonSTAStr.size() + 10;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 15 + namelen;
		default:
		return 0;
	}
}

size_t WebSettings::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
	bool isAP = (m_wifi_mode == WIFI_AP);
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							createId(10), createId(10), m_hostname.c_str(), createId(10),
							(isAP)?PSTR("checked"):PSTR(""), createId(0), WIFI_AP, createId(0),
							createId(1), createId(1), (!ap_credentials.name.empty()) ? ap_credentials.name.c_str() : "OFB_ESP32",
							createId(2), createId(2),
							createId(9), (m_ap_hidden) ? PSTR("checked") : PSTR(""), createId(9),
							(!isAP)?PSTR("checked"):PSTR(""), createId(3), WIFI_STA, createId(3),
							createId(11), createId(12),
							createId(4), createId(4), (!sta_credentials.name.empty()) ? sta_credentials.name.c_str() : "",
							createId(5), createId(5),
							createId(6), (authentication_enabled)?PSTR("checked"):PSTR(""), createId(6), 
							createId(7), createId(7), (!user_credentials.name.empty()) ? user_credentials.name.c_str() : "",
							createId(8), createId(8)
						);
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        case StrObject::JSON_DYN:
        return snprintf((char*)buffer, len, m_jsonSTAStr.c_str());
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
								createId((isAP) ? 0 : 3), 1,
								createId(1), (!ap_credentials.name.empty()) ? ap_credentials.name.c_str() : "",
								createId(9), (uint8_t)m_ap_hidden,
								createId(4), (!sta_credentials.name.empty()) ? sta_credentials.name.c_str() : "",
								createId(6), (uint8_t)authentication_enabled,
								createId(7), (!user_credentials.name.empty()) ? user_credentials.name.c_str() : "",
								createId(10), m_hostname.c_str()
							);
        default:
        return 0;
    }
}
void WebSettings::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 0 || varId == 3) m_wifi_mode = (WiFiMode_t)*(int*)payload;
	else if(varId == 1) ap_credentials.name = (const char*)payload;
	else if(varId == 2) ap_credentials.password = (const char*)payload;
	else if(varId == 4) sta_credentials.name = (const char*)payload;
	else if(varId == 5) sta_credentials.password = (const char*)payload;
	else if(varId == 6) authentication_enabled = *(bool*)payload;
	else if(varId == 7) user_credentials.name = (const char*)payload;
	else if(varId == 8) user_credentials.password = (const char*)payload;
	else if(varId == 9) m_ap_hidden = *(bool*)payload;
	else if(varId == 10) m_hostname = (const char*)payload;
	else if(varId == 12) scanWifi();
	if(varId <= 10){
		IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
		ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
		if(varId < 5){
			ISerializable::enableFlag(ISerializable::RESTART_REQUEST);
		} else {
			IWeb::requestPageRefresh();
		}
	}
}
WebSettings* WebSettings::singleton()
{
	static WebSettings *m_instance = nullptr; 
	if(m_instance == nullptr){
		m_instance = new WebSettings();
	}
	return m_instance;
}

size_t WebSettings::size()
{
	return sizeof(authentication_enabled) + sizeof(m_wifi_mode) + sizeof(m_ap_hidden) + m_hostname.size();
}

uint8_t* WebSettings::serialize(uint8_t *eptr)
{
	eptr = SerializablePOD<uint8_t>::serialize(eptr, (uint8_t)authentication_enabled);
	eptr = SerializablePOD<uint8_t>::serialize(eptr, (uint8_t)m_wifi_mode);
	eptr = SerializablePOD<uint8_t>::serialize(eptr, (uint8_t)m_ap_hidden);
    eptr = SerializablePOD<char>::serializeString(eptr, m_hostname);

	return eptr;
}

const uint8_t* WebSettings::deserialize(const uint8_t *eptr)
{
	uint8_t auth, mode, hidden;
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, auth);
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, mode);
	eptr = SerializablePOD<uint8_t>::deserialize(eptr, hidden);
    eptr = SerializablePOD<char>::deserializeString(eptr, m_hostname);
	authentication_enabled = auth;
	m_wifi_mode = (WiFiMode_t)mode;
	m_ap_hidden = hidden;
	return eptr;
}
