/*generated file devicetime.cpp*/
#include "devicetime.hpp"

#include "../persistence/serializablepod.hpp"
using namespace Persistence;

const char DeviceTime::s_header[] PROGMEM = R"=====(					<article style='text-align: left;'>
						<button class='collapsible' type='button'><i class='fas fa-clock'></i></button>
						<div class='content'>
							<table style='text-align:left;'>
							<colgroup>
							<col span="1" style="width: 30%%;">
							<col span="1" style="width: 70%%;">
							</colgroup>
							<tr>
								<td><label for='%u'><i class='fas fa-calendar-alt'></i></label></td>
								<td><input type='text' class='dtime2' id='%u' onchange='sendTime(this)' value='00W 00D 00h' placeholder='grow progress'></td>
							</tr>
							<tr>
							<td>
							<label for='%u'>
								<span class='fa-layers fa-fw'>
								<i class='fas fa-globe-europe'></i>
								<span class='fa-layers-text' data-fa-transform='shrink-5 down-11 right-16' style='font-weight:700'>UTC</span>
								</span>
							</label>
							</td>
							<td>
							<button type='button' name='numCntDown' class='progressin progressbtn'><i class='fas fa-minus'></i></button><!--
						 --><input type='number' class='progressin progressinput' id='%u' value='%i'  min='-12' max='14' onchange='sendNumber(this);'><!--
						 --><button type='button' name='numCntUp' class='progressin progressbtn'><i class='fas fa-plus'></i></button>
							</td>
							</tr>
							<tr>
							<td>
							<label for='%u'>
								<span class='fa-layers fa-fw'>
								<i class='fas fa-globe-europe'></i>
								<span class='fa-layers-text' data-fa-transform='shrink-5 down-11 right-16' style='font-weight:700'>NTP</span>
								</span>
							</label>
							</td>
							<td>
							<input type='text' id='%u' value='%s' onchange='sendText(this)' placeholder='NTP server'>
							</td>
							</tr>
							</table>
						</div>
					</article>
)=====";

const char DeviceTime::s_json_dyn[] PROGMEM = R"=====({
"%u":%u
})=====";

const char DeviceTime::s_json_ui[] PROGMEM = R"=====({
"%u":%i,
"%u":"%s"
})=====";


DeviceTime::DeviceTime(IWeb* const parent) :
IWeb(parent),
ISerializable(ISerializable::Type::BASIC_CFG),
m_offset(2),
m_runtime_sec(0),
m_pool_server("pool.ntp.org"),
sync_client_time(0),
sync_timer_start(0),
udp_ptr(nullptr),
ntp_client_ptr(nullptr),
m_mode(Mode::USE_DEVC_TIMESTAMP)
{
	runTimer.start();
}

DeviceTime* DeviceTime::singleton(){
	static DeviceTime *instance = nullptr;
	if(instance == nullptr){
		instance = new DeviceTime();
	}
	return instance;
}

void DeviceTime::setOffset(int32_t offset)
{
	m_offset = offset;
	if(ntp_client_ptr){
		ntp_client_ptr->setTimeOffset(m_offset);
	}
}

unsigned long DeviceTime::getEpochTime()
{
	if(m_mode == Mode::USE_NTP_SERVER) return ntp_client_ptr->getEpochTime();
	
	return  sync_client_time + m_offset +
			((runTimer.runtime() - sync_timer_start) / 1000);
}

void DeviceTime::begin(bool connectedToInternet){
	if(connectedToInternet){
		m_mode = Mode::USE_NTP_SERVER;		
		if(ntp_client_ptr == nullptr){
			if(udp_ptr == nullptr){
				udp_ptr = new WiFiUDP();
			}
			ntp_client_ptr = new NTPClient(*udp_ptr, m_pool_server.c_str(), m_offset/*offset in sec*/, 3600000/*ntp update server request interval in ms*/);
		}
		ntp_client_ptr->begin();
	} else {
		m_mode = Mode::USE_DEVC_TIMESTAMP;
				
		if(ntp_client_ptr){
			delete ntp_client_ptr;
			ntp_client_ptr = nullptr;
			if(udp_ptr){
				udp_ptr->stop();
				delete udp_ptr;
				udp_ptr = nullptr;
			}
		}
	}
}

void DeviceTime::update()
{
	if(m_mode == Mode::USE_NTP_SERVER){
		if(ntp_client_ptr == nullptr){
			begin(true);
		}
		if(!ntp_client_ptr->update()){
			begin(false);
		}
	}
	
	uint32_t rsec = getCurrentRuntimeSec();
	static uint32_t sec = rsec;
	if((rsec % 60) == 0 && sec != rsec){
		sec = rsec;
		IWeb::enableFlag(IWeb::HAS_CHANGE);
	}
}

void DeviceTime::syncToClient(long long clientUnixTime)
{
	sync_client_time = clientUnixTime;
	sync_timer_start = runTimer.runtime();
}

size_t DeviceTime::len(IWeb::StrObject so)
{
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 6*3 + m_pool_server.size();
		case StrObject::JSON_DYN:
		return strlen_P(s_json_dyn) + 10;
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui) + 15 + m_pool_server.size();
		default:
		return 0;
	}
}

size_t DeviceTime::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							createId(0), createId(0),
							createId(1), createId(1), getOffsetInHours(),
							createId(2), createId(2), m_pool_server.c_str()
						);
		case StrObject::JSON_DYN:
        return snprintf_P((char*)buffer, len, s_json_dyn, createId(0), getTotalRuntimeSec());
        case StrObject::JSON_UI:
        return snprintf_P((char*)buffer, len, s_json_ui,
							createId(1), getOffsetInHours(),
							createId(2), m_pool_server.c_str()
						);
        default:
        return 0; 
	}       
}

size_t DeviceTime::size()
{
	return sizeof(m_runtime_sec) + sizeof(m_offset) + m_pool_server.size();
}

uint8_t* DeviceTime::serialize(uint8_t *eptr)
{
	eptr = SerializablePOD<uint32_t>::serialize(eptr, getTotalRuntimeSec());
	eptr = SerializablePOD<int32_t>::serialize(eptr, m_offset);
    eptr = SerializablePOD<char>::serializeString(eptr, m_pool_server);
	return eptr;
}

const uint8_t* DeviceTime::deserialize(const uint8_t *eptr)
{
	eptr = SerializablePOD<uint32_t>::deserialize(eptr, m_runtime_sec);
	eptr = SerializablePOD<int32_t>::deserialize(eptr, m_offset);
    eptr = SerializablePOD<char>::deserializeString(eptr, m_pool_server);
	return eptr;
}

void DeviceTime::handleData(uint8_t varId, void* const &payload)
{
	if(varId == 0){
		m_runtime_sec = *(uint32_t*)payload - getCurrentRuntimeSec();
	} else if(varId == 1){
		setOffset((*(int*)payload) * 3600);
	} else if(varId == 2){
		m_pool_server = (char*)payload;
		if(ntp_client_ptr){
			ntp_client_ptr->setPoolServerName(m_pool_server.c_str());
		}
	}
	IWeb::enableFlag(IWeb::HAS_UI_CHANGE);
	ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
}


//EOF
