#ifndef WEB_HPP
#define WEB_HPP

#include <Arduino.h>

#ifdef ARDUINO_ARCH_ESP32
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPmDNS.h>
#include <LittleFS.h>
#else
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESP8266mDNS.h>
#endif

#include <ArduinoOTA.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>

#include "rootdoc.hpp"


namespace WEB
{
    static AsyncWebServer server(80);
    static AsyncWebSocket ws("/ws");

	bool initWifiAP();
	bool initWifiSTA();
	void handleUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final);
    void begin();
    void update();
    
    void notFound(AsyncWebServerRequest *request);

    void onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *payload, size_t len);
    void handleWebsocketData(uint8_t* payload, size_t len);
    void passOnDataToTarget(IWeb* target, uint8_t varId, uint8_t* payload, size_t len);
    
    //void createWebsite();
};

#endif
