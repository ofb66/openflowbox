#include "web.hpp"

#include "websettings.hpp"
#include "configmanager.hpp"
#include "devicetime.hpp"
#include "../persistence/fileexchangehandler.hpp"

#include "../utils/timer.hpp"

#include <esp_task_wdt.h>

#define WIFI_STA_CONN_RETRIES	5

const char UPDATE_PAGE[] PROGMEM =  R"=====(
	<!DOCTYPE html>
	<html>
	<head>
    <meta charset="utf-8">
	<title>Page Title</title>
	</head>
	<body>
	<h3>Upload files</h3>
	<form enctype="multipart/form-data" method="POST">
		<input type="file" name="upload" multiple/><br><br>
		<button type='submit' style='width:100%%'>Upload</button>
	</form>
	</body>
	</html>
)=====";


bool WEB::initWifiAP(){
	auto settings = WebSettings::singleton();
	
	auto apc = settings->apCredentials();
	
	WiFi.mode(settings->wifiMode());
	WiFi.softAP(apc.name.c_str(), apc.password.c_str(), 1, settings->apHidden());
	
	return true;
}

bool WEB::initWifiSTA()
{
	static uint8_t retryCount = 0;
	auto settings = WebSettings::singleton();
	if(settings->wifiMode() == WIFI_AP){
		return false;
	}
	
	auto stac = settings->staCredentials();
	if(stac.areEmpty()){
		settings->setWifiMode(WIFI_AP);
		return false;
	}
	WiFi.mode(settings->wifiMode());
	WiFi.begin(stac.name.c_str(), stac.password.c_str());
	
	if (WiFi.waitForConnectResult() != WL_CONNECTED) {
		log_w("WiFi Failed!");
		WiFi.disconnect();
		WiFi.mode(WIFI_OFF);
		++retryCount;
		if(retryCount < WIFI_STA_CONN_RETRIES){
			log_w("Try again");
			return initWifiSTA();
		}
		retryCount = 0;
		settings->setWifiMode(WIFI_AP);
		return false;
	}
	return true;
}

void WEB::handleUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final){
	if(!index){
		log_d("UploadStart: %s", filename.c_str());
		String filepath = F("/");
		if(filename.endsWith(".js.gz")){
			filepath += F("js/");
		} else if(filename.endsWith(".css.gz")){
			filepath += F("css/");
		} else if(filename.endsWith(".svg") || filename.endsWith(".png")){
			filepath += F("svg/");
		} else if(filename.endsWith(".woff2")){
			filepath += F("webfonts/");
		}
		/*
		if(!LittleFS.exists(filepath)){
			LittleFS.mkdir(filepath);
		}*/
		filepath += filename;
		if(LittleFS.exists(filepath)){
			LittleFS.remove(filepath);
		}
		// open the file on first call and store the file handle in the request object
		request->_tempFile = LittleFS.open(filepath, FILE_WRITE);
	}
	if(len) {
		// stream the incoming chunk to the opened file
		request->_tempFile.write(data,len);
	}
	if(final){
		log_d("UploadEnd: %s,%u", filename.c_str(), index+len);
		// close the file handle as the upload is now done
		request->_tempFile.close();
		//auto response = request->beginResponse(200);
		//response->addHeader("redirected", "false");
		//request->send(response);
		if(filename.endsWith(".ocp")){
			filename = String("/") + filename;
			if(FileExchangeHandler::singleton()->processPackageImport(filename.c_str())==Persistence::ErrorStatus::OK){
				ESP.restart();
			}
		}
		request->redirect("/update");
		//ws.textAll("\"R\":[{\"tos\":1}]");
	}
}

void WEB::begin(){
		
	//Simple::DebugTimer dt;
	//dt.start();
	//createWebsite();
	//dt.stop();
	
	bool connectedToInternet = false;
	if(initWifiSTA()){
		connectedToInternet = true;
		log_i("ip= %s", WiFi.localIP().toString());
		//WiFi.setAutoReconnect(true);
		//WiFi.persistent(true);
		WiFi.setSleep(false);
	} else {
		initWifiAP();
		log_i("ip= %s", WiFi.softAPIP().toString());
	}

	//server.serveStatic("/", LittleFS, "/index.html").setDefaultFile("index.html");

	
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){    
        
        auto settings = WebSettings::singleton();
        auto uc = settings->userCredentials();
        if(settings->authEnabledAndValid() && !request->authenticate(uc.name.c_str(), uc.password.c_str())){
			return request->requestAuthentication();
		}
		     
		//auto settings = WebSettings::singleton();
		//settings->scanWifi();
		//yield();
		//request->send(LittleFS, "/index.html", "text/html");
        
        //esp_task_wdt_init(30, true);
        //Serial.print("Page req ");
        log_d("Page req ");
        IWeb::resetBeforeLoad();
        if(ws.enabled()) ws.enable(false);
        
		AsyncWebServerResponse *response = request->beginChunkedResponse("text/html", [](uint8_t *buffer, size_t maxLen, size_t index) -> size_t {
					
			//if(IWeb::load_ptr()){
				//size_t maxLen_tmp = maxLen;
				static IWeb* cptr = IWeb::root();
				
				if(!cptr) { 
					if(!ws.enabled()) ws.enable(true);
					cptr = IWeb::root();
					return 0;
				}
				const size_t buffer_size = 1500;
				if(buffer_size < maxLen){
					maxLen = buffer_size;
				}
				size_t indx = 0;
				size_t len_copy = 0;
				bool getMore = true;
				while (getMore){
					len_copy = IWeb::load_page_elements(&buffer[indx], maxLen, cptr);
					maxLen -= len_copy;
					indx += len_copy;
					if(buffer[indx-1] == 0){
						//ignore null char to not serve it with the webpage, otherwise leads to an ugly page which will not work properly
						--indx;
						++maxLen;
					}
					if(maxLen == 0 || len_copy == 0) getMore = false;
				}
				return indx;
				/*
				if(IWeb::load_ptr()) {
					return len;
				} else {
					if(!ws.enabled()) ws.enable(true);
					log_i("Page load finished");
					return 0;
				}
				*/ 
			//} else {
				//IWeb::resetBeforeLoad();
				//if(!ws.enabled()) ws.enable(true);
				//log_i("Page load finished");
				//esp_task_wdt_init(3, true);
				//return 0;
			//}
		});
		
		request->send(response);
		
    });

    server.on("/con_ips", HTTP_GET, [](AsyncWebServerRequest *request){ 
		auto settings = WebSettings::singleton();
				
		const size_t table_len = settings->getIPTableLen();
		char buf[table_len];
		settings->getIPTable(buf);
		request->send(200, "text/html", buf);
	});
	
    server.on("/css/style.min.css", HTTP_GET, [](AsyncWebServerRequest *request){
        auto response = request->beginResponse(LittleFS, PSTR("/css/style.min.css.gz"), PSTR("text/css"));
        response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
        request->send(response);
    });

	server.on("/css/fontawesome.min.css", HTTP_GET, [](AsyncWebServerRequest *request){
        auto response = request->beginResponse(LittleFS, PSTR("/css/fontawesome.min.css.gz"), PSTR("text/css"));
        response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
        request->send(response);
	});
    
	server.on("/js/fontawesome.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
        auto response = request->beginResponse(LittleFS, PSTR("/js/fontawesome.min.js.gz"), PSTR("application/javascript"));
        response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
        request->send(response);
	});

	server.on("/js/solid_custom.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
        auto response = request->beginResponse(LittleFS, PSTR("/js/solid_custom.min.js.gz"), PSTR("application/javascript"));
        response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
        request->send(response);
	});
	
	server.on("/js/page_script.min.js", HTTP_GET, [](AsyncWebServerRequest *request){
        auto response = request->beginResponse(LittleFS, PSTR("/js/page_script.min.js.gz"), PSTR("application/javascript"));
        response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
        request->send(response);
	});
    server.on("/webfonts/fa-solid-900.woff2", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send(LittleFS, "/webfonts/fa-solid-900.woff2", "font/woff2");
    });
	
	server.on("/svg/leaf.svg", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send(LittleFS, "/svg/leaf.svg", "image/svg+xml");
	});

	server.on("/svg/leaf.png", HTTP_GET, [](AsyncWebServerRequest *request){
		request->send(LittleFS, "/svg/leaf.png", "image/png");
	});
	
	server.on("/logout", HTTP_GET, [](AsyncWebServerRequest *request){
        auto settings = WebSettings::singleton();
        if(settings->userCredentials().areEmpty()) {
			request->send(200);
		} else {
			request->send(401);
		}
	});
	
	server.on("/download", HTTP_GET, [](AsyncWebServerRequest *request){
		auto feh = FileExchangeHandler::singleton();
		
		const char* pkgname = request->getParam(0)->name().c_str();
		feh->prepareExportPackage(pkgname);
		request->send(LittleFS, pkgname, String(), true);
	});

	server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request){
        //auto response = request->beginResponse(LITTLEFS, PSTR("/index.html"), PSTR("text/html"));
        //response->addHeader(PSTR("Content-Encoding"), PSTR("gzip"));
        request->send_P(200, "text/html", UPDATE_PAGE);
	});

	
	server.onFileUpload(handleUpload);

    server.onNotFound(notFound);

    ws.onEvent(onWsEvent);
    server.addHandler(&ws);

	server.begin();

	if (!MDNS.begin(WebSettings::singleton()->dnsHostname().c_str())) {
		log_w("Error setting up MDNS responder!");
	}
	MDNS.addService("http", "tcp", 80);

	//TODO determine if device is connected to internet to use ntp server as time server
	DeviceTime::singleton()->begin(connectedToInternet);

    ArduinoOTA.setHostname(WebSettings::singleton()->dnsHostname().c_str());
    ArduinoOTA.begin();
}

void WEB::update(){
#ifndef ARDUINO_ARCH_ESP32
	ws.cleanupClients(3);
	MDNS.update();
#endif

	DeviceTime::singleton()->update();
	
	if(ws.count()>0 && ws.availableForWriteAll()){
		IWeb::root()->pushToJsonQueue();
		if(IWeb::pageRefreshRequested()){
			log_d("ESP RST");
			if(IWeb::restartESPRequested()){ 
				ws.textAll("{\"R\":[{\"tos\":10,\"rst\":0}]}");
				ESP.restart();
			}
			else { 
				ws.textAll("{\"R\":[{\"tos\":3,\"rst\":0}]}");
			}
		}
		if(IWeb::jsonAvailable(IWeb::StrObject::JSON_UI)){
			size_t len = 2048; //IWeb::getJsonLen(IWeb::StrObject::JSON_UI);
			uint8_t buffer[len];
			len = IWeb::handleJson(buffer, len, IWeb::StrObject::JSON_DYN);
			ws.textAll(buffer, len);
		}		
	}
	ArduinoOTA.handle();
    
}

void WEB::notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void WEB::onWsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *payload, size_t len){
    if(type == WS_EVT_CONNECT){
        log_d("Websocket client connection received");
    } else if(type == WS_EVT_DISCONNECT){
        log_d("Client disconnected");
    } else if(type == WS_EVT_DATA){
        AwsFrameInfo* info = (AwsFrameInfo*)arg;
        if(info->final && info->index == 0 && info->len == len){
            if(info->opcode == WS_TEXT){
                payload[len] = 0;
                handleWebsocketData(payload, len);
            }
        }
    }
}

void WEB::handleWebsocketData(uint8_t *payload, size_t len)
{
	log_v("Content: %s", (const char*)payload);
	char* pEnd;
	if(payload[0] == 'T'){
		auto ts = strtoll((const char *)&payload[1], &pEnd, 10);
		DeviceTime::singleton()->syncToClient(ts);
		return;
	}
	
	uint16_t ids = strtol((const char *)payload, &pEnd, 10);
	uint8_t devId = ids >> 8;
	uint8_t varId = ids & 0xFF;
	log_d("DID: %u, VID: %u, ID: %u", devId, varId, ids);
	
	IWeb* targetObject = nullptr;
	if(IWeb::getObjById(devId, targetObject)){
		passOnDataToTarget(targetObject, varId, payload, len);
	}
}

void WEB::passOnDataToTarget(IWeb* target, uint8_t varId, uint8_t* payload, size_t len)
{
		uint8_t indx = 0;
		indx = strcspn((const char *)payload, "DTCNBt");
		
		char payloadType = payload[indx];
		unsigned long l_val;
		float d_val;
		
		void* data_ptr = nullptr;
		++indx;
		if(payloadType == 'D'){
			d_val = strtod((const char *)&payload[indx], NULL);
			data_ptr = &d_val;
		} else if (payloadType == 'T'){
			data_ptr = (void*)&payload[indx];
		} else if (payloadType == 'C'){
			//Click, no payload expected 
		} else /*B=bool, N=number, t=timestamp*/{
			l_val = strtoul((const char *)&payload[indx], NULL, 10);
			data_ptr = (void*)&l_val;
		}
		target->handleData(varId, data_ptr);
}
/*
void WEB::createWebsite()
{
	const char* fname = "/index.html";
	
	if(LittleFS.exists(fname)){
		LittleFS.remove(fname);
	}
	
	File file = LittleFS.open(fname, FILE_WRITE);
	
	if(file){
		IWeb::createWebsite(file);	
		file.close();
	}
}
*/
