#include "devicesettings.hpp"

#include "../hw/plughandle.hpp"
#include "../hw/pwmhandle.hpp"

const char DeviceSettings::s_footer[] PROGMEM = R"=====(				</div>
				</article> 
)=====";

const char DeviceSettings::s_symbol_select[] PROGMEM = R"=====(
		<option value='0'>&#xf1e6;</option>
		<option value='1'>&#xf863;</option>
		<option value='2'>&#xf7e4;</option>
		<option value='3'>&#xf0eb;</option>
		<option value='4'>&#xe005;</option>
		<option value='5'>&#xf2cc;</option>
)=====";

const char PlugHandle12VSettings::s_header[] PROGMEM = R"=====(				<article>
				<button class='collapsible' type='button'>
					<i class='fas fa-plug fa-lg'></i>
				</button>
				<div class='content'>
				<table>
					<colgroup>
						<col style='width: 70%%;'>
						<col style='width: 30%%;'>
					</colgroup>
					<tr><th>Name</th><th>Symbol</th></tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' type='text' id='%u' value='%s'></td>
						<td><select onchange='sendNumber(this)' class='selection' id='%u' data-sindx='%u'>%s</select></td>
					</tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' type='text' id='%u' value='%s'></td>
						<td><select onchange='sendNumber(this)' class='selection' id='%u' data-sindx='%u'>%s</select></td>
					</tr>
				</table>
)=====";

PlugHandle12VSettings* PlugHandle12VSettings::singleton()
{
	static PlugHandle12VSettings* instance = nullptr;
	if(instance == nullptr) instance = new PlugHandle12VSettings();
	return instance;
}

size_t PlugHandle12VSettings::len(IWeb::StrObject so){
	
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + strlen_P(s_symbol_select)*2 + 40;
		case StrObject::FOOTER:
		return strlen_P(s_footer) + 3;
		default:
		return 0;
	}
}
size_t PlugHandle12VSettings::copy(IWeb::StrObject so, uint8_t* buffer, size_t len){
	
	auto children = PlugHandle12V::singleton()->getChildren();
	auto child1 = static_cast<PlugDevice12V*>(children[0]);
	auto child2 = static_cast<PlugDevice12V*>(children[1]);
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							child1->createId(50), child1->name(),
							child1->createId(51), child1->getSymbol(), s_symbol_select,
							child2->createId(50), child2->name(),
							child2->createId(51), child2->getSymbol(), s_symbol_select
						);
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        default:
        return 0;
	}
}
/*
void PlugHandle12VSettings::handleData(uint8_t varId, void* const &payload){
	
}
*/

const char PlugHandleRemoteSettings::s_header[] PROGMEM = R"=====(				<article>
				<button class='collapsible' type='button'>
				<span class='fa-layers fa-fw'>
					<i class='fas fa-plug fa-lg'></i>
					<i class='fas fa-wifi' data-fa-transform='shrink-5 down-9 right-12'></i>
				</span>
				</button>
				<div class='content'>
				<table style='table-layout:fixed;'>
					<colgroup>
						<col style='width:25%%;'>
						<col style='width:50%%;'>
						<col style='width:15%%;'>
					</colgroup>
					<tr><th>Name</th><th>IP-Address</th><th>Symbol</th></tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' class='w100' type='text' id='%u' value='%s'></td>
						<td><input type='text' onchange='sendIP(this);' class='ip' id='%u' data-ip='%u'></td>
						<td><select onchange='sendNumber(this)' class='selection' id='%u' data-sindx='%u'>%s</select></td>
					</tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' class='w100' type='text' id='%u' value='%s'></td>
						<td><input type='text' onchange='sendIP(this);' class='ip' id='%u' data-ip='%u'></td>
						<td><select onchange='sendNumber(this)' class='selection' id='%u' data-sindx='%u'>%s</select></td>
					</tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' class='w100' type='text' id='%u' value='%s'></td>
						<td><input type='text' onchange='sendIP(this);' class='ip' id='%u' data-ip='%u'></td>
						<td><select onchange='sendNumber(this)' class='selection' id='%u' data-sindx='%u'>%s</select></td>
					</tr>
				</table>
				<diff style='text-align:left;'><a href='/con_ips' target="_blank" rel='noopener noreferrer'>Get connected IPs (AP only)</a></diff>
)=====";

PlugHandleRemoteSettings* PlugHandleRemoteSettings::singleton()
{
	static PlugHandleRemoteSettings* instance = nullptr;
	if(instance == nullptr) instance = new PlugHandleRemoteSettings();
	return instance;
}

size_t PlugHandleRemoteSettings::len(IWeb::StrObject so){
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + strlen_P(s_symbol_select)*3 + 100;
		case StrObject::FOOTER:
		return strlen_P(s_footer) + 3;
		default:
		return 0;
	}
}
size_t PlugHandleRemoteSettings::copy(IWeb::StrObject so, uint8_t* buffer, size_t len){
	auto children = PlugHandleRemote::singleton()->getChildren();
	auto child1 = static_cast<PlugDeviceRemote*>(children[0]);
	auto child2 = static_cast<PlugDeviceRemote*>(children[1]);
	auto child3 = static_cast<PlugDeviceRemote*>(children[2]);
	
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header, 
							child1->createId(50), child1->name(),
							child1->createId(52), child1->ipAddress(),
							child1->createId(51), child1->getSymbol(), s_symbol_select,
							child2->createId(50), child2->name(),
							child2->createId(52), child2->ipAddress(),
							child2->createId(51), child2->getSymbol(), s_symbol_select,
							child3->createId(50), child3->name(),
							child3->createId(52), child3->ipAddress(),
							child3->createId(51), child3->getSymbol(), s_symbol_select
						);
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        default:
        return 0;
	}
}
/*
void PlugHandleRemoteSettings::handleData(uint8_t varId, void* const &payload){
	
}
*/

const char PWMHandleSettings::s_header[] PROGMEM = R"=====(				<article>
				<button class='collapsible' type='button'><i class='fas fa-fan fa-lg'></i></button>
				<div class='content'>
				<table>
					<colgroup>
						<col style='width: 70%%;'>
						<col style='width: 30%%;'>
					</colgroup>
					<tr><th>Name</th></tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' type='text' id='%u' value='%s'></td>
						<td><input type='number' onchange='sendNumber(this)' id='%u' value='%u' style='width:100%%'></td>
					</tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' type='text' id='%u' value='%s'></td>
						<td><input type='number' onchange='sendNumber(this)' id='%u' value='%u' style='width:100%%'></td>	
					</tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' type='text' id='%u' value='%s'></td>
						<td><input type='number' onchange='sendNumber(this)' id='%u' value='%u' style='width:100%%'></td>
					</tr>
					<tr>
						<td><input onchange='sendText(this)' maxlength='15' type='text' id='%u' value='%s'></td>
						<td><input type='number' onchange='sendNumber(this)' id='%u' value='%u' style='width:100%%'></td>
					</tr>
				</table>
)=====";

PWMHandleSettings* PWMHandleSettings::singleton()
{
	static PWMHandleSettings* instance = nullptr;
	if(instance == nullptr) instance = new PWMHandleSettings();
	return instance;
}

size_t PWMHandleSettings::len(IWeb::StrObject so){
	
	switch(so){
		case StrObject::HEADER:
		return strlen_P(s_header) + 50;
		case StrObject::FOOTER:
		return strlen_P(s_footer) + 3;
		default:
		return 0;
	}
}
size_t PWMHandleSettings::copy(IWeb::StrObject so, uint8_t* buffer, size_t len){
	auto children = PWMHandle::singleton()->getChildren();
	auto child1 = static_cast<PWMDevice*>(children[0]);
	auto child2 = static_cast<PWMDevice*>(children[1]);
	auto child3 = static_cast<PWMDevice*>(children[2]);
	auto child4 = static_cast<PWMDevice*>(children[3]);
    switch(so){
        case StrObject::HEADER:
        return snprintf_P((char*)buffer, len, s_header,
							child1->createId(50), child1->name(),
							child1->createId(51), 0,
							child2->createId(50), child2->name(),
							child2->createId(51), 0,
							child3->createId(50), child3->name(),
							child3->createId(51), 0,
							child4->createId(50), child4->name(),
							child4->createId(51), 0
						);
        case StrObject::FOOTER:
        return snprintf_P((char*)buffer, len, s_footer);
        default:
        return 0;
	}
}
