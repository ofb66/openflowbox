/*generated file configmanager.cpp*/
#include "configmanager.hpp"
#include "../persistence/fileexchangehandler.hpp"

#include <LittleFS.h>
#include <FS.h>

const char ModuleConfigManager::s_header[] PROGMEM = R"=====(
	<article title='Download/Upload configurations'>
		<button type='button' class='collapsible'><i class='fas fa-download'></i></button>
		<div class='content'>
			<h3>Download files</h3>
			<table>
			<tr>
			<td>Basic(Wifi settings, etc...)</td>
			<td><a href="/download?/Basic.ocp" download><button type='button'><i class='fas fa-download'></i></button></a></td>
			</tr>
			<tr>
			<td>Modules(Device settings)</td>
			<td><a href="/download?/Module.ocp" download><button type='button'><i class='fas fa-download'></i></button></a></td>
			</tr>
			<tr>
			<td>Full(Basic+Modules)</td>
			<td><a href="/download?/Full.ocp" download><button type='button'><i class='fas fa-download'></i></button></a></td>
			</tr>
			</table>
			<hr>
			<h3>Upload files</h3>
			<form enctype="multipart/form-data" method="POST">
				<input type="file" name="upload" multiple/><br><br>
				<button type='submit' class='progressbtn' style='width:100%%'><i class='fas fa-upload'></i></button>
			</form>
		</div>
	</article>
	<article>
		<button type='button' class='collapsible'>
			<span class='fa-layers fa-fw'>
			<i class='fas fa-file'></i>
			<i class='fas fa-cog' data-fa-transform='shrink-5' style='color: #777'></i>
			</span>
		</button>
		<div class='content' style="padding-left: 0px;padding-right: 0px;padding-bottom: 0px;" id='%u'>
			<table style='table-layout:fixed;'>
				<colgroup>
					<col style='width: 40%%; text-align: left'>
					<col style='width: 40%%; text-align: left'>
					<col style='width: 20%%; text-align: center'>
				</colgroup>
				<tr>
					<td><label for='%u'>New config:</label></td>
					<td><input type='text' maxlength='8' class='w100' id='%u'></td>
					<td><button type='button' class='progressbtn' style='width:100%%;' id='%u' onclick='createNewFile("%u", "%u")'><i class='fas fa-plus'></i></button></td>
				</tr>
				<tr>
					<td><label>Config:</label></td>
					<td>
						<select id='%u' onchange='sendText(this)' class='selection' data-sindx='%u' data-csv='%s'>
						</select>
					</td>
					<td><button type='button' class='progressbtn' style='width:100%%;' id='%u' onclick='sendClick(this); var s=ebi("%u");s.remove(s.selectedIndex);'><i class='fas fa-minus'></i></button></td>
				</tr>
			</table>
			<hr>
			<h3>Timebased configuration chain</h3>
		</div>
	</article>
)=====";

const char ModuleConfigManager::s_json_ui[] PROGMEM = R"=====(

)=====";

//ctor
ModuleConfigManager::ModuleConfigManager(IWeb *parent) : 
	IWeb(parent), mod_cfg_dir("/cfg/mod/"), m_sindx(0)
{
	createOptions();
}

size_t ModuleConfigManager::len(IWeb::StrObject so)
{
	switch(so)
	{
		case StrObject::HEADER:
		return strlen_P(s_header) + 40 + getOptLen();
		case StrObject::JSON_UI:
		return strlen_P(s_json_ui);
		default: return 0;	
	}
}

size_t ModuleConfigManager::copy(IWeb::StrObject so, uint8_t* buffer, size_t len)
{	
	switch(so){
		case StrObject::HEADER:
		return snprintf_P((char*)buffer, len, s_header, 
							webId(), createId(0), createId(0), 
							createId(3), createId(0), createId(1),
							createId(1), m_sindx, opt_buf,
							createId(2), createId(1)
						);
		case StrObject::JSON_UI:
		return snprintf_P((char*)buffer, len, s_json_ui);
		default:
		return 0;
	}
}

size_t ModuleConfigManager::getOptLen()
{
	return OPT_BUF_MAX_LEN;
}

void ModuleConfigManager::createOptions()
{
	//String str = F("");
	if(!LittleFS.exists(mod_cfg_dir)) {
		log_w("dir /cfg not exists");
		return; 
	}
	File dir = LittleFS.open(mod_cfg_dir);
	size_t cfgDirLen = strlen(mod_cfg_dir);
	if(!dir) return;
	
	File file = dir.openNextFile();
	uint8_t indx = 0;
	
	while(file){
		if(indx > 0){
			opt_buf[indx++] = ',';
		}
		auto fnlen = strlen(file.name()) - cfgDirLen;
		strncpy(&opt_buf[indx], &file.name()[cfgDirLen], fnlen);
		indx += fnlen;
		
		file = dir.openNextFile();
	}
	opt_buf[indx] = 0;
}

ModuleConfigManager* ModuleConfigManager::singleton()
{
	static ModuleConfigManager* _singleton = nullptr;
	if(_singleton == nullptr) _singleton = new ModuleConfigManager();
	return _singleton;
}

//dtor
ModuleConfigManager::~ModuleConfigManager()
{
}

//inherited functions
void ModuleConfigManager::handleData(uint8_t varId, void* const &payload)
{
	if(varId < 2){
		const size_t pathLen = strlen(mod_cfg_dir)+strlen((char*)payload)+1;
		char path[pathLen];
		strcpy(path, mod_cfg_dir);
		strncpy(&path[strlen(mod_cfg_dir)], (char*)payload, strlen((char*)payload));
		path[pathLen-1] = 0;
		
		setConfigFile(path);
		
		if(varId == 0){
			write();
			ISerializable::enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES);
		} else if(varId == 1){
			read();
			ISerializable::s_enableFlag(ISerializable::SER_NOTIFY_ABOUT_CHANGES, ISerializable::Type::BASIC_CFG);
		}
	} else if(varId == 2){
		if(!configFile().empty()){
			LittleFS.remove(configFile().c_str());
			File nextFile = LittleFS.open(mod_cfg_dir).openNextFile();
			if(nextFile){
				setConfigFile(nextFile.name());
				read();
			}
		}
	}
	createOptions();
}

//EOF
