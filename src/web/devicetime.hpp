/*generated file devicetime.hpp*/
#ifndef DEVICETIME_HPP
#define DEVICETIME_HPP


#include <Arduino.h>

#include <WiFiUdp.h>
#include <NTPClient.h>

#include "iweb.hpp"
#include "../utils/timer.hpp"

#include "../persistence/serializable.hpp"
using namespace Persistence;

class DeviceTime : public IWeb, public ISerializable
{
private:	
	static const char s_header[] PROGMEM;
	static const char s_json_dyn[] PROGMEM;
	static const char s_json_ui[] PROGMEM;
	
	int32_t m_offset;
	uint16_t m_day_count;
	uint32_t m_runtime_sec;
	
    std::string m_pool_server;

	long long sync_client_time;
	unsigned long sync_timer_start;
	
	Utils::Timer64 runTimer;
	
	WiFiUDP *udp_ptr;
	NTPClient *ntp_client_ptr;
	
	enum Mode {
		USE_NTP_SERVER,
		USE_DEVC_TIMESTAMP
	} m_mode;
	
	DeviceTime(IWeb* const parent = nullptr);
public:
	static DeviceTime * singleton();
	
	void begin(bool connectedToInternet);
	void update();
	void syncToClient(long long clientUnixTime);
	
	inline int32_t getOffset() const { return m_offset; }
	void setOffset(int32_t offset);
	
	inline int32_t getOffsetInHours() const { return m_offset / 3600; }
	inline uint16_t getDayCount() const { return m_day_count; }
	inline uint32_t getTotalRuntimeSec() { return m_runtime_sec + getCurrentRuntimeSec(); }
	inline int64_t getCurrentRuntimeSec() { return runTimer.runtime()/1000; }
	unsigned long getEpochTime();
	
	virtual size_t len(IWeb::StrObject so);
	virtual size_t copy(IWeb::StrObject so, uint8_t* buffer, size_t len);
	
    virtual size_t size();
    virtual uint8_t* serialize(uint8_t *eptr);
    virtual const uint8_t* deserialize(const uint8_t *eptr);
    
    virtual void handleData(uint8_t varId, void* const &payload) override;
};

#endif //DEVICETIME_HPP
