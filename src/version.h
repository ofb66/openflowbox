#ifndef VERSION_H
#define VERSION_H

#define V_STRINGIFY_(x) #x
#define V_STRINGIFY(x) V_STRINGIFY_(x)

#define V_MAJOR 0
#define V_MINOR 8
#define V_PATCH 0

#define S_V_MAJOR V_STRINGIFY(V_MAJOR)
#define S_V_MINOR V_STRINGIFY(V_MINOR)
#define S_V_PATCH V_STRINGIFY(V_PATCH)

#define VERSION S_V_MAJOR "." S_V_MINOR "." S_V_PATCH  // the version of this sketch 

#endif
