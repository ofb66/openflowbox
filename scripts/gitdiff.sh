#!/bin/bash 

while getopts "b:" opt; do
  case $opt in
    b) BRANCH="$OPTARG"
    shift
    shift
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

FILE=$1

if [[ "${BRANCH}" == "" ]]
then
	BRANCH="master"
fi

echo "###### git diff $BRANCH:$FILE $FILE ######"
echo ""

git diff $BRANCH:./$FILE $FILE
