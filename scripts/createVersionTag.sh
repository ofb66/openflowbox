#!/bin/bash

VFILE="src/version.h"

MAJOR_S="#define V_MAJOR"
MINOR_S="#define V_MINOR"
PATCH_S="#define V_PATCH"

MAJOR=$(grep "${MAJOR_S}" ${VFILE} | tr -d "${MAJOR_S} ")
MINOR=$(grep "${MINOR_S}" ${VFILE} | tr -d "${MINOR_S} ")
PATCH=$(grep "${PATCH_S}" ${VFILE} | tr -d "${PATCH_S} ")

HAS_UPDATE=0

C_MSG=""
T_MSG=""
while getopts "pmnc:t:" opt; do
  case $opt in
    m) HAS_UPDATE="MAJOR"
    ;;
    n) HAS_UPDATE="MINOR"
    ;;
    p) HAS_UPDATE="PATCH"
    ;;
    c) C_MSG="$OPTARG"
    ;;
    t) T_MSG="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done


if [[ "${HAS_UPDATE}" == "PATCH" ]]
then
	((NPATCH=PATCH+1))
	NMINOR=${MINOR}
	NMAJOR=${MAJOR}
elif [[ "${HAS_UPDATE}" == "MINOR" ]]
then
	NPATCH=0
	((NMINOR=MINOR+1))
	NMAJOR=${MAJOR}
elif [[ "${HAS_UPDATE}" == "MAJOR" ]]
then
	NPATCH=0
	NMINOR=0
	((NMAJOR=MAJOR+1))
elif git rev-parse "$TAG" >/dev/null 2>&1
then
  echo "tag already exists"
  exit 1
fi

sed -i "s/${MAJOR_S} ${MAJOR}/${MAJOR_S} ${NMAJOR}/g" $VFILE
sed -i "s/${MINOR_S} ${MINOR}/${MINOR_S} ${NMINOR}/g" $VFILE
sed -i "s/${PATCH_S} ${PATCH}/${PATCH_S} ${NPATCH}/g" $VFILE

if [[ "${C_MSG}" == "" ]]
then
    C_MSG="${T_MSG}"
fi

if [[ "${C_MSG}" != "" ]]
then
    C_MSG="${C_MSG} + "
fi

C_MSG="${C_MSG}Update version to v${NMAJOR}.${NMINOR}.${NPATCH}"
echo "git commit -am \"${C_MSG}\""
git commit -am "${C_MSG}"

if [[ "${T_MSG}" == "" ]]
then
    T_MSG="${C_MSG}"
fi 

echo "git tag -a v${NMAJOR}.${NMINOR}.${NPATCH} -m \"${T_MSG}\""
git tag -a v${NMAJOR}.${NMINOR}.${NPATCH} -m "${T_MSG}"


