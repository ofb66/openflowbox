//#ifndef UNIT_TEST
#include <Arduino.h>

#include "src/web/web.hpp"
#include "src/hw/sensorhandle.hpp"
#include "src/hw/plughandle.hpp"
#include "src/hw/pwmhandle.hpp"
#include "src/utils/timer.hpp"

//#include "src/eeprom/eeprommanager.hpp"
#include "src/persistence/persistence.hpp"
#include "src/persistence/serializable.hpp"
#include "src/persistence/fileexchangehandler.hpp"
#include "src/web/configmanager.hpp"

const unsigned long persistenceUpdateInterval = 3600000;  // set persitence module update interval 1hr

const uint16_t interval = 1000;
Utils::Timer16 intervalTimer;

void listDir(fs::FS &fs, const char * dirname, uint8_t levels){
    log_v("Listing directory: %s\r\n", dirname);

    File root = fs.open(dirname);
    if(!root){
        log_v("- failed to open directory");
        return;
    }
    if(!root.isDirectory()){
        log_v(" - not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file){
        if(file.isDirectory()){
            log_v("  DIR : %s", file.name());
            if(levels){
                listDir(fs, file.path(), levels -1);
            }
        } else {
            log_v("  FILE: %s\tSIZE: %u", file.name(), file.size());
        }
        file = root.openNextFile();
    }
}

void setup() {
    //Serial.begin(115200);

    //while(!Serial){ ; }
    if(!LittleFS.begin()){
        log_v("An Error has occured while mounting LITTLEFS");
        //LittleFS.format();
        ESP.restart();
    }
    //LittleFS.format();
    /*Range of initialization
     * 1) RootDoc and all its children
     * 2) Persistence: looking for data in memory (including wifi access data (ssid, pw,...))
     * 3) WEB
     */
    if(!LittleFS.exists("/cfg")){
      LittleFS.mkdir("/cfg");
      LittleFS.mkdir("/cfg/mod");
    }
    if(!LittleFS.exists("/js")){
      LittleFS.mkdir("/js");
    }
    if(!LittleFS.exists("/css")){
      LittleFS.mkdir("/css");
    }
    if(!LittleFS.exists("/svg")){
      LittleFS.mkdir("/svg");
    }
    if(!LittleFS.exists("/webfonts")){
      LittleFS.mkdir("/webfonts");
    }

    log_d("TB: %u UB: %u FB: %u", LittleFS.totalBytes(), LittleFS.usedBytes(), LittleFS.totalBytes()-LittleFS.usedBytes());
    
     
    RootDoc::singleton();

    BasicConfigHandle::singleton()->begin();
    ModuleConfigManager::singleton()->begin(); 
    
    WEB::begin();
    listDir(LittleFS, "/", 3);
    
    intervalTimer.start();

    
    /*
    FileExchangeHandler::singleton()->prepareExportPackage();
    FileExchangeHandler::singleton()->processPackageImport();
    */
}

void loop() {
  WEB::update();
  BasicConfigHandle::singleton()->update(persistenceUpdateInterval);
  ModuleConfigManager::singleton()->update(persistenceUpdateInterval);

  SensorHandle::singleton()->update();
  
  if(intervalTimer.hasExpired(interval)){
    intervalTimer.start();
    PWMHandle::singleton()->update();
    PlugHandle12V::singleton()->update();
    PlugHandleRemote::singleton()->update();
  }
}
//#endif
